package com.frog.sip.server.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author neal
 * @date 2024/12/18  18:13
 */
@Getter
@AllArgsConstructor
public enum SipResponseCode {
    TRYING(100, "Trying"),
    RINGING(180, "Ringing"),
    CALL_IS_BEING_FORWARDED(181, "Call_Is_Being_Forwarded"),
    QUEUED(182, "Queued"),
    SESSION_PROGRESS(183, "Session_Progress"),
    OK(200, "OK"),
    ACCEPTED(202, "Accepted"),
    MULTIPLE_CHOICES(300, "Multiple_Choices"),
    MOVED_PERMANENTLY(301, "Moved_Permanently"),
    MOVED_TEMPORARILY(302, "Moved_Temporarily"),
    USE_PROXY(305, "Use_Proxy"),
    ALTERNATIVE_SERVICE(380, "Alternative_Service"),
    BAD_REQUEST(400, "Bad_Request"),
    UNAUTHORIZED(401, "Unauthorized"),
    PAYMENT_REQUIRED(402, "Payment_Required"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not_Found"),
    METHOD_NOT_ALLOWED(405, "Method_Not_Allowed"),
    NOT_ACCEPTABLE(406, "Not_Acceptable"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy_Authentication_Required"),
    REQUEST_TIMEOUT(408, "Request_Timeout"),
    GONE(410, "Gone"),
    CONDITIONAL_REQUEST_FAILED(412, "Conditional_Request_Failed"),
    REQUEST_ENTITY_TOO_LARGE(413, "Request_Entity_Too_Large"),
    REQUEST_URI_TOO_LONG(414, "Request_URI_Too_Long"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported_Media_Type"),
    UNSUPPORTED_URI_SCHEME(416, "Unsupported_URI_Scheme"),
    BAD_EXTENSION(420, "Bad_Extension"),
    EXTENSION_REQUIRED(421, "Extension_Required"),
    INTERVAL_TOO_BRIEF(423, "Interval_Too_Brief"),
    TEMPORARILY_UNAVAILABLE(480, "Temporarily_Unavailable"),
    CALL_OR_TRANSACTION_DOES_NOT_EXIST(481, "Call_Or_Transaction_Does_Not_Exist"),
    LOOP_DETECTED(482, "Loop_Detected"),
    TOO_MANY_HOPS(483, "Too_Many_Hops"),
    ADDRESS_INCOMPLETE(484, "Address_Incomplete"),
    AMBIGUOUS(485, "Ambiguous"),
    BUSY_HERE(486, "Busy_Here"),
    REQUEST_TERMINATED(487, "Request_Terminated"),
    NOT_ACCEPTABLE_HERE(488, "Not_Acceptable_Here"),
    BAD_EVENT(489, "Bad_Event"),
    REQUEST_PENDING(491, "Request_Pending"),
    UNDECIPHERABLE(493, "Undecipherable"),
    SERVER_INTERNAL_ERROR(500, "Server_Internal_Error"),
    NOT_IMPLEMENTED(501, "Not_Implemented"),
    BAD_GATEWAY(502, "Bad_Gateway"),
    SERVICE_UNAVAILABLE(503, "Service_Unavailable"),
    SERVER_TIMEOUT(504, "Server_Timeout"),
    VERSION_NOT_SUPPORTED(505, "Version_Not_Supported"),
    MESSAGE_TOO_LARGE(513, "Message_Too_Large"),
    BUSY_EVERYWHERE(600, "Busy_Everywhere"),
    DECLINE(603, "Decline"),
    DOES_NOT_EXIST_ANYWHERE(604, "Does_Not_Exist_Anywhere"),
    SESSION_NOT_ACCEPTABLE(606, "Session_Not_Acceptable");

    public static SipResponseCode getEnumByCode(int code){
        for( SipResponseCode sipResponseCode:SipResponseCode.values()){
            if(sipResponseCode.getCode()==code){
                return sipResponseCode;
            }
        }
        throw new IllegalArgumentException("No enum be find ");
    }

    private int code;
    private String name;
}
