package com.frog.sip.service.impl;

import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.redis.RedisCache;
import com.frog.common.utils.DateUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.domain.Product;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.mapper.ProductMapper;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.enums.DeviceChannelStatus;
import com.frog.sip.mapper.SipDeviceChannelMapper;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.ISipDeviceChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.frog.common.utils.SecurityUtils.getLoginUser;

/**
 * 监控设备通道信息Service业务层处理
 *
 * @author zhuangpeng.li
 * @date 2022-10-07
 */
@Service
@Slf4j
public class SipDeviceChannelServiceImpl implements ISipDeviceChannelService {
    @Autowired
    private SipDeviceChannelMapper sipDeviceChannelMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private DeviceMapper deviceMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ISipCacheService sipCacheService;

    @Override
    public void updateChannel(String deviceId, SipDeviceChannel channel) {
        String channelSipId = channel.getChannelSipId();
        channel.setDeviceSipId(deviceId);
        SipDeviceChannel deviceChannel = sipDeviceChannelMapper.selectSipDeviceChannelByChannelSipId(channelSipId);
        if (deviceChannel == null) {
            log.info("不存在该channle:{}，请用生成的channel注册",channelSipId);
        } else {
            channel.setId(deviceChannel.getId());
            sipDeviceChannelMapper.updateSipDeviceChannel(channel);
        }
    }

    /**
     * 查询监控设备通道信息
     *
     * @param channelId 监控设备通道信息主键
     * @return 监控设备通道信息
     */
    @Override
    public SipDeviceChannel selectSipDeviceChannelByChannelId(Long channelId) {
        return sipDeviceChannelMapper.selectSipDeviceChannelById(channelId);
    }

    @Override
    public SipDeviceChannel selectSipDeviceChannelByChannelSipId(String channelSipId) {
        return sipDeviceChannelMapper.selectSipDeviceChannelByChannelSipId(channelSipId);
    }

    @Override
    public List<SipDeviceChannel> selectSipDeviceChannelByDeviceSipId(String deviceSipId) {
        return sipDeviceChannelMapper.selectSipDeviceChannelByDeviceSipId(deviceSipId);
    }

    /**
     * 查询监控设备通道信息列表
     *
     * @param sipDeviceChannel 监控设备通道信息
     * @return 监控设备通道信息
     */
    @Override
    public List<SipDeviceChannel> selectSipDeviceChannelList(SipDeviceChannel sipDeviceChannel) {
        return sipDeviceChannelMapper.selectSipDeviceChannelList(sipDeviceChannel);
    }

    /**
     * 新增监控设备通道信息
     *
     * @param sipDeviceChannel 监控设备通道信息
     * @return 结果
     */
    @Override
    public String insertSipDeviceChannel(Long createNum, SipDeviceChannel sipDeviceChannel) {
        int result = 0;
        String tmpdev = sipDeviceChannel.getDeviceSipId();
        String tmpchannel = sipDeviceChannel.getChannelSipId();
        int deviceId14B = sipCacheService.getSipUnicodeFront14B(tmpdev);
        sipCacheService.IncrSipUnicodeFront14B(tmpdev,1L);
        int channelId14B = sipCacheService.getSipUnicodeFront14B(tmpchannel);
        sipCacheService.IncrSipUnicodeFront14B(tmpchannel,createNum);
        String devstr = String.format("%06d", deviceId14B + 1);
        SysUser sysUser = getLoginUser().getUser();
        sipDeviceChannel.setUserId(sysUser.getUserId());
        sipDeviceChannel.setUserName(sysUser.getUserName());
        sipDeviceChannel.setCreateTime(DateUtils.getNowDate());
        sipDeviceChannel.setDeviceSipId(tmpdev + devstr);
        sipDeviceChannel.setStatus(DeviceChannelStatus.notused.getValue());
        for (int i = 1; i <= createNum; i++) {
            String channelstr = String.format("%06d", channelId14B + i);
            sipDeviceChannel.setChannelSipId(tmpchannel + channelstr);
            result = sipDeviceChannelMapper.insertSipDeviceChannel(sipDeviceChannel);
        }

        if (result > 0) {
            return tmpdev + devstr;
        } else {
            return "";
        }
    }

    /**
     * 新增监控设备通道信息
     *
     * @param sipDeviceChannel 监控设备通道信息
     * @return 结果
     */
    @Override
    public String insertSipDeviceChannelToDevice(Long createNum, SipDeviceChannel sipDeviceChannel) {
        int result = 0;
        String tmpchannel = sipDeviceChannel.getChannelSipId();
        int channelId14B = sipCacheService.getSipUnicodeFront14B(tmpchannel);
        sipCacheService.IncrSipUnicodeFront14B(tmpchannel,createNum);
        SysUser sysUser = getLoginUser().getUser();
        sipDeviceChannel.setUserId(sysUser.getUserId());
        sipDeviceChannel.setUserName(sysUser.getUserName());
        sipDeviceChannel.setCreateTime(DateUtils.getNowDate());
        sipDeviceChannel.setDeviceSipId(sipDeviceChannel.getDeviceSipId());
        sipDeviceChannel.setStatus(DeviceChannelStatus.notused.getValue());
        Device device = deviceMapper.selectDeviceBySerialNumber(sipDeviceChannel.getDeviceSipId());
        if(device !=null ){
            Product product = productMapper.selectProductByProductId(device.getProductId());
            if(product !=null){
                sipDeviceChannel.setProductName(product.getProductName());
            }
        }
        for (int i = 1; i <= createNum; i++) {
            String channelstr = String.format("%06d", channelId14B + i);
            sipDeviceChannel.setChannelSipId(tmpchannel + channelstr);
            result = sipDeviceChannelMapper.insertSipDeviceChannel(sipDeviceChannel);
        }
        if (result > 0) {
            return "success";
        } else {
            return "";
        }
    }

    /**
     * 修改监控设备通道信息
     *
     * @param sipDeviceChannel 监控设备通道信息
     * @return 结果
     */
    @Override
    public int updateSipDeviceChannel(SipDeviceChannel sipDeviceChannel)
    {
        return sipDeviceChannelMapper.updateSipDeviceChannel(sipDeviceChannel);
    }

    @Override
    public int updateSipDeviceChannelStatus(String ChannelId, Integer status) {
        SipDeviceChannel sipDeviceChannel = sipDeviceChannelMapper.selectSipDeviceChannelByChannelSipId(ChannelId);
        if (sipDeviceChannel != null) {
            if (sipDeviceChannel.getRegisterTime() == null && status == 2) {
                sipDeviceChannel.setRegisterTime(DateUtils.getNowDate());
            }
            sipDeviceChannel.setStatus(status);
            return sipDeviceChannelMapper.updateSipDeviceChannel(sipDeviceChannel);
        }
        return 0;
    }

    /**
     * 批量删除监控设备通道信息
     *
     * @param channelIds 需要删除的监控设备通道信息主键
     * @return 结果
     */
    @Override
    public int deleteSipDeviceChannelByChannelIds(Long[] channelIds)
    {
        return sipDeviceChannelMapper.deleteSipDeviceChannelByIds(channelIds);
    }

    /**
     * 删除监控设备通道信息信息
     *
     * @param channelId 监控设备通道信息主键
     * @return 结果
     */
    @Override
    public int deleteSipDeviceChannelByChannelId(Long channelId)
    {
        return sipDeviceChannelMapper.deleteSipDeviceChannelById(channelId);
    }
}
