package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 萤石云设备通道对象 ys_device_channel
 * 
 * @author nealtsiao
 * @date 2024-11-25
 */
@Data
public class YsDeviceChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备ID */
    private Long channelId;

    /** 产品ID */
    private Long productId;

    /** 设备序列号 */
    @Excel(name = "设备序列号")
    private String deviceSerial;


    /** 通道号 */
    @Excel(name = "通道号")
    private String channelNo;

    /** 通道名称 */
    @Excel(name = "通道名称")
    private String channelName;

    /** 在线状态：0-不在线，1-在线 */
    @Excel(name = "在线状态：0-不在线，1-在线")
    private String status;

    /** 图片地址（大图），若在萤石客户端设置封面则返回封面图片，未设置则返回默认图片 */
    @Excel(name = "图片地址", readConverterExp = "大=图")
    private String picUrl;

    /** 是否加密，0：不加密，1：加密 */
    @Excel(name = "是否加密，0：不加密，1：加密")
    private Long isEncrypt;

    /** 视频质量：0-流畅，1-均衡，2-高清，3-超清 */
    @Excel(name = "视频质量：0-流畅，1-均衡，2-高清，3-超清")
    private Long videoLevel;

    /** 分享设备的权限字段 */
    @Excel(name = "分享设备的权限字段")
    private Long permission;

    /** 0:隐藏，1:显示 */
    @Excel(name = "0:隐藏，1:显示")
    private Long isAdd;

    /** 排序 */
    @Excel(name = "排序")
    private Long orderNum;
}
