package com.frog.sip.server.listener.impl;

import com.frog.sip.server.enums.SipResponseCode;
import com.frog.sip.server.handler.IReqHandler;
import com.frog.sip.server.handler.IResHandler;
import com.frog.sip.server.listener.IGBListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.sip.*;
import javax.sip.header.CSeqHeader;
import javax.sip.message.Response;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.frog.sip.server.enums.SipResponseCode.getEnumByCode;

@Slf4j
@Component
public class GBListenerImpl implements IGBListener {

    private static final Map<String, IReqHandler> requestProcessorMap = new ConcurrentHashMap<>();
    private static final Map<String, IResHandler> responseProcessorMap = new ConcurrentHashMap<>();

    public void addRequestProcessor(String method, IReqHandler processor) {
        requestProcessorMap.put(method, processor);
    }

    public void addResponseProcessor(String method, IResHandler processor) {
        responseProcessorMap.put(method, processor);
    }

    @Override
    @Async("sipTask")
    /**
     * 处理设备请求
     * 实现SipListener接口processRequest方法
     */
    public void processRequest(RequestEvent evt) {
        String method = evt.getRequest().getMethod();
        IReqHandler sipRequestProcessor = requestProcessorMap.get(method);
        if (sipRequestProcessor == null) {
            log.warn("不支持方法{}的request", method);
            return;
        }
        requestProcessorMap.get(method).processMsg(evt);
    }

    @Override
    @Async("sipTask")
    /**
     * 处理设备响应
     * 实现SipListener接口processResponse方法
     */
    public void processResponse(ResponseEvent evt) {
        //处理响应消息
        Response response = evt.getResponse();
        int status = response.getStatusCode();
        // 响应成功
        SipResponseCode code = getEnumByCode(status);
        CSeqHeader cseqHeader = (CSeqHeader) evt.getResponse().getHeader(CSeqHeader.NAME);
        String method = cseqHeader.getMethod();
        IResHandler sipRequestProcessor = responseProcessorMap.get(code.getName()+method);
        if (sipRequestProcessor != null) {
            try {
                sipRequestProcessor.processMsg(evt);
            } catch (ParseException e) {
               log.error("[没有找到合适的Response处理器]");
            }
        }
    }

    @Override
    public void processTimeout(TimeoutEvent timeoutEvent) {
        log.error("processTimeout：{}", timeoutEvent);
    }

    @Override
    public void processIOException(IOExceptionEvent ioExceptionEvent) {
        //log.debug("processIOException：{}", ioExceptionEvent);
    }

    @Override
    public void processTransactionTerminated(TransactionTerminatedEvent transactionTerminatedEvent) {
        //log.debug("processTransactionTerminated：{}", transactionTerminatedEvent);
    }

    @Override
    public void processDialogTerminated(DialogTerminatedEvent dialogTerminatedEvent) {
        //log.debug("processDialogTerminated：{}", dialogTerminatedEvent);
    }
}
