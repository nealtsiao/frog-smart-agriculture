package com.frog.view.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.enums.BusinessType;
import com.frog.view.domain.GoViewInfo;
import com.frog.view.service.IGoViewInfoService;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.common.core.page.TableDataInfo;

/**
 * goview大屏Controller
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@Api(tags = "view-GoView大屏")
@RestController
@RequestMapping("/view/project")
public class GoViewInfoController extends BaseController
{
    @Autowired
    private IGoViewInfoService GoViewInfoService;

    /**
     * 查询goview大屏列表
     */
    @ApiOperation("查询GoView大屏列表")
    @GetMapping("/list")
    public TableDataInfo list(GoViewInfo GoViewInfo)
    {
        startPage();
        List<GoViewInfo> list = GoViewInfoService.selectGoViewInfoList(GoViewInfo);
        return getDataTable(list);
    }

    /**
     * 导出goview大屏列表
     */
    @ApiOperation("导出GoView大屏列表")
    @Log(title = "goview大屏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoViewInfo GoViewInfo)
    {
        List<GoViewInfo> list = GoViewInfoService.selectGoViewInfoList(GoViewInfo);
        ExcelUtil<GoViewInfo> util = new ExcelUtil<GoViewInfo>(GoViewInfo.class);
        util.exportExcel(response, list, "goview大屏数据");
    }

    /**
     * 获取goview大屏详细信息
     */
    @ApiOperation("获取GoView大屏详细信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(GoViewInfoService.selectGoViewInfoById(id));
    }

    /**
     * 新增goview大屏
     */
    @ApiOperation("新增GoView大屏信息")
    @Log(title = "goview大屏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoViewInfo GoViewInfo)
    {
        return toAjax(GoViewInfoService.insertGoViewInfo(GoViewInfo));
    }

    /**
     * 修改goview大屏
     */
    @ApiOperation("修改GoView大屏信息")
    @Log(title = "goview大屏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoViewInfo GoViewInfo)
    {
        return toAjax(GoViewInfoService.updateGoViewInfo(GoViewInfo));
    }

    /**
     * 删除goview大屏
     */
    @ApiOperation("删除GoView大屏信息")
    @Log(title = "goview大屏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(GoViewInfoService.deleteGoViewInfoByIds(ids));
    }
}
