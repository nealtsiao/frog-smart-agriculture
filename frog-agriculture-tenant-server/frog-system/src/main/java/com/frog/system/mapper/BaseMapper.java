package com.frog.system.mapper;
import com.frog.system.domain.Base;

import java.util.List;

/**
 * 基地信息Mapper接口
 * 
 * @author nealtsiao
 * @date 2023-05-13
 */
public interface BaseMapper
{
    /**
     * 查询基地信息
     * 
     * @param baseId 基地信息主键
     * @return 基地信息
     */
    public Base selectBaseByBaseId(Long baseId);

    /**
     * 查询基地信息
     *
     * @param deptId
     * @return
     */
    public Base selectBaseByDeptId(Long deptId);

    /**
     * 查询基地信息列表
     * 
     * @param base 基地信息
     * @return 基地信息集合
     */
    public List<Base> selectBaseList(Base base);

    /**
     * 新增基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    public int insertBase(Base base);

    /**
     * 修改基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    public int updateBase(Base base);

    /**
     * 删除基地信息
     * 
     * @param baseId 基地信息主键
     * @return 结果
     */
    public int deleteBaseByBaseId(Long baseId);

    /**
     * 批量删除基地信息
     * 
     * @param baseIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseByBaseIds(Long[] baseIds);
}
