package com.frog.system.service;
import com.frog.common.core.domain.entity.SysDept;
import com.frog.system.domain.Base;

import java.util.List;

/**
 * 基地信息Service接口
 * 
 * @author nealtsiao
 * @date 2023-05-13
 */
public interface IBaseService
{
    /**
     * 查询基地信息
     * 
     * @param baseId 基地信息主键
     * @return 基地信息
     */
    public Base selectBaseinfoByBaseId(Long baseId);

    /**
     * 查询基地信息列表
     * 
     * @param base 基地信息
     * @return 基地信息集合
     */
    public List<Base> selectBaseinfoList(Base base);

    /**
     * 新增基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    public int insertBaseinfo(Base base);

    /**
     * 修改基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    public int updateBaseinfo(Base base);

    /**
     * 批量删除基地信息
     * 
     * @param baseIds 需要删除的基地信息主键集合
     * @return 结果
     */
    public int deleteBaseinfoByBaseIds(Long[] baseIds);

    /**
     * 删除基地信息信息
     * 
     * @param baseId 基地信息主键
     * @return 结果
     */
    public int deleteBaseinfoByBaseId(Long baseId);

    /**
     * 同步基地信息
     * @param dept
     * @param type
     * @return
     */
    public void synchronizeBaseinfo(SysDept dept,String type);
}
