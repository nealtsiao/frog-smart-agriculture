package com.frog.iot.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.frog.common.constant.Constants;
import com.frog.common.constant.UserConstants;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.redis.RedisCache;
import com.frog.common.exception.user.CaptchaException;
import com.frog.common.exception.user.CaptchaExpireException;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.MessageUtils;
import com.frog.common.utils.SecurityUtils;
import com.frog.common.utils.StringUtils;
import com.frog.framework.manager.AsyncManager;
import com.frog.framework.manager.factory.AsyncFactory;
import com.frog.iot.domain.Device;
import com.frog.iot.domain.ProductAuthorize;
import com.frog.iot.mapper.ProductAuthorizeMapper;
import com.frog.iot.model.AuthenticateInputModel;
import com.frog.iot.model.MqttAuthenticationModel;
import com.frog.iot.model.ProductAuthenticateModel;
import com.frog.iot.model.RegisterUserInput;
import com.frog.iot.model.ThingsModels.ThingsModelShadow;
import com.frog.iot.mqtt.EmqxService;
import com.frog.iot.service.IDeviceService;
import com.frog.iot.service.IToolService;
import com.frog.iot.util.AESUtils;
import com.frog.system.mapper.SysUserMapper;
import com.frog.system.service.ISysConfigService;
import com.frog.system.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.frog.iot.mqtt.MqttConfig;

import java.util.List;
import java.util.Random;

/**
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Service
public class ToolServiceImpl implements IToolService
{
    private static final Logger log = LoggerFactory.getLogger(ToolServiceImpl.class);

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ProductAuthorizeMapper productAuthorizeMapper;

    @Autowired
    private MqttConfig mqttConfig;

    @Lazy
    @Autowired
    private EmqxService emqxService;

    @Autowired
    @Lazy
    private IDeviceService deviceService;


    /**
     * 生成随机数字和字母
     */
    @Override
    public String getStringRandom(int length) {
        String val = "";
        Random random = new Random();
        //参数length，表示生成几位随机数
        for(int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            //输出字母还是数字
            if( "char".equalsIgnoreCase(charOrNum) ) {
                //输出是大写字母还是小写字母
                // int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
                val += (char)(random.nextInt(26) + 65);
            } else if( "num".equalsIgnoreCase(charOrNum) ) {
                val += String.valueOf(random.nextInt(10));
            }
        }
        return val;
    }

    /**
     * 注册
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String register(RegisterUserInput registerBody)
    {
        String msg = "";
        String username = registerBody.getUsername();
        String password = registerBody.getPassword();
        String phonenumber=registerBody.getPhonenumber();

        boolean captchaOnOff = configService.selectCaptchaOnOff();
        // 验证码开关
        if (captchaOnOff)
        {
            validateCaptcha(username, registerBody.getCode(), registerBody.getUuid());
        }

        if (StringUtils.isEmpty(username))
        {
            msg = "用户名不能为空";
        }
        else if (StringUtils.isEmpty(password))
        {
            msg = "用户密码不能为空";
        }
        else if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            msg = "账户长度必须在2到20个字符之间";
        }
        else if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            msg = "密码长度必须在5到20个字符之间";
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(username)))
        {
            msg = "保存用户'" + username + "'失败，注册账号已存在";
        }else if (UserConstants.NOT_UNIQUE.equals(checkPhoneUnique(phonenumber)))
        {
            msg = "保存用户'" + username + "'失败，注册手机号码已存在";
        }
        else
        {
            SysUser sysUser = new SysUser();
            sysUser.setUserName(username);
            sysUser.setNickName(username);
            sysUser.setPhonenumber(phonenumber);
            sysUser.setPassword(SecurityUtils.encryptPassword(registerBody.getPassword()));
            boolean regFlag = userService.registerUser(sysUser);
            //分配普通用户角色(1=超级管理员，2=设备租户，3=普通用户，4=游客)
            Long[] roleIds={3L};
            userService.insertUserAuth(sysUser.getUserId(),roleIds);
            if (!regFlag)
            {
                msg = "注册失败,请联系系统管理人员";
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.REGISTER,
                        MessageUtils.message("user.register.success")));
            }
        }
        return msg;
    }

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    public List<SysUser> selectUserList(SysUser user)
    {
        return userMapper.selectUserList(user);
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return
     */
    public String checkPhoneUnique(String phonenumber)
    {
        SysUser info = userMapper.checkPhoneUnique(phonenumber);
        if (StringUtils.isNotNull(info))
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            throw new CaptchaException();
        }
    }

    /**
     * 设备简单认证
     */
    @Override
    public ResponseEntity simpleMqttAuthentication(String emqxVersion,MqttAuthenticationModel mqttModel, ProductAuthenticateModel productModel) {
        // 1=简单认证，2=加密认证，3=简单+加密认证

        //排除认证设备对应的产品不支持简单认证的情况
        if(productModel.getVertificateMethod()!=1 && productModel.getVertificateMethod()!=3){
            return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，设备对应产品不支持简单认证");
        }

        //产品如果启用授权，那么密码构成应该是：密码&授权码，如果密码长度不为2直接返回
        String[] passwordArray = mqttModel.getPassword().split("&");
        if (productModel.getIsAuthorize() == 1 && passwordArray.length != 2) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，产品启用授权码后，密码格式为：密码 & 授权码");
        }
        //产品如果没有开启授权码，那么密码构成应该是：密码，如果密码长度不是1，直接返回
        if(productModel.getIsAuthorize() == 0 && passwordArray.length !=1){
            return returnUnauthorized(emqxVersion,mqttModel,"设备简单认证，产品未启用授权码，密码格式为：密码");
        }
        String mqttPassword = passwordArray[0];
        String authCode = passwordArray.length == 2 ? passwordArray[1] : "";
        // 验证用户名
        if (!mqttModel.getUserName().equals(productModel.getMqttAccount())) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，设备mqtt用户名错误");
        }
        // 验证密码
        if (!mqttPassword.equals(productModel.getMqttPassword())) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，设备mqtt密码错误");
        }
        // 验证授权码
        if (productModel.getIsAuthorize() == 1) {
            // 授权码验证和处理
            String resultMessage = authCodeProcess(authCode, mqttModel, productModel);
            if (!resultMessage.equals("")) {
                return returnUnauthorized(emqxVersion,mqttModel, resultMessage);
            }
        }
        if (productModel.getDeviceId() != null && productModel.getDeviceId() != 0) {
            if (productModel.getStatus() == 2) {
                return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，设备处于禁用状态");
            }
            log.info("设备简单认证成功,clientId:{}", mqttModel.getClientId());
            return returnAuthorized(emqxVersion);
        } else {
            // 自动添加设备
            int result = deviceService.insertDeviceAuto(mqttModel.getDeviceNumber(), mqttModel.getUserId(), mqttModel.getProductId());
            if (result == 1) {
                log.info("设备简单认证成功,并自动添加设备到系统，clientId:{}", mqttModel.getClientId());
                return returnAuthorized(emqxVersion);
            }
            return returnUnauthorized(emqxVersion,mqttModel, "设备简单认证，自动添加设备失败");
        }
    }

    /**
     * 设备加密认证
     *
     * @return
     */
    @Override
    public ResponseEntity encryptAuthentication(String emqxVersion,MqttAuthenticationModel mqttModel, ProductAuthenticateModel productModel) throws Exception {
        /**
         * 1=简单认证，2=加密认证，3=简单+加密认证
         */

        //排除设备对应的产品不支持加密认证的情况
        if(productModel.getVertificateMethod()!=2 && productModel.getVertificateMethod()!=3){
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，设备对应产品不支持加密认证");
        }

        //排除解密失败情况
        String decryptPassword = AESUtils.decrypt(mqttModel.getPassword(), productModel.getMqttSecret());
        if (decryptPassword == null || decryptPassword.equals("")) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，mqtt密码解密失败");
        }

        //排除授权模式，但是密码构成不是密码 & 过期时间 & 授权码
        String[] passwordArray = decryptPassword.split("&");
        if ( productModel.getIsAuthorize()==1 && passwordArray.length != 3) {
            // 密码加密格式 password & expireTime (& authCode 可选)
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密授权认证，mqtt密码加密格式为：密码 & 过期时间 & 授权码");
        }

        //排除非授权模式，但是密码构成不是密码 & 过期时间
        if ( productModel.getIsAuthorize()==0 && passwordArray.length != 2) {
            // 密码加密格式 password & expireTime (& authCode 可选)
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密授权认证，mqtt密码加密格式为：密码 & 过期时间");
        }

        String mqttPassword = passwordArray[0];
        Long expireTime = Long.valueOf(passwordArray[1]);
        String authCode = passwordArray.length == 3 ? passwordArray[2] : "";

        // 验证用户吗错误情况，不通过
        if (!mqttModel.getUserName().equals(productModel.getMqttAccount())) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，设备mqtt用户名错误");
        }

        // 验证密码错误情况，不通过
        if (!mqttPassword.equals(productModel.getMqttPassword())) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，设备mqtt密码错误");
        }

        // 验证授权码失效，超时不通过
        if (expireTime < System.currentTimeMillis()) {
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，设备mqtt密码已过期");
        }
        // 验证授权码
        if (productModel.getIsAuthorize() == 1) {
            // 授权码验证和处理
            String resultMessage = authCodeProcess(authCode, mqttModel, productModel);
            if (!resultMessage.equals("")) {
                return returnUnauthorized(emqxVersion,mqttModel, resultMessage);
            }
        }
        // 设备状态验证 （1-未激活，2-禁用，3-在线，4-离线）
        if (productModel.getDeviceId() != null && productModel.getDeviceId() != 0) {
            if (productModel.getStatus() == 2) {
                return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，设备处于禁用状态");
            }
            log.info("-----------设备加密认证成功,clientId:" + mqttModel.getClientId() + "---------------");
            return returnAuthorized(emqxVersion);
        } else {
            // 自动添加设备
            int result = deviceService.insertDeviceAuto(mqttModel.getDeviceNumber(), mqttModel.getUserId(), mqttModel.getProductId());
            if (result == 1) {
                log.info("-----------设备加密认证成功,并自动添加设备到系统，clientId:" + mqttModel.getClientId() + "---------------");
                return returnAuthorized(emqxVersion);
            }
            return returnUnauthorized(emqxVersion,mqttModel, "设备加密认证，自动添加设备失败");
        }
    }

    /**
     * 授权码认证和处理
     */
    private String authCodeProcess(String authCode, MqttAuthenticationModel mqttModel, ProductAuthenticateModel productModel) {
        String message = "";
        if (authCode.equals("")) {
            return message = "设备认证，设备授权码不能为空";
        }
        // 查询授权码是否存在
        ProductAuthorize authorize = productAuthorizeMapper.selectFirstAuthorizeByAuthorizeCode(new ProductAuthorize(authCode, productModel.getProductId()));
        if (authorize == null) {
            message = "设备认证，设备授权码不存在";
            return message;
        }
        //如果授权码绑定了设备且必须是当前设备才能通过
        if (authorize.getSerialNumber() != null && !authorize.getSerialNumber().equals("")) {
            if (!authorize.getSerialNumber().equals( productModel.getSerialNumber())) {
                message = "设备认证，设备授权码已经分配给其他设备";
                return message;
            }
        }
        //如果授权码没有绑定设备
        else {
            authorize.setSerialNumber(mqttModel.getDeviceNumber());
            authorize.setUserId(mqttModel.getUserId());
            authorize.setUserName("");
            authorize.setUpdateTime(DateUtils.getNowDate());
            // 状态（1-未使用，2-已绑定）
            authorize.setStatus(2);
            int result = productAuthorizeMapper.updateProductAuthorize(authorize);
            if (result != 1) {
                message = "设备认证，设备授权码关联失败";
                return message;
            }
        }
        return message;
    }

    /**
     * 返回认证失败信息
     */
    @Override
    public ResponseEntity returnUnauthorized(String emqxVersion,MqttAuthenticationModel mqttModel, String message) {
        log.warn("认证失败：" + message
                + "\nclientid:" + mqttModel.getClientId()
                + "\nusername:" + mqttModel.getUserName()
                + "\npassword:" + mqttModel.getPassword());
        if(emqxVersion.equals("v4")){
            return ResponseEntity.status(401).body("Unauthorized");
        }else{
            JSONObject ret = new JSONObject();
            ret.put("is_superuser", false);
            ret.put("result", "deny");
            return ResponseEntity.ok().body(ret);
        }
    }

    /**
     * 返回认证成功信息
     */
    @Override
    public ResponseEntity returnAuthorized(String emqxVersion) {
        if(emqxVersion.equals("v4")) {
            return ResponseEntity.ok().body("ok");
        }else{
            JSONObject ret = new JSONObject();
            ret.put("is_superuser", false);
            ret.put("result", "allow");
            return ResponseEntity.ok().body(ret);
        }
    }


    /**
     * http 设备认证
     */
    @Override
    public ResponseEntity clientAuth(String emqxVersion, String clientid, String username, String password) throws Exception {
        if (clientid.startsWith("server")) {
            // 服务端认证：配置的账号密码认证
            if (mqttConfig.getusername().equals(username) && mqttConfig.getpassword().equals(password)) {
                return returnAuthorized(emqxVersion);
            } else {
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), "mqtt账号和密码与认证服务器配置不匹配");
            }
        } else if (clientid.startsWith("web") || clientid.startsWith("phone")) {
            // web端和移动端认证：token认证
            String token = password;
            if (StringUtils.isNotEmpty(token) && token.startsWith(Constants.TOKEN_PREFIX)) {
                token = token.replace(Constants.TOKEN_PREFIX, "");
            }
            try {
                log.info("-----------移动端/Web端mqtt认证成功,clientId:" + clientid + "---------------");
                return returnAuthorized(emqxVersion);
            } catch (Exception ex) {
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), ex.getMessage());
            }
        } else {
            // 设备端认证：加密认证（E）和简单认证（S，配置的账号密码认证）
            String[] clientArray = clientid.split("&");
            if(clientArray.length != 4 || clientArray[0].equals("") || clientArray[1].equals("") || clientArray[2].equals("") || clientArray[3].equals("")){
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), "设备mqtt客户端Id格式为：认证类型 & 设备编号 & 产品ID & 用户ID");
            }
            String authType = clientArray[0];
            String deviceNumber = clientArray[1];
            Long productId = Long.valueOf(clientArray[2]);
            Long userId = Long.valueOf(clientArray[3]);

            // 查询产品信息以及关联出对应的设备信息（根据deviceNumber过滤）
            ProductAuthenticateModel model = deviceService.selectProductAuthenticate(new AuthenticateInputModel(deviceNumber, productId));
            //找不到对应的产品验证不通过
            if (model == null) {
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), "设备认证，通过产品ID查询不到信息");
            }
            //产品处于未发布的状态验证不通过
            if (model.getProductStatus() != 2) {
                // 产品必须为发布状态：1-未发布，2-已发布
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), "设备认证，设备对应产品还未发布");
            }
            //简单认证
            if (authType.equals("S")) {
                // 设备简单认证
                return simpleMqttAuthentication(emqxVersion,new MqttAuthenticationModel(clientid, username, password, deviceNumber, productId, userId), model);

            }
            //加密认证
            else if (authType.equals("E")) {
                // 设备加密认证
                return encryptAuthentication(emqxVersion,new MqttAuthenticationModel(clientid, username, password, deviceNumber, productId, userId), model);
            }
            //其他情况验证不通过
            else {
                return returnUnauthorized(emqxVersion,new MqttAuthenticationModel(clientid, username, password), "设备认证，认证类型有误");
            }
        }
    }

    @Override
    public void hook(String clientid,String event,String IpAddress){
        try {
            // 过滤服务端、web端和手机端
            if (clientid.startsWith("server") || clientid.startsWith("web") || clientid.startsWith("phone")) {
                return;
            }
            // 设备端认证：加密认证（E）和简单认证（S，配置的账号密码认证）
            String[] clientArray = clientid.split("&");
            String authType = clientArray[0];
            String deviceNumber = clientArray[1];
            Long productId = Long.valueOf(clientArray[2]);
            Long userId = Long.valueOf(clientArray[3]);

            Device device = deviceService.selectShortDeviceBySerialNumber(deviceNumber);
            // 设备状态（1-未激活，2-禁用，3-在线，4-离线）
            if (event.equals("client_disconnected") || event.equals("client.disconnected")) {
                //更新数据库设备状态为离线
                device.setStatus(4);
                deviceService.updateDeviceStatusAndLocation(device, "");
                // 设备掉线后发布设备状态
                emqxService.publishStatus(device.getProductId(), device.getSerialNumber(), 4, device.getIsShadow(),device.getRssi());
                // 发布空消息
                emqxService.publishProperty(device.getProductId(), device.getSerialNumber(), null,0);
                emqxService.publishFunction(device.getProductId(), device.getSerialNumber(), null,0);
                // 规则匹配处理（告警和场景联动）
                emqxService.ruleMatch(device.getProductId(), device.getSerialNumber(),null,6);
            } else if (event.equals("client_connected") || event.equals("client.connected")) {
                //更新数据库设备状态为在线
                device.setStatus(3);
                deviceService.updateDeviceStatusAndLocation(device, IpAddress);
                //上线后发布设备状态
                emqxService.publishStatus(device.getProductId(), device.getSerialNumber(), 3, device.getIsShadow(),device.getRssi());
                // 影子模式，发布属性和功能
                if (device.getIsShadow() == 1) {
                    ThingsModelShadow shadow = deviceService.getDeviceShadowThingsModel(device);
                    if (shadow.getProperties().size() > 0) {
                        emqxService.publishProperty(device.getProductId(), device.getSerialNumber(), shadow.getProperties(),3);
                    }
                    if (shadow.getFunctions().size() > 0) {
                        emqxService.publishFunction(device.getProductId(), device.getSerialNumber(), shadow.getFunctions(),3);
                    }
                }
                // 规则匹配处理（告警和场景联动）
                emqxService.ruleMatch(device.getProductId(), device.getSerialNumber(),null,5);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("发生错误：" + ex.getMessage());
        }
    }
}
