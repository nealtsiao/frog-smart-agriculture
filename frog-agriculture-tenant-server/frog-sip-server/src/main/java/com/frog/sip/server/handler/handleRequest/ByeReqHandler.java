package com.frog.sip.server.handler.handleRequest;

import com.frog.sip.server.handler.IReqHandler;
import com.frog.sip.server.listener.IGBListener;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.server.transcation.SipSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.RequestEvent;
import javax.sip.message.Response;

@Slf4j
@Component
public class ByeReqHandler  implements IReqHandler, InitializingBean {
    @Autowired
    private HeaderBuilder headerBuilder;
    @Autowired
    private SipSender sipSender;
    @Autowired
    private IGBListener sipListener;
    @Override
    public void processMsg(RequestEvent evt) {
        /* 设备主动发过来的信息不能从缓存中去，因为设备新建立了dialog，因为你重启导致之前的消息dialog不存在了，设备主动结束推流 */
//            log.info("超大眼睛看清楚，设备大爷发送bye请求过来了{}",evt.getRequest().toString());
//            Response okResponse = headerBuilder.createOkResponse(evt.getRequest());
//            sipSender.tansmitByServerTanscation(evt.getServerTransaction(),okResponse);

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = "BYE";
        sipListener.addRequestProcessor(method, this);
    }
}
