package com.frog.sip.service;

import com.frog.sip.domain.SipConfig;

import java.util.List;

/**
 * sip系统配置Service接口
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
public interface ISipConfigService
{
    int updateDefaultSipConfig(SipConfig sipConfig);
    /**
     * 查询产品下第一条sip系统配置
     *
     * @param productId sip系统配置主键
     * @return sip系统配置
     */
    List<SipConfig> selectSipConfigByProductId(Long productId);
    List<SipConfig> selectSipConfigByUserId(Long userId);
    SipConfig selectSipConfigBydeviceSipId(String deviceSipId);
    /**
     * 查询sip系统配置列表
     *
     * @param sipConfig sip系统配置
     * @return sip系统配置集合
     */
    List<SipConfig> selectSipConfigList(SipConfig sipConfig);

    /**
     * 新增sip系统配置
     *
     * @param sipConfig sip系统配置
     * @return 结果
     */
    int insertSipConfig(SipConfig sipConfig);

    /**
     * 修改sip系统配置
     *
     * @param sipConfig sip系统配置
     * @return 结果
     */
    int updateSipConfig(SipConfig sipConfig);

    /**
     * 批量删除sip系统配置
     *
     * @param ids 需要删除的sip系统配置主键集合
     * @return 结果
     */
    int deleteSipConfigByIds(Long[] ids);
    int deleteSipConfigByProductIds(Long[] productIds);
    /**
     * 删除sip系统配置信息
     *
     * @param id sip系统配置主键
     * @return 结果
     */
    int deleteSipConfigById(Long id);
}
