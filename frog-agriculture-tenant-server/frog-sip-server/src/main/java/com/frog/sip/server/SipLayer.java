package com.frog.sip.server;

import com.frog.sip.server.conf.SysSipConfig;
import com.frog.sip.domain.MediaServer;
import com.frog.sip.server.listener.IGBListener;
import com.frog.sip.server.model.ZlmMediaServer;
import com.frog.sip.service.IMediaServerService;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.server.util.ZlmApiUtil;
import gov.nist.javax.sip.SipStackImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.sip.*;
import java.util.Properties;

@Slf4j
@Configuration
public class SipLayer {
    @Autowired
    private SysSipConfig sipConfig;

    @Autowired
    private IGBListener gbSIPListener;

    @Autowired
    private ZlmApiUtil zlmApiUtil;

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private IMediaServerService mediaServerService;
    private SipStack sipStack;
    private SipFactory sipFactory;
    @Bean("sipFactory")
    SipFactory createSipFactory() {
        sipFactory = SipFactory.getInstance();
        sipFactory.setPathName("gov.nist");
        if (sipConfig.isEnabled()) {
            //缓存zlm服务器配置
            MediaServer media = mediaServerService.selectMediaServerBytenantId(1L);
            ZlmMediaServer mediaServer = zlmApiUtil.getMediaServerConfig(media);
            if (mediaServer != null) {
                sipCacheService.addMediaInfo(mediaServer);
            }
        }
        return sipFactory;
    }

    @Bean("sipStack")
    @DependsOn("sipFactory")
    SipStack createSipStack() throws PeerUnavailableException {
        Properties properties = new Properties();
        properties.setProperty("javax.sip.STACK_NAME", "GB28181_SIP");
        properties.setProperty("javax.sip.IP_ADDRESS", sipConfig.getIp());
        properties.setProperty("gov.nist.javax.sip.LOG_MESSAGE_CONTENT", "false");
        properties.setProperty("gov.nist.javax.sip.TRACE_LEVEL", "0");
        properties.setProperty("gov.nist.javax.sip.SERVER_LOG", "sip_server_log");
        properties.setProperty("gov.nist.javax.sip.DEBUG_LOG", "sip_debug_log");
        sipStack = (SipStackImpl) sipFactory.createSipStack(properties);
        return sipStack;
    }

    @Bean("udpSipServer")
    @DependsOn("sipStack")
    SipProvider startUdpListener() throws Exception {
        if (sipConfig.isEnabled()) {
            ListeningPoint udpListeningPoint = sipStack.createListeningPoint(sipConfig.getIp(), sipConfig.getPort(), "UDP");
            SipProvider udpSipProvider = sipStack.createSipProvider(udpListeningPoint);
            udpSipProvider.addSipListener(gbSIPListener);
            log.info("[2-sip服务在udp端口{}启动成功]", sipConfig.getPort());
            return udpSipProvider;
        } else {
            return new NullSipProvider();
        }
    }

}
