package com.frog.iot.mapper;

import java.util.List;
import com.frog.iot.domain.YsDeviceChannel;

/**
 * 萤石云设备通道Mapper接口
 * 
 * @author nealtsiao
 * @date 2024-11-25
 */
public interface YsDeviceChannelMapper 
{
    /**
     * 查询萤石云设备通道
     * 
     * @param channelId 萤石云设备通道主键
     * @return 萤石云设备通道
     */
    public YsDeviceChannel selectYsDeviceChannelByChannelId(Long channelId);

    /**
     * 查询萤石云设备通道列表
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 萤石云设备通道集合
     */
    public List<YsDeviceChannel> selectYsDeviceChannelList(YsDeviceChannel ysDeviceChannel);

    /**
     * 新增萤石云设备通道
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 结果
     */
    public int insertYsDeviceChannel(YsDeviceChannel ysDeviceChannel);

    /**
     * 修改萤石云设备通道
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 结果
     */
    public int updateYsDeviceChannel(YsDeviceChannel ysDeviceChannel);

    /**
     * 删除萤石云设备通道
     * 
     * @param channelId 萤石云设备通道主键
     * @return 结果
     */
    public int deleteYsDeviceChannelByChannelId(Long channelId);

    /**
     * 批量删除萤石云设备通道
     * 
     * @param channelIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteYsDeviceChannelByChannelIds(Long[] channelIds);

    /**
     * 根据产品名称删除通道
     * @param productId
     * @return
     */
    public int deleteYsDeviceChannelByProductId(Long productId);
}
