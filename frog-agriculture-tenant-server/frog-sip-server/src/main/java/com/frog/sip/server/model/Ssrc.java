package com.frog.sip.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author neal
 * @date 2024/12/23  15:08
 */
@Data
public class Ssrc {
    private String ssrc;
    private int isUsed;//0未使用 1使用
}
