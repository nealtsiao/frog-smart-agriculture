package com.frog.sip.server.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 定义摄像头的类型，SIP协议，萤石云协议,这边与字典iot_device_type保持一直
 * @author neal
 * @date 2024/11/30  16:25
 */
@Getter
@AllArgsConstructor
public enum CameraType {
    //国标摄像头
    SIP(3,"sip","sipPlay","sipPtz","sipRecord"),
    //萤石云摄像头
    YS(4,"ys","ysPlay","ysPtz","ysRecord");




    private Integer type;
    private String platform;
    private String playBeanName;
    private String ptzBeanName;
    private String recordBeanName;
}
