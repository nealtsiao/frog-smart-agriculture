package com.frog.sip.service.impl;

import com.frog.common.core.domain.entity.SysRole;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.utils.DateUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.sip.domain.MediaServer;
import com.frog.sip.mapper.MediaServerMapper;
import com.frog.sip.service.IMediaServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.frog.common.utils.SecurityUtils.getLoginUser;

/**
 * 流媒体服务器配置Service业务层处理
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
@Service
public class MediaServerServiceImpl implements IMediaServerService
{
    @Autowired
    private MediaServerMapper mediaServerMapper;
    @Autowired
    private DeviceMapper deviceMapper;
    /**
     * 查询流媒体服务器配置
     *
     * @param id 流媒体服务器配置主键
     * @return 流媒体服务器配置
     */
    @Override
    public List<MediaServer> selectMediaServer()
    {
        MediaServer mediaServer=new MediaServer();
        SysUser user = getLoginUser().getUser();
        List<SysRole> roles = user.getRoles();
        for (int i = 0; i < roles.size(); i++) {
            if (roles.get(i).getRoleKey().equals("tenant")) {
                // 租户查看第一条流媒体服务器配置
                mediaServer.setTenantId(user.getUserId());
            }
        }
        return mediaServerMapper.selectMediaServer(mediaServer);
    }

    @Override
    public MediaServer selectMediaServerBytenantId(Long tenantId) {
        return mediaServerMapper.selectMediaServerBytenantId(tenantId);
    }

    @Override
    public MediaServer getMediaInfo() {
            return mediaServerMapper.selectMediaServerBytenantId(1L);
    }

    /**
     * 查询流媒体服务器配置列表
     *
     * @param mediaServer 流媒体服务器配置
     * @return 流媒体服务器配置
     */
    @Override
    public List<MediaServer> selectMediaServerList(MediaServer mediaServer)
    {
        return mediaServerMapper.selectMediaServerList(mediaServer);
    }

    /**
     * 新增流媒体服务器配置
     *
     * @param mediaServer 流媒体服务器配置
     * @return 结果
     */
    @Override
    public int insertMediaServer(MediaServer mediaServer)
    {
        mediaServer.setCreateTime(DateUtils.getNowDate());
        SysUser user = getLoginUser().getUser();
        mediaServer.setTenantId(user.getUserId());
        return mediaServerMapper.insertMediaServer(mediaServer);
    }

    /**
     * 修改流媒体服务器配置
     *
     * @param mediaServer 流媒体服务器配置
     * @return 结果
     */
    @Override
    public int updateMediaServer(MediaServer mediaServer)
    {
        mediaServer.setUpdateTime(DateUtils.getNowDate());
        return mediaServerMapper.updateMediaServer(mediaServer);
    }

    /**
     * 批量删除流媒体服务器配置
     *
     * @param ids 需要删除的流媒体服务器配置主键
     * @return 结果
     */
    @Override
    public int deleteMediaServerByIds(Long[] ids)
    {
        return mediaServerMapper.deleteMediaServerByIds(ids);
    }

    /**
     * 删除流媒体服务器配置信息
     *
     * @param id 流媒体服务器配置主键
     * @return 结果
     */
    @Override
    public int deleteMediaServerById(Long id)
    {
        return mediaServerMapper.deleteMediaServerById(id);
    }
}
