package com.frog.sip.server.model;

import lombok.Data;

@Data
public class RecordInput {
    String deviceId;
    String channelId;
    String startTime;
    String endTime;
}
