package com.frog.sip.server.handler.handleRequest;

import com.frog.common.utils.DateUtils;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.enums.DeviceChannelStatus;
import com.frog.sip.server.enums.SipDeviceStatus;
import com.frog.sip.server.handler.IReqHandler;
import com.frog.sip.server.model.RecordItem;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.server.listener.IGBListener;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.RecordCacheManager;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.server.transcation.SipSender;
import com.frog.sip.service.IMqttService;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.ISipDeviceChannelService;
import com.frog.sip.service.ISipDeviceService;
import com.frog.sip.server.util.SipUtil;
import com.frog.sip.server.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.sip.RequestEvent;
import javax.sip.SipException;
import javax.sip.header.ViaHeader;
import javax.sip.message.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import static com.frog.common.constant.SipConstants.*;

@Slf4j
@Component
public class MessageReqHandler  implements InitializingBean, IReqHandler {

    @Autowired
    private IGBListener sIPListener;

    @Autowired
    private ISipCmd sipCmd;

    @Autowired
    private ISipDeviceService sipDeviceService;

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private IMqttService mqttService;

    @Autowired
    private RecordCacheManager recordCacheManager;

    @Autowired
    private ISipDeviceChannelService sipDeviceChannelService;
    @Autowired
    private HeaderBuilder headerBuilder;
    @Autowired
    SipSender sipSender;

    @Override
    public void processMsg(RequestEvent evt) {
        try {
            log.info(evt.getRequest().toString());
            //先回复
            Response okResponse = headerBuilder.createOkResponse(evt.getRequest());
            sipSender.transmit(okResponse);
            if (evt.getRequest().getContentLength().getContentLength() > 0) {
                Element rootElement = SipUtil.getRootElement(evt);
                String cmd = XmlUtil.getText(rootElement, "CmdType");
                if (MESSAGE_KEEP_ALIVE.equals(cmd)) {
                    processMessageKeepAlive(evt);
                } else if (MESSAGE_CATALOG.equals(cmd)) {
                    processMessageCatalogList(evt);
                } else if (MESSAGE_DEVICE_INFO.equals(cmd)) {
                    processMessageDeviceInfo(evt);
                } else if (MESSAGE_RECORD_INFO.equals(cmd)) {
                    processMessageRecordInfo(evt);
                } else if (MESSAGE_MEDIA_STATUS.equals(cmd)) {
                    processMessageMediaStatus(evt);
                }
            }
        } catch (DocumentException | SipException e) {
            throw new RuntimeException(e);
        }


    }

    private void processMessageKeepAlive(RequestEvent evt) {
        try {
            Element rootElement = SipUtil.getRootElement(evt);
            String deviceId = XmlUtil.getText(rootElement, "DeviceID");
            if (sipDeviceService.exists(deviceId)) {
                ViaHeader viaHeader = (ViaHeader) evt.getRequest().getHeader(ViaHeader.NAME);
                String received = viaHeader.getReceived();
                int rPort = viaHeader.getRPort();
                if (StringUtils.isEmpty(received) || rPort == -1) {
                    received = viaHeader.getHost();
                    rPort = viaHeader.getPort();
                }
                SipDevice sipDevice = sipDeviceService.selectSipDeviceBySipId(deviceId);
                if (sipDevice == null) {
                    log.info("sipDevice:{},不存在！", deviceId);
                    return;
                }
                sipDevice.setDeviceSipId(deviceId);
                sipDevice.setLastconnecttime(DateUtils.getNowDate());
                sipDevice.setOnline(SipDeviceStatus.online.getStatus());
                sipDevice.setIp(received);
                sipDevice.setPort(rPort);
                sipDevice.setHostaddress(received.concat(":").concat(String.valueOf(rPort)));
                // 更新sip_device在线状态
                sipDeviceService.updateSipDeviceStatus(sipDevice);
                // 更新deivce在线状态到emqx
                mqttService.publishSipDeviceInfoToInfoPost(sipDevice);
                //发送cataLog到设备来更新通道在线状态
                sipCmd.catalogQuery(sipDevice);
            }

        } catch ( DocumentException e) {
            e.printStackTrace();
        }
    }

    private void processMessageCatalogList(RequestEvent evt) {
        try {
            Element rootElement = SipUtil.getRootElement(evt);
            Element deviceIdElement = rootElement.element("DeviceID");
            String sipId = deviceIdElement.getText();
            Element deviceListElement = rootElement.element("DeviceList");
            if (deviceListElement == null) {
                return;
            }
            Iterator<Element> deviceListIterator = deviceListElement.elementIterator();
            List<SipDeviceChannel> channels = new ArrayList<>();
            if (deviceListIterator != null) {
                SipDevice device = sipDeviceService.selectSipDeviceBySipId(sipId);
                if (device == null) {
                    return;
                }
                // 遍历DeviceList
                while (deviceListIterator.hasNext()) {
                    Element itemDevice = deviceListIterator.next();
                    Element channelDeviceElement = itemDevice.element("DeviceID");
                    if (channelDeviceElement == null) {
                        continue;
                    }
                    String channelDeviceId = channelDeviceElement.getText();
                    Element channdelNameElement = itemDevice.element("Name");
                    String channelName = channdelNameElement != null ? channdelNameElement.getTextTrim() : "";
                    Element statusElement = itemDevice.element("Status");
                    String status = statusElement != null ? statusElement.getText() : "ON";
                    //找到设备下所有的通道
                    List<SipDeviceChannel> sipDeviceChannels = sipDeviceChannelService.selectSipDeviceChannelByDeviceSipId(sipId);

                    //SipDeviceChannel deviceChannel = sipDeviceChannelService.selectSipDeviceChannelByChannelSipId(channelDeviceId);
                    for(SipDeviceChannel deviceChannel : sipDeviceChannels){
                        if(deviceChannel.getChannelSipId().equals(channelDeviceId)){
                            if (deviceChannel == null) {
                                deviceChannel = new SipDeviceChannel();
                            }
                            deviceChannel.setDeviceSipId(device.getDeviceSipId());
                            deviceChannel.setChannelName(channelName);
                            deviceChannel.setChannelSipId(channelDeviceId);
                            // ONLINE OFFLINE  HIKVISION DS-7716N-E4 NVR的兼容性处理
                            if (status.equals("ON") || status.equals("On") || status.equals("ONLINE")) {
                                deviceChannel.setStatus(DeviceChannelStatus.online.getValue());
                            }
                            if (status.equals("OFF") || status.equals("Off") || status.equals("OFFLINE")) {
                                deviceChannel.setStatus(DeviceChannelStatus.offline.getValue());
                            }
                            if (deviceChannel.getRegisterTime() == null) {
                                deviceChannel.setRegisterTime(DateUtils.getNowDate());
                            }
                            deviceChannel.setManufacture(XmlUtil.getText(itemDevice, "Manufacturer"));
                            deviceChannel.setModel(XmlUtil.getText(itemDevice, "Model"));
                            deviceChannel.setOwner(XmlUtil.getText(itemDevice, "Owner"));
                            deviceChannel.setCivilcode(XmlUtil.getText(itemDevice, "CivilCode"));
                            deviceChannel.setBlock(XmlUtil.getText(itemDevice, "Block"));
                            deviceChannel.setAddress(XmlUtil.getText(itemDevice, "Address"));
                            if (XmlUtil.getText(itemDevice, "Parental") == null || Objects.equals(XmlUtil.getText(itemDevice, "Parental"), "")) {
                                deviceChannel.setParental(0);
                            } else {
                                deviceChannel.setParental(Integer.parseInt(XmlUtil.getText(itemDevice, "Parental")));
                            }
                            deviceChannel.setParentid(XmlUtil.getText(itemDevice, "ParentID"));

                            deviceChannel.setIpaddress(XmlUtil.getText(itemDevice, "IPAddress"));
                            if (XmlUtil.getText(itemDevice, "Port") == null || Objects.equals(XmlUtil.getText(itemDevice, "Port"), "")) {
                                deviceChannel.setPort(0);
                            } else {
                                deviceChannel.setPort(Integer.parseInt(XmlUtil.getText(itemDevice, "Port")));
                            }
                            deviceChannel.setPassword(XmlUtil.getText(itemDevice, "Password"));
                            if (XmlUtil.getText(itemDevice, "Longitude") == null || Objects.equals(XmlUtil.getText(itemDevice, "Longitude"), "")) {
                                deviceChannel.setLongitude(BigDecimal.valueOf(0.00));
                            } else {
                                deviceChannel.setLongitude(BigDecimal.valueOf(Double.parseDouble(XmlUtil.getText(itemDevice, "Longitude"))));
                            }
                            if (XmlUtil.getText(itemDevice, "Latitude") == null || Objects.equals(XmlUtil.getText(itemDevice, "Latitude"), "")) {
                                deviceChannel.setLatitude(BigDecimal.valueOf(0.00));
                            } else {
                                deviceChannel.setLatitude(BigDecimal.valueOf(Double.parseDouble(XmlUtil.getText(itemDevice, "Latitude"))));
                            }
                            if (XmlUtil.getText(itemDevice, "PTZType") == null || Objects.equals(XmlUtil.getText(itemDevice, "PTZType"), "")) {
                                deviceChannel.setPtztype(0L);
                            } else {
                                deviceChannel.setPtztype((long) Integer.parseInt(XmlUtil.getText(itemDevice, "PTZType")));
                            }
                            deviceChannel.setHasaudio(1);// 默认含有音频，播放时再检查是否有音频及是否AAC
                        }else{
                            if(deviceChannel.getStatus()!=1) {
                                deviceChannel.setStatus(DeviceChannelStatus.offline.getValue());
                            }
                        }
                        //更新到数据库
                        sipDeviceChannelService.updateChannel(device.getDeviceSipId(), deviceChannel);
                        channels.add(deviceChannel);
                    }
                }
            }

        } catch ( DocumentException e) {
            e.printStackTrace();
        }
    }

    private void processMessageDeviceInfo(RequestEvent evt) {
        try {
            Element rootElement = SipUtil.getRootElement(evt);
            Element deviceIdElement = rootElement.element("DeviceID");
            String sipId = deviceIdElement.getText();
            SipDevice device = sipDeviceService.selectSipDeviceBySipId(sipId);
            if (device == null) {
                device = new SipDevice();
                device.setDeviceSipId(sipId);
            }
            device.setDeviceName(XmlUtil.getText(rootElement, "DeviceName"));
            device.setManufacturer(XmlUtil.getText(rootElement, "Manufacturer"));
            device.setModel(XmlUtil.getText(rootElement, "Model"));
            device.setFirmware(XmlUtil.getText(rootElement, "Firmware"));
            if (StringUtils.isEmpty(device.getStreammode())) {
                device.setStreammode("UDP");
            }
            // 更新sip设备到数据库
            sipDeviceService.updateDevice(device);
            // 发布设备info到emqx
            mqttService.publishSipDeviceInfoToInfoPost(device);

        } catch (DocumentException  e) {
            e.printStackTrace();
        }

    }

    private void processMessageRecordInfo(RequestEvent evt) {
        try {
            Element rootElement = SipUtil.getRootElement(evt);
            String deviceId = rootElement.element("DeviceID").getText();
            String sn = XmlUtil.getText(rootElement, "SN");
            String sumNum = XmlUtil.getText(rootElement, "SumNum");
            String recordkey = deviceId + ":" + sn;
            ReentrantLock lock = recordCacheManager.getlock(recordkey);
            if (lock == null) {
                return;
            }
            lock.lock();
            RecordList recordList = recordCacheManager.getRecordList(recordkey);
            if (recordList != null) {
                int recordnum = 0;
                recordList.setDeviceID(deviceId);
                Element recordListElement = rootElement.element("RecordList");
                if (recordListElement == null || sumNum == null || sumNum.equals("")) {
                    log.info("无录像数据");
                } else {
                    Iterator<Element> recordListIterator = recordListElement.elementIterator();
                    if (recordListIterator != null) {
                        while (recordListIterator.hasNext()) {
                            Element itemRecord = recordListIterator.next();
                            Element recordElement = itemRecord.element("DeviceID");
                            if (recordElement == null) {
                                continue;
                            }
                            RecordItem item = new RecordItem();
                            item.setStart(SipUtil.ISO8601Totimestamp(XmlUtil.getText(itemRecord, "StartTime")));
                            item.setEnd(SipUtil.ISO8601Totimestamp(XmlUtil.getText(itemRecord, "EndTime")));
                            recordList.addItem(item);
                            recordnum++;
                        }
                    }
                    if (recordList.getSumNum() + recordnum == Integer.parseInt(sumNum)) {
                        //时间合并 保存到redis
                        recordList.mergeItems();
                        log.info("mergeItems recordList:{}", recordList);
                        recordCacheManager.remove(recordkey);
                        sipCacheService.setRecordList(recordkey, recordList);
                    } else {
                        recordList.setSumNum(recordList.getSumNum() + recordnum);
                        recordCacheManager.put(recordkey, recordList);
                    }
                }
            } else {
                recordList = new RecordList();
                int recordnum = 0;
                recordList.setDeviceID(deviceId);
                Element recordListElement = rootElement.element("RecordList");
                if (recordListElement == null || sumNum == null || sumNum.equals("")) {
                    log.info("无录像数据");
                } else {
                    Iterator<Element> recordListIterator = recordListElement.elementIterator();
                    if (recordListIterator != null) {
                        while (recordListIterator.hasNext()) {
                            Element itemRecord = recordListIterator.next();
                            Element recordElement = itemRecord.element("DeviceID");
                            if (recordElement == null) {
                                continue;
                            }
                            RecordItem item = new RecordItem();
                            item.setStart(SipUtil.ISO8601Totimestamp(XmlUtil.getText(itemRecord, "StartTime")));
                            item.setEnd(SipUtil.ISO8601Totimestamp(XmlUtil.getText(itemRecord, "EndTime")));
                            recordList.addItem(item);
                            recordnum++;
                        }
                    }
                    if (recordnum == Integer.parseInt(sumNum)) {
                        recordList.setSumNum(recordnum);
                        //时间合并 保存到redia
                        recordList.mergeItems();
                        log.info("mergeItems recordList:{}", recordList);
                        recordCacheManager.remove(recordkey);
                        sipCacheService.setRecordList(recordkey, recordList);
                    } else {
                        recordList.setSumNum(recordnum);
                        recordCacheManager.put(recordkey, recordList);
                    }
                }
            }
            lock.unlock();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void processMessageMediaStatus(RequestEvent evt) {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = "MESSAGE";
        sIPListener.addRequestProcessor(method, this);
    }
}
