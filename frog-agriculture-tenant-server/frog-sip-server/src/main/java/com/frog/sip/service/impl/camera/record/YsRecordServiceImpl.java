package com.frog.sip.service.impl.camera.record;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.iot.util.TimeStampUtils;
import com.frog.iot.util.YsApiUtils;
import com.frog.sip.server.model.RecordItem;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.service.IRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service("ysRecord")
public class YsRecordServiceImpl implements IRecordService {
    @Autowired
    private YsApiUtils ysApiUtils;

    @Override
    public RecordList listDevRecord(String deviceId, String channelId, String start, String end) {
        RecordList recordList = new RecordList();
        recordList.setDeviceID(deviceId);
        String localIndex = channelId.split("-")[1];

        List<RecordItem> allRecordItems = new ArrayList<>();

        LinkedHashMap<String, Object> timeStampList = TimeStampUtils.getAllDayByMonth(start, end);
        for (Map.Entry<String, Object> entry : timeStampList.entrySet()) {
            long[] timestamps = (long[]) entry.getValue();
            String startTime = String.valueOf(timestamps[0]);
            String endTime = String.valueOf(timestamps[1]);
            String records = ysApiUtils.getLocalVideo(deviceId, localIndex, startTime, endTime);

            if (records != null) {
                List<RecordItem> items = new ArrayList<>();
                JSONArray recordsArray = JSONArray.parseArray(records);

                for (Object record : recordsArray) {
                    JSONObject recordObj = (JSONObject) record;
                    RecordItem item = new RecordItem();
                    item.setStart(recordObj.getLong("startTime"));
                    item.setEnd(recordObj.getLong("endTime"));
                    // 将符合条件的RecordItem对象添加到items列表中
                    items.add(item);
                }
                // 将RecordItem对象添加到allRecordItems
                allRecordItems.addAll(items);
            }
        }

        if (!allRecordItems.isEmpty()) {
            //判断回播录像的第一条的开始时间和最后一条的结束时间 是否在当天
            if (allRecordItems.get(0).getStart() < Long.valueOf(start)) {
                allRecordItems.get(0).setStart(Long.valueOf(start));
            }
            if (allRecordItems.get(allRecordItems.size() - 1).getEnd() > Long.valueOf(end)) {
                allRecordItems.get(allRecordItems.size() - 1).setEnd(Long.valueOf(end));
            }
            recordList.setRecordItems(allRecordItems);
            recordList.setSumNum(allRecordItems.size());
            return recordList;
        }

        return null;
    }

}
