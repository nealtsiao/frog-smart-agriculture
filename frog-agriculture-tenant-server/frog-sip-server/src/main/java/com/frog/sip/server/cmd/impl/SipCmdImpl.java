package com.frog.sip.server.cmd.impl;

import com.frog.sip.domain.MediaServer;
import com.frog.sip.domain.SipConfig;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.server.model.DialogInfo;
import com.frog.sip.service.IMediaServerService;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.ISipConfigService;
import com.frog.sip.server.transcation.SipSender;
import com.frog.sip.server.util.PtzCmdUtil;
import com.frog.sip.server.util.SipUtil;
import com.frog.sip.server.util.ZlmApiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sip.*;
import javax.sip.message.Request;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;


@Slf4j
@Component
public class SipCmdImpl implements ISipCmd {
    @Autowired
    private HeaderBuilder headerBuilder;

    @Autowired
    @Qualifier(value = "udpSipServer")
    private SipProvider sipserver;

    @Autowired
    private ISipConfigService sipConfigService;

    @Autowired
    private IMediaServerService mediaServerService;

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private ZlmApiUtil zlmApiUtil;

    @Autowired
    SipSender sipSender;
    public static final String PLAY_TAG = "gb_play";
    public static final String PLAY_BACK_TAG="gb_playback";

    public static final String RECORD_INFO ="record_info";
    public static final String DEVICE_INFO ="device_info";
    public static final String CATE_LOG = "cate_log";

    /**
     * 播放直播流
     * @param device
     * @param channelId
     * @param streamId
     */
    @Override
    public void playStreamCmd(SipDevice device, String channelId, String streamId) {
        try {
            SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(device.getDeviceSipId());
            String ssrc = sipCacheService.getPlaySsrc(sipConfig.getDomain());
            //（udp）
            String streamMode = device.getStreammode().toUpperCase();
            /** 通过deviceId找到iotDevice，再通过iotDevice的tenantId找到mediaServer
             *  目前系统是无论什么账户创建的摄像头，摄像头的tenantId都是1，mediaServer目前仅存在单挑记录，tenantId也是1
             **/
            MediaServer mediaInfo = mediaServerService.getMediaInfo();
            if (mediaInfo == null) {
                log.warn("点播发现不存在zlm的配置信息...");
                return;
            }
            // 创建zlm的rtpServer用来接收摄像头的流
            String mediaPort = zlmApiUtil.createRTPServer(mediaInfo, streamId) + "";//打开zlmedia的RTP接受端口
            if("-1".equals(mediaPort)){
                return;
            }
            //拼接sip指令
            StringBuilder content = new StringBuilder(200);
            content.append("v=0\r\n");//表示协议版本
            content.append("o=" + "00000" + " 0 0 IN IP4 ").append(mediaInfo.getIp()).append("\r\n");//会话的原始者/创建者
            content.append("s=Play\r\n");//会话的主题名称
            content.append("c=IN IP4 ").append(mediaInfo.getIp()).append("\r\n");//表示会话的连接信息 IN网络类型  IP4地址类型
            content.append("t=0 0\r\n");//会话时间  0 0 表示一个及时的，非持久的会话

            if (sipConfig.getSeniorsdp() != null && sipConfig.getSeniorsdp() == 1) {
                switch (streamMode) {
                    case "TCP-PASSIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 126 125 99 34 98 97\r\n");
                        break;
                    case "TCP-ACTIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 126 125 99 34 98 97\r\n");
                        break;
                    case "UDP":
                        content.append("m=video ").append(mediaPort).append(" RTP/AVP 96 126 125 99 34 98 97\r\n");
                        break;
                }
                content.append("a=recvonly\r\n");
                content.append("a=rtpmap:96 PS/90000\r\n");
                content.append("a=fmtp:126 profile-level-id=42e01e\r\n");
                content.append("a=rtpmap:126 H264/90000\r\n");
                content.append("a=rtpmap:125 H264S/90000\r\n");
                content.append("a=fmtp:125 profile-level-id=42e01e\r\n");
                content.append("a=rtpmap:99 MP4V-ES/90000\r\n");
                content.append("a=fmtp:99 profile-level-id=3\r\n");
                content.append("a=rtpmap:98 H264/90000\r\n");
                content.append("a=rtpmap:97 MPEG4/90000\r\n");
                if ("TCP-PASSIVE".equals(streamMode)) { // tcp被动模式
                    content.append("a=setup:passive\r\n");
                    content.append("a=connection:new\r\n");
                } else if ("TCP-ACTIVE".equals(streamMode)) { // tcp主动模式
                    content.append("a=setup:active\r\n");
                    content.append("a=connection:new\r\n");
                }
            } else {
                switch (streamMode) {
                    case "TCP-PASSIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 98 97\r\n");
                        break;
                    case "TCP-ACTIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 98 97\r\n");
                        break;
                    case "UDP":
                        content.append("m=video ").append(mediaPort).append(" RTP/AVP 96 98 97\r\n");
                        break;
                }
                content.append("a=recvonly\r\n");
                content.append("a=rtpmap:96 PS/90000\r\n");
                content.append("a=rtpmap:98 H264/90000\r\n");
                content.append("a=rtpmap:97 MPEG4/90000\r\n");
                if ("TCP-PASSIVE".equals(streamMode)) { // tcp被动模式
                    content.append("a=setup:passive\r\n");
                    content.append("a=recvonly\r\n");
                    content.append("a=rtpmap:96 PS/90000\r\n");
                    content.append("a=rtpmap:98 H264/90000\r\n");
                    content.append("a=rtpmap:97 MPEG4/90000\r\n");
                    // tcp被动模式
                    content.append("a=setup:passive\r\n");
                    content.append("a=connection:new\r\n");
                }
            }

            content.append("y=").append(ssrc).append("\r\n");// ssrc

            Request request = headerBuilder.createInviteRequest(device, channelId, content.toString(), null, PLAY_TAG,
                    PLAY_TAG, ssrc);

            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放录像流
     * @param device
     * @param channelId
     * @param streamId
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public void playbackStreamCmd(SipDevice device, String channelId, String streamId, String startTime, String endTime) {
        try {
            //默认获取admin账号配置
            SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(device.getDeviceSipId());
            MediaServer mediaInfo = mediaServerService.getMediaInfo();
            if (mediaInfo == null) {
                log.warn("点播时发现ZLM尚未连接...");
                return ;
            }
            String ssrc = sipCacheService.getPlaySsrc(sipConfig.getDomain());
            StringBuilder content = new StringBuilder(200);
            content.append("v=0\r\n");
            content.append("o=").append(sipConfig.getServerSipid()).append(" 0 0 IN IP4 ").append(sipConfig.getIp()).append("\r\n");
            content.append("s=Playback\r\n");
            content.append("u=").append(channelId).append(":0\r\n");
            content.append("c=IN IP4 ").append(mediaInfo.getIp()).append("\r\n");
            content.append("t=").append(startTime).append(" ").append(endTime).append("\r\n");
            String mediaPort = null;
            mediaPort = zlmApiUtil.createRTPServer(mediaInfo, streamId) + "";
            if("-1".equals(mediaPort)){
                return ;
            }
            String streamMode = device.getStreammode().toUpperCase();
            if (sipConfig.getSeniorsdp() != null && sipConfig.getSeniorsdp() == 1) {
                if ("TCP-PASSIVE".equals(streamMode)) {
                    content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 126 125 99 34 98 97\r\n");
                } else if ("TCP-ACTIVE".equals(streamMode)) {
                    content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 126 125 99 34 98 97\r\n");
                } else if ("UDP".equals(streamMode)) {
                    content.append("m=video ").append(mediaPort).append(" RTP/AVP 96 126 125 99 34 98 97\r\n");
                }
                content.append("a=recvonly\r\n");
                content.append("a=rtpmap:96 PS/90000\r\n");
                content.append("a=fmtp:126 profile-level-id=42e01e\r\n");
                content.append("a=rtpmap:126 H264/90000\r\n");
                content.append("a=rtpmap:125 H264S/90000\r\n");
                content.append("a=fmtp:125 profile-level-id=42e01e\r\n");
                content.append("a=rtpmap:99 MP4V-ES/90000\r\n");
                content.append("a=fmtp:99 profile-level-id=3\r\n");
                content.append("a=rtpmap:98 H264/90000\r\n");
                content.append("a=rtpmap:97 MPEG4/90000\r\n");
                if ("TCP-PASSIVE".equals(streamMode)) { // tcp被动模式
                    content.append("a=setup:passive\r\n");
                    content.append("a=connection:new\r\n");
                } else if ("TCP-ACTIVE".equals(streamMode)) { // tcp主动模式
                    content.append("a=setup:active\r\n");
                    content.append("a=connection:new\r\n");
                }
            } else {
                switch (streamMode) {
                    case "TCP-PASSIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 98 97\r\n");
                        break;
                    case "TCP-ACTIVE":
                        content.append("m=video ").append(mediaPort).append(" TCP/RTP/AVP 96 98 97\r\n");
                        break;
                    case "UDP":
                        content.append("m=video ").append(mediaPort).append(" RTP/AVP 96 98 97\r\n");
                        break;
                }
                content.append("a=recvonly\r\n");
                content.append("a=rtpmap:96 PS/90000\r\n");
                content.append("a=rtpmap:98 H264/90000\r\n");
                content.append("a=rtpmap:97 MPEG4/90000\r\n");
                if ("TCP-PASSIVE".equals(streamMode)) { // tcp被动模式
                    content.append("a=setup:passive\r\n");
                    content.append("a=connection:new\r\n");
                } else if ("TCP-ACTIVE".equals(streamMode)) { // tcp主动模式
                    content.append("a=setup:active\r\n");
                    content.append("a=connection:new\r\n");
                }
            }
            content.append("y=").append(ssrc).append("\r\n");// ssrc
            Request request = headerBuilder.createPlaybackInviteRequest(device, channelId, content.toString(), null, PLAY_BACK_TAG, null);
            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Bye,基于对话
     * @param deviceId
     * @param streamId
     */
    @Override
    public void streamByeCmd(String streamId) {
        try {
            MediaServer mediaInfo = mediaServerService.getMediaInfo();
            DialogInfo dialogInfo = sipCacheService.getDialog(streamId);
            if(dialogInfo==null){
                log.error("[不存在此次对话，dialog缓存丢失,无法通知设备停止推流，zlmediaRTP服务已经关闭]");
                return;
            }
            Request byeRequest = headerBuilder.createByeRequest(dialogInfo);
            sipSender.transmit(byeRequest);
            sipCacheService.removeDialog(streamId);

            //关闭流媒体服务器推流端口
            if (mediaInfo != null) {
                zlmApiUtil.closeRTPServer(mediaInfo, streamId);
            }
        } catch (SipException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 发送查询设备信息指令
     * @param device
     * @return
     */
    @Override
    public boolean deviceInfoQuery(SipDevice device) {
        try {
            String catalogXml = "<?xml version=\"1.0\" encoding=\"GB2312\"?>\r\n" +
                    "<Query>\r\n" +
                    "<CmdType>DeviceInfo</CmdType>\r\n" +
                    "<SN>" + (int) ((Math.random() * 9 + 1) * 100000) + "</SN>\r\n" +
                    "<DeviceID>" + device.getDeviceSipId() + "</DeviceID>\r\n" +
                    "</Query>\r\n";

            Request request = headerBuilder.createMessageRequest(device, catalogXml, "ViaDeviceInfoBranch",
                    DEVICE_INFO, null);

            sipSender.transmit(request);

        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 发送查询设备通道指令
     * @param device
     * @return
     */
    @Override
    public boolean catalogQuery(SipDevice device) {
        try {
            String catalogXml = "<?xml version=\"1.0\" encoding=\"GB2312\"?>\r\n" +
                    "<Query>\r\n" +
                    "<CmdType>Catalog</CmdType>\r\n" +
                    "<SN>" + (int) ((Math.random() * 9 + 1) * 100000) + "</SN>\r\n" +
                    "<DeviceID>" + device.getDeviceSipId() + "</DeviceID>\r\n" +
                    "</Query>\r\n";

            Request request = headerBuilder.createMessageRequest(device, catalogXml, "ViaCatalogBranch",
                    CATE_LOG, null);

            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 发送查询设备录像记录指令
     * @param device
     * @param channelId
     * @param sn
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public boolean recordInfoQuery(SipDevice device, String channelId, String sn, String startTime, String endTime) {
        try {
            String recordInfoXml = "<?xml version=\"1.0\" encoding=\"GB2312\"?>\r\n" +
                    "<Query>\r\n" +
                    "<CmdType>RecordInfo</CmdType>\r\n" +
                    "<SN>" + sn + "</SN>\r\n" +
                    "<DeviceID>" + channelId + "</DeviceID>\r\n" +
                    "<StartTime>" + SipUtil.timestampToISO8601(startTime) + "</StartTime>\r\n" +
                    "<EndTime>" + SipUtil.timestampToISO8601(endTime) + "</EndTime>\r\n" +
                    "<Secrecy>0</Secrecy>\r\n" +
                    "<Type>all</Type>\r\n" +
                    "</Query>\r\n";

            Request request = headerBuilder.createMessageRequest(device, recordInfoXml,
                    "ViaRecordInfoBranch", RECORD_INFO, null);
            sipSender.transmit(request);

        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * PTZ控制指令
     * @param device
     * @param channelId
     * @param leftRight
     * @param upDown
     * @param inOut
     * @param moveSpeed
     * @param scaleSpeed
     * @return
     */
    @Override
    public boolean ptzCmd(SipDevice device, String channelId, int leftRight, int upDown, int inOut, int moveSpeed, int scaleSpeed) {
        try {
            String cmdStr = PtzCmdUtil.cmdString(leftRight, upDown, inOut, moveSpeed, scaleSpeed);
            String ptzXml = "<?xml version=\"1.0\"?>\r\n" +
                            "<Control>\r\n" +
                            "<CmdType>DeviceControl</CmdType>\r\n" +
                            "<SN>" + (int) ((Math.random() * 9 + 1) * 100000) + "</SN>\r\n" +
                            "<DeviceID>" + channelId + "</DeviceID>\r\n" +
                            "<PTZCmd>" + cmdStr + "</PTZCmd>\r\n" +
                            "<Info>\r\n" +
                            "<ControlPriority>5</ControlPriority>\r\n" +
                            "</Info>\r\n" +
                            "</Control>\r\n";
            Request request = headerBuilder.createMessageRequest(device, ptzXml, "ViaPTZBranch", "FromPTZTag", null);
            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 轮询从缓存中取出录像记录
     * @param key
     * @param timeout
     * @return
     */
    public RecordList getRecord(String key, long timeout) {
        long time = 0;
        while (true) {
            try {
                RecordList recordList = sipCacheService.getRecordList(key);
                if (null == recordList) {
                    if (time >= timeout) {
                        log.error("key:{} get Response timeout", key);
                        return null;
                    }
                    time += 500;
                    TimeUnit.MILLISECONDS.sleep(500L);
                } else {
                    return recordList;
                }
            } catch (Exception e) {
                log.error("", e);
                Thread.currentThread().interrupt();
                break;
            }
        }
        log.error("key:{} can't get Response", key);
        return null;
    }
}
