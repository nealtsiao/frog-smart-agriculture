package com.frog.sip.server.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author neal
 * @date 2024/4/21  08:26
 */
@Getter
@AllArgsConstructor
public enum SipDeviceStatus {
    online("3"),offline("4");
    
    private String status;
}
