package com.frog.trace.mapper;

import com.frog.trace.domain.TraceRecord;

import java.util.HashMap;
import java.util.List;

public interface TraceDataStatisticsMapper {

    public List<HashMap> selectRecordGroupByMonth(Long baseId);
    public List<HashMap> selectRecordGroupByCity(Long baseId);
    public List<HashMap> selectRecordStatistics(Long baseId);
    public List<HashMap> selectRecordGroupBySellpro(Long baseId);
    //查询溯源的记录列表
    public List<HashMap> selectRecord(Long baseId);
    //查询溯源报表功能上边的六
    public List<HashMap> selectTraceInfo(Long baseId);

}
