package com.frog.view.service;

import java.util.List;
import com.frog.view.domain.GoViewInfo;

/**
 * goview大屏Service接口
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
public interface IGoViewInfoService
{
    /**
     * 查询goview大屏
     * 
     * @param id goview大屏主键
     * @return goview大屏
     */
    public GoViewInfo selectGoViewInfoById(String id);

    /**
     * 查询goview大屏列表
     * 
     * @param GoViewInfo goview大屏
     * @return goview大屏集合
     */
    public List<GoViewInfo> selectGoViewInfoList(GoViewInfo GoViewInfo);

    /**
     * 新增goview大屏
     * 
     * @param GoViewInfo goview大屏
     * @return 结果
     */
    public int insertGoViewInfo(GoViewInfo GoViewInfo);

    /**
     * 修改goview大屏
     * 
     * @param GoViewInfo goview大屏
     * @return 结果
     */
    public int updateGoViewInfo(GoViewInfo GoViewInfo);

    /**
     * 批量删除goview大屏
     * 
     * @param ids 需要删除的goview大屏主键集合
     * @return 结果
     */
    public int deleteGoViewInfoByIds(String[] ids);

    /**
     * 删除goview大屏信息
     * 
     * @param id goview大屏主键
     * @return 结果
     */
    public int deleteGoViewInfoById(String id);
}
