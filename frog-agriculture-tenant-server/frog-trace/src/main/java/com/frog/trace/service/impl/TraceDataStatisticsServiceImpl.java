package com.frog.trace.service.impl;

import com.frog.trace.service.ITraceDataStatisticsService;
import com.frog.trace.domain.TraceRecord;
import com.frog.trace.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class TraceDataStatisticsServiceImpl implements ITraceDataStatisticsService {

    @Autowired
    private TraceDataStatisticsMapper traceDataStatisticsMapper;
    public List<HashMap> selectRecordGroupByMonth(Long baseId){
        return  traceDataStatisticsMapper.selectRecordGroupByMonth(baseId);
    }
    //大屏-溯源
    public List<HashMap> selectRecordGroupByCity(Long baseId){
        return  traceDataStatisticsMapper.selectRecordGroupByCity(baseId);
    }
    public List<HashMap> selectRecordStatistics(Long baseId){
        return  traceDataStatisticsMapper.selectRecordStatistics(baseId);
    }
    public List<HashMap> selectRecordGroupBySellpro(Long baseId){
        return  traceDataStatisticsMapper.selectRecordGroupBySellpro(baseId);
    }
    /**
     * 查询统计
     * @param baseId
     * @return
     */
    @Override
    public List<HashMap> selectRecord(Long baseId){
        return traceDataStatisticsMapper.selectRecord(baseId);
    }

    //溯源报表上边面六个数据
    @Override
    public List<HashMap> selectTraceInfo(Long baseId) {
        return traceDataStatisticsMapper.selectTraceInfo(baseId);
    }

}
