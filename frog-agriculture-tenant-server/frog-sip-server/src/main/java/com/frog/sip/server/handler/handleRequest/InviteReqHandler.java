package com.frog.sip.server.handler.handleRequest;

import com.frog.sip.server.handler.IReqHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.sip.RequestEvent;

@Slf4j
@Component
public class InviteReqHandler  implements IReqHandler {
    @Override
    public void processMsg(RequestEvent evt) {

    }
}
