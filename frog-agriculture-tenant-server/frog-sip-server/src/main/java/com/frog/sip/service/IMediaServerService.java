package com.frog.sip.service;

import com.frog.sip.domain.MediaServer;

import java.util.List;

/**
 * 流媒体服务器配置Service接口
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
public interface IMediaServerService
{
    /**
     * 查询流媒体服务器配置
     *
     * @return 流媒体服务器配置
     */
    List<MediaServer> selectMediaServer();
    MediaServer selectMediaServerBytenantId(Long tenantId);
    MediaServer getMediaInfo();

    /**
     * 查询流媒体服务器配置列表
     *
     * @param mediaServer 流媒体服务器配置
     * @return 流媒体服务器配置集合
     */
    List<MediaServer> selectMediaServerList(MediaServer mediaServer);

    /**
     * 新增流媒体服务器配置
     *
     * @param mediaServer 流媒体服务器配置
     * @return 结果
     */
    int insertMediaServer(MediaServer mediaServer);

    /**
     * 修改流媒体服务器配置
     *
     * @param mediaServer 流媒体服务器配置
     * @return 结果
     */
    int updateMediaServer(MediaServer mediaServer);

    /**
     * 批量删除流媒体服务器配置
     *
     * @param ids 需要删除的流媒体服务器配置主键集合
     * @return 结果
     */
    int deleteMediaServerByIds(Long[] ids);

    /**
     * 删除流媒体服务器配置信息
     *
     * @param id 流媒体服务器配置主键
     * @return 结果
     */
    int deleteMediaServerById(Long id);
}
