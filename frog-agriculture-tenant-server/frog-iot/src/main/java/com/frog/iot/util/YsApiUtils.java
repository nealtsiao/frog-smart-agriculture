package com.frog.iot.util;

import cn.hutool.core.io.unit.DataUnit;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.common.utils.StringUtils;
import com.frog.common.utils.http.HttpUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.domain.YsConfig;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.mapper.YsConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * 萤石云接口
 * @author neal
 * @date 2024/11/29  14:14
 */
@Component
public class YsApiUtils {
    @Autowired
    private YsConfigMapper ysConfigMapper;
    @Autowired
    private DeviceMapper deviceMapper;
    //token
    private String accessToken;
    //token过期时间
    private String expireTime;

    /**
     * 获取acccessToken
     * @return
     */
    public String getAccessToken(String appKey,String secret){
        if(StringUtils.isEmpty(accessToken) || System.currentTimeMillis() > Long.parseLong(expireTime)){
            if(StringUtils.isEmpty(appKey) || StringUtils.isEmpty(secret)){
                return "";
            }
            String returns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/token/get", String.format("appKey=%s&appSecret=%s", appKey, secret));
            JSONObject jsonObject = JSONObject.parseObject(returns);
            JSONObject data = jsonObject.getJSONObject("data");
            accessToken = data.getString("accessToken");
            expireTime = data.getString("expireTime");
        }
        return accessToken;
    }

    /**
     * 获取acccessToken
     * @param serialNum
     * @return
     */
    public String getAccessToken(String serialNum){
        Device device = deviceMapper.selectDeviceBySerialNumber(serialNum);
        YsConfig ysConfig = ysConfigMapper.selectYsConfigByProductId(device.getProductId());
        String accessToken = getAccessToken(ysConfig.getAppKey(), ysConfig.getSecret());
        return accessToken;
    }

    /**
     * 获取acccessToken
     * @param productId
     * @return
     */
    public String getAccessToken(Long productId){
        YsConfig ysConfig = ysConfigMapper.selectYsConfigByProductId(productId);
        String accessToken = getAccessToken(ysConfig.getAppKey(), ysConfig.getSecret());
        return accessToken;
    }

    /**
     * 获取设备详细信息
     * @param productId
     * @param serialNum
     * @return
     */
    public JSONObject getDeviceInfo(Long productId,String serialNum){
        String accessToken = getAccessToken(productId);
        String returns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/device/info", String.format("accessToken=%s&deviceSerial=%s", accessToken, serialNum));
        JSONObject jsonObject = JSONObject.parseObject(returns);
        JSONObject device = jsonObject.getJSONObject("data");
        return device;
    }

    /**
     * 获取设备分页列表
     * @param productId
     * @param pageSize
     * @param pageStart
     * @return
     */
    public JSONObject getDeviceList(Long productId, int pageSize, int pageStart){
        String accessToken = getAccessToken(productId);
        String returns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/device/list", String.format("accessToken=%s&pageSize=%s&pageStart=%s",accessToken, pageSize, pageStart));
        JSONObject jsonObject = JSONObject.parseObject(returns);
        return jsonObject;
    }

    /**
     * 获取设备下的通道
     * @param productId
     * @param serialNum
     * @return
     */
    public JSONArray getChannelsByDevice(Long productId,String serialNum){
        String accessToken = getAccessToken(productId);
        String channelReturns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/device/camera/list", String.format("accessToken=%s&deviceSerial=%s", accessToken, serialNum));
        JSONObject channels = JSONObject.parseObject(channelReturns);
        return channels.getJSONArray("data");
    }

    /**
     * 获取萤石云摄像头FLV播放地址
     * @param deviceSerial
     * @param protocol
     * @return
     */
    public String getPlayUrl(String deviceSerial,String channelNo, String protocol){
        String accessToken = getAccessToken(deviceSerial);
        String urlReturns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/v2/live/address/get", String.format("accessToken=%s&deviceSerial=%s&channelNo=%s&&protocol=%s", accessToken, deviceSerial,channelNo,protocol));
        JSONObject data = JSONObject.parseObject(urlReturns).getJSONObject("data");
        return data.getString("url");
    }

    /**
     * 停止云台控制
     * @param deviceSerial
     * @param channelNo
     * @param direction
     */
    public void PtzStop(String deviceSerial,Integer channelNo,Integer direction){
        String accessToken = getAccessToken(deviceSerial);
        String returns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/device/ptz/stop", String.format("accessToken=%s&deviceSerial=%s&channelNo=%s&direction=%s", accessToken, deviceSerial,channelNo,direction));
    }

    /**
     * 开始云台控制
     * @param deviceSerial
     * @param channelNo
     * @param direction
     * @param speed
     */
    public void ptzStart(String deviceSerial,Integer channelNo,Integer direction,Integer speed){
        String accessToken = getAccessToken(deviceSerial);
        String returns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/device/ptz/start", String.format("accessToken=%s&deviceSerial=%s&channelNo=%s&direction=%s&speed=%s", accessToken, deviceSerial,channelNo,direction,speed));
    }
    /**
     * 查询设备本地录像
     * @param deviceSerial
     * @param localIndex
     */
    public String getLocalVideo(String deviceSerial, String localIndex, String startTime, String endTime) {
        String accessToken = getAccessToken(deviceSerial);
        try {
            URL url = new URL("https://open.ys7.com/api/v3/device/local/video/unify/query?startTime=" + startTime + "&endTime=" + endTime);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("accessToken", accessToken);
            con.setRequestProperty("deviceSerial", deviceSerial);
            con.setRequestProperty("localIndex", localIndex);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject data = JSONObject.parseObject(response.toString()).getJSONObject("data");
            return data.getString("records");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取萤石云摄像头FLV播放地址(回播)
     * @param deviceSerial
     * @param protocol
     * @return
     */
    public HashMap<String, String> getPlayUrlBack(String deviceSerial,String channelNo, String protocol, String startTime, String stopTime){
        String accessToken = getAccessToken(deviceSerial);
        String urlReturns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/v2/live/address/get", String.format("accessToken=%s&deviceSerial=%s&channelNo=%s&&protocol=%s&type=%s&startTime=%s&stopTime=%s", accessToken, deviceSerial, channelNo, protocol, "2", startTime, stopTime));
        JSONObject data = JSONObject.parseObject(urlReturns).getJSONObject("data");
        HashMap<String, String> map = new HashMap<>();
        map.put("url",data.getString("url"));
        map.put("urlId",data.getString("id"));
        return map;
    }

    /**
     * 失效获取的播放地址
     * @param deviceSerial
     * @param channelNo
     * @param urlId
     * @return
     */
    public String getPlayUrlDisable(String deviceSerial,String channelNo, String urlId){
        String accessToken = getAccessToken(deviceSerial);
        String urlDisableReturns = HttpUtils.sendPost("https://open.ys7.com/api/lapp/v2/live/address/disable", String.format("accessToken=%s&deviceSerial=%s&channelNo=%s&urlId=%s", accessToken, deviceSerial, channelNo, urlId));
        String code = JSONObject.parseObject(urlDisableReturns).getString("code");
        return code;
    }
}
