package com.frog.sip.server.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync(proxyTargetClass = true)
public class SipThreadPoolTaskConfig {
    /**
     * 关于线程池配置建议说明：
     * IO密集型任务  = 一般为2*CPU核心数（常出现于线程中：数据库数据交互、文件上传下载、网络数据传输等等）
     * CPU密集型任务 = 一般为CPU核心数+1（常出现于线程中：复杂算法）
     * 混合型任务  = 视机器配置和复杂度自测而定
     */

    public static final int cpuNum = Runtime.getRuntime().availableProcessors();
    private static final int corePoolSize = cpuNum;
    private static final int maxPoolSize = cpuNum*2;
    private static final int keepAliveTime = 30;
    private static final int queueCapacity = 10000;
    private static final String threadNamePrefix = "sip-";

    @Bean("sipTask")
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程数目
        executor.setCorePoolSize(corePoolSize);
        //指定最大线程数,只有在缓冲队列满了之后才会申请超过核心线程数的线程
        executor.setMaxPoolSize(maxPoolSize);
        //队列中最大的数目
        executor.setQueueCapacity(queueCapacity);
        //备用线程（也就是超过核心线程数之外的线程）被销毁之前可空闲的时间数（单位秒）
        executor.setKeepAliveSeconds(keepAliveTime);
        //线程名称前缀
        executor.setThreadNamePrefix(threadNamePrefix);
        //线程池饱和策略，此处线程池饱和就交由主线程自己处理
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
