package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 设备告警对象 iot_alert_log
 * 
 * @author kerwincui
 * @date 2022-01-13
 */
@Data
public class AlertLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 告警ID */
    private Long alertLogId;

    /** 告警名称 */
    @Excel(name = "告警名称")
    private String alertName;

    /** 告警级别（1=提醒通知，2=轻微问题，3=严重警告，4=场景联动） */
    @Excel(name = "告警级别", readConverterExp = "1==提醒通知，2=轻微问题，3=严重警告，4=场景联动")
    private Long alertLevel;

    /** 处理状态(0=不需要处理,1=未处理,2=已处理) */
    @Excel(name = "处理状态(1=不需要处理,2=未处理,3=已处理)")
    private Integer status;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long productId;

    /** 设备编号 */
    private String serialNumber;

    /** 告警详情 */
    private String detail;

    private Long userId;

    private String deviceName;

    private Long deviceId;

    private Integer pageSize;

    private Integer offSet;

    private Long baseId;
}
