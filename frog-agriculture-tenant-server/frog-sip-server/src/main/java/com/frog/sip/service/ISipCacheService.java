package com.frog.sip.service;

import com.frog.sip.server.model.RecordList;
import com.frog.sip.server.model.Stream;
import com.frog.sip.server.model.ZlmMediaServer;
import com.frog.sip.server.model.DialogInfo;

public interface ISipCacheService {
    Long getCSEQ(String serverSipId);
    void addStream(Stream stream);
    Stream getStreamByStreamId(String streamId);
    void addMediaInfo(ZlmMediaServer mediaServerConfig);

    ZlmMediaServer getMediaInfo();

    void setRecordList(String key, RecordList recordList);
    public RecordList getRecordList(String key);
    public void addDialog(DialogInfo sipDialogInfo);
    public DialogInfo getDialog(String streamId);
    public void removeDialog(String streamId);
    public String getPlaySsrc(String domain);
    public void releaseSsrc(String ssrc);
    public int getSipUnicodeFront14B(String deviceId);
    public void setPlaybackUserStream(Long userId,String urlId);
    public String getPlaybackUserStream(Long userId);
    public void IncrSipUnicodeFront14B(String deviceId, Long num);

}
