package com.frog.system.service.impl;

import java.util.List;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.system.mapper.SysAppUpgradeMapper;
import com.frog.system.domain.SysAppUpgrade;
import com.frog.system.service.ISysAppUpgradeService;

/**
 * App升级Service业务层处理
 * 
 * @author nealtsiao
 * @date 2023-08-23
 */
@Service
public class SysAppUpgradeServiceImpl implements ISysAppUpgradeService
{
    @Autowired
    private SysAppUpgradeMapper sysAppUpgradeMapper;

    /**
     * 查询App升级
     * 
     * @param recordId App升级主键
     * @return App升级
     */
    @Override
    public SysAppUpgrade selectExtUpgradeByRecordId(Long recordId)
    {
        return sysAppUpgradeMapper.selectExtUpgradeByRecordId(recordId);
    }

    /**
     * 查询App升级列表
     * 
     * @param sysAppUpgrade App升级
     * @return App升级
     */
    @Override
    public List<SysAppUpgrade> selectExtUpgradeList(SysAppUpgrade sysAppUpgrade)
    {
        return sysAppUpgradeMapper.selectExtUpgradeList(sysAppUpgrade);
    }

    /**
     * 新增App升级
     * 
     * @param sysAppUpgrade App升级
     * @return 结果
     */
    @Override
    public int insertExtUpgrade(SysAppUpgrade sysAppUpgrade)
    {
        sysAppUpgrade.setCreateBy(SecurityUtils.getUserId().toString());
        sysAppUpgrade.setCreateTime(DateUtils.getNowDate());
        return sysAppUpgradeMapper.insertExtUpgrade(sysAppUpgrade);
    }

    /**
     * 修改App升级
     * 
     * @param sysAppUpgrade App升级
     * @return 结果
     */
    @Override
    public int updateExtUpgrade(SysAppUpgrade sysAppUpgrade)
    {
        sysAppUpgrade.setUpdateBy(SecurityUtils.getUserId().toString());
        sysAppUpgrade.setUpdateTime(DateUtils.getNowDate());
        return sysAppUpgradeMapper.updateExtUpgrade(sysAppUpgrade);
    }

    /**
     * 批量删除App升级
     * 
     * @param recordIds 需要删除的App升级主键
     * @return 结果
     */
    @Override
    public int deleteExtUpgradeByRecordIds(Long[] recordIds)
    {
        return sysAppUpgradeMapper.deleteExtUpgradeByRecordIds(recordIds);
    }

    /**
     * 删除App升级信息
     * 
     * @param recordId App升级主键
     * @return 结果
     */
    @Override
    public int deleteExtUpgradeByRecordId(Long recordId)
    {
        return sysAppUpgradeMapper.deleteExtUpgradeByRecordId(recordId);
    }
}
