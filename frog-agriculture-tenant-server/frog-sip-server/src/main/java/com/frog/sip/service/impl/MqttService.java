package com.frog.sip.service.impl;

import com.alibaba.fastjson.JSON;
import com.frog.iot.domain.Device;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.model.ThingsModels.ThingsModelSimpleItem;
import com.frog.iot.mqtt.EmqxClient;
import com.frog.sip.domain.SipConfig;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.server.model.SipDeviceSummary;
import com.frog.sip.service.IMqttService;
import com.frog.sip.service.ISipConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
public class MqttService implements IMqttService {

    @Autowired
    private EmqxClient emqxClient;

    @Autowired
    private ISipConfigService sipConfigService;

    @Autowired
    private DeviceMapper deviceMapper;

    /**
     * 发布的主题
     */
    String pStatusTopic = "/status/post";
    String pInfoTopic = "/info/post";
    String pPropertyTopic = "/property/post";
    String pFunctionTopic = "/function/post";

    @Override
    public void publishSipDeviceInfoToInfoPost(SipDevice sipDevice) {
        // wifi信号:rssi
        // 固件版本,firmwareVersion
        // 设备状态,status // （1-未激活，2-禁用，3-在线，4-离线）
        // 用户Id,userId
        // 经度,longitude
        // 纬度,latitude
        // 设备摘要,可选（自定义配置信息）summary

        //默认获取admin账号配置
        Device iotDevice = deviceMapper.selectDeviceBySerialNumber(sipDevice.getDeviceSipId());
        if (iotDevice == null) {
            iotDevice = new Device();
            iotDevice.setSerialNumber(sipDevice.getDeviceSipId());
            iotDevice.setDeviceName(sipDevice.getDeviceName());
        }
        iotDevice.setUserId(iotDevice.getUserId());
        iotDevice.setProductId(iotDevice.getProductId());
//        iotDevice.setProductName(sipDevice.getModel());

        iotDevice.setRssi(0);
        iotDevice.setStatus(Integer.parseInt(sipDevice.getOnline()));
        iotDevice.setFirmwareVersion(BigDecimal.valueOf(1.0));
        iotDevice.setNetworkIp(sipDevice.getIp());
        SipDeviceSummary deviceSummary = new SipDeviceSummary(sipDevice);
        iotDevice.setSummary(JSON.toJSONString(deviceSummary));
        emqxClient.publish(1, false, "/" + iotDevice.getProductId() + "/" + sipDevice.getDeviceSipId() + pInfoTopic, JSON.toJSONString(iotDevice));
    }

    public void publishChannelsProperty(String DeviceSipId, List<SipDeviceChannel> channels) {
        SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(DeviceSipId);
        if (sipConfig != null) {
            List<ThingsModelSimpleItem> thingsList = new ArrayList<>();
            ThingsModelSimpleItem item = new ThingsModelSimpleItem();
            item.setId("channel");
            item.setValue(JSON.toJSONString(channels));
            thingsList.add(item);
            publishProperty(sipConfig.getProductId(), DeviceSipId, thingsList, 0);
        }
    }

    @Override
    public void publishRecordsProperty(String DeviceSipId, RecordList recordList) {
        SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(DeviceSipId);
        if (sipConfig != null) {
            List<ThingsModelSimpleItem> thingsList = new ArrayList<>();
            ThingsModelSimpleItem item = new ThingsModelSimpleItem();
            item.setId("recordList");
            item.setValue(JSON.toJSONString(recordList));
            thingsList.add(item);
            publishProperty(sipConfig.getProductId(), DeviceSipId, thingsList, 0);
        }
    }

    public void publishProperty(Long productId, String deviceNum, List<ThingsModelSimpleItem> thingsList, int delay) {
        String pre = "";
        if (delay > 0) {
            pre = "$delayed/" + String.valueOf(delay) + "/";
        }
        if (thingsList == null) {
            emqxClient.publish(1, true, "/" + productId + "/" + deviceNum + pPropertyTopic, "");
        } else {
            emqxClient.publish(1, true, "/" + productId + "/" + deviceNum + pPropertyTopic, JSON.toJSONString(thingsList));
        }
    }
}
