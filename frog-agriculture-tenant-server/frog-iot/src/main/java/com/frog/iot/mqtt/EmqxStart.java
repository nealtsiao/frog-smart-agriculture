package com.frog.iot.mqtt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *项目启动执行
 */
@Component
@Order(1)
public class EmqxStart implements ApplicationRunner {

    @Autowired
    private EmqxClient emqxClient;

    @Override
    public void run(ApplicationArguments applicationArguments){
        //连接客户端
        emqxClient.connect();
    }
}
