package com.frog.trace.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 店铺对象 agriculture_trace_shop
 * 
 * @author nealtsiao
 * @date 2023-08-02
 */
@Data
public class TraceShop extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 店铺ID */
    private Long shopId;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String shopName;

    /** 店铺类型 */
    @Excel(name = "店铺类型")
    private String shopType;

    /** 店铺图片 */
    @Excel(name = "店铺图片")
    private String shopImg;

    /** 店铺地址 */
    @Excel(name = "店铺地址")
    private String shopAddress;

    /** 店铺坐标 */
    @Excel(name = "店铺坐标")
    private String shopCoordinate;

    /** 店铺联系人 */
    @Excel(name = "店铺联系人")
    private String shopContacts;

    /** 店铺联系方式 */
    @Excel(name = "店铺联系方式")
    private String shopTel;

    /** 店铺链接 */
    @Excel(name = "店铺链接")
    private String shopUrl;

    /** 店铺描述 */
    @Excel(name = "店铺描述")
    private String shopDes;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 排序 */
    private Long orderNum;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;
}
