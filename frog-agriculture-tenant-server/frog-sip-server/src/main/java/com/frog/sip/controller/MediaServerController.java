package com.frog.sip.controller;

import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.page.TableDataInfo;
import com.frog.common.enums.BusinessType;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.sip.domain.MediaServer;
import com.frog.sip.service.IMediaServerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 流媒体服务器配置Controller
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
@RestController
@RequestMapping("/sip/mediaserver")
@Api(tags = "sip-流媒体库管理")
public class MediaServerController extends BaseController
{
    @Autowired
    private IMediaServerService mediaServerService;

    /**
     * 查询流媒体服务器配置列表
     */
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @GetMapping("/list")
    @ApiOperation("查询流媒体服务器配置列表")
    public TableDataInfo list(MediaServer mediaServer)
    {
        startPage();
        List<MediaServer> list = mediaServerService.selectMediaServerList(mediaServer);
        return getDataTable(list);
    }

    /**
     * 导出流媒体服务器配置列表
     */
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @Log(title = "流媒体服务器配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出流媒体服务器配置列表")
    public void export(HttpServletResponse response, MediaServer mediaServer)
    {
        List<MediaServer> list = mediaServerService.selectMediaServerList(mediaServer);
        ExcelUtil<MediaServer> util = new ExcelUtil<MediaServer>(MediaServer.class);
        util.exportExcel(response, list, "流媒体服务器配置数据");
    }

    /**
     * 获取流媒体服务器配置详细信息,只获取第一条
     */
    @PreAuthorize("@ss.hasPermi('iot:video:query')")
    @GetMapping()
    @ApiOperation("获取流媒体服务器配置详细信息,只获取第一条")
    public AjaxResult getInfo()
    {
        List<MediaServer> list=mediaServerService.selectMediaServer();
        if(list==null || list.size()==0){
            MediaServer mediaServer=new  MediaServer();
            // 设置默认值
            mediaServer.setEnabled(1);
            mediaServer.setDomain("");
            mediaServer.setIp("");
            mediaServer.setPortHttp(8082L);
            mediaServer.setPortWs(8082L);
            mediaServer.setPortRtmp(1935L);
            mediaServer.setPortRtsp(554L);
            mediaServer.setProtocol("HTTP");
            mediaServer.setSecret("035c73f7-bb6b-4889-a715-d9eb2d192xxx");
            mediaServer.setRtpPortRange("30000,30500");
            list=new ArrayList<>();
            list.add(mediaServer);
        }
        return AjaxResult.success(list.get(0));
    }

    /**
     * 新增流媒体服务器配置
     */
    @PreAuthorize("@ss.hasPermi('iot:video:add')")
    @Log(title = "流媒体服务器配置", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("流媒体服务器配置")
    public AjaxResult add(@RequestBody MediaServer mediaServer)
    {
        int result = mediaServerService.insertMediaServer(mediaServer);
        return AjaxResult.success(mediaServer);
    }

    /**
     * 修改流媒体服务器配置
     */
    @ApiOperation("修改流媒体服务器配置")
    @PreAuthorize("@ss.hasPermi('iot:video:edit')")
    @Log(title = "流媒体服务器配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MediaServer mediaServer)
    {
        return toAjax(mediaServerService.updateMediaServer(mediaServer));
    }

    /**
     * 删除流媒体服务器配置
     */
    @ApiOperation("删除流媒体服务器配置")
    @PreAuthorize("@ss.hasPermi('iot:video:remove')")
    @Log(title = "流媒体服务器配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mediaServerService.deleteMediaServerByIds(ids));
    }
}
