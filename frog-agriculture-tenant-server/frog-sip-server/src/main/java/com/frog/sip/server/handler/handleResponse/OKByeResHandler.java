package com.frog.sip.server.handler.handleResponse;

import com.frog.sip.server.handler.IResHandler;
import com.frog.sip.server.listener.IGBListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.ResponseEvent;
import java.text.ParseException;
import static com.frog.sip.server.enums.SipResponseCode.OK;
@Slf4j
@Component
public class OKByeResHandler implements InitializingBean, IResHandler {
    @Autowired
    IGBListener sipListener;
    @Override
    public void processMsg(ResponseEvent evt) throws ParseException {
        log.info(evt.getResponse().toString());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = OK.getName()+"BYE";
        sipListener.addResponseProcessor(method,this);
    }
}
