package com.frog.sip.server.model;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

@Data
public class Stream {
    private String streamId;
    private String deviceID;
    private String channelId;
    private String playurl;
    private String ssrc;
    private String flv;
    private String ws_flv;
    private String ts;
    private String ws_ts;
    private String rtmp;
    private String rtsp;
    private JSONArray tracks;

    public Stream(String deviceSipId, String channelId, String streamId) {
        this.deviceID = deviceSipId;
        this.channelId = channelId;
        this.streamId = streamId;
    }
    public Stream() {}
}
