package com.frog.iot.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.common.utils.StringUtils;
import com.frog.common.utils.http.HttpUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.domain.YsConfig;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.mapper.YsConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;

/**
 * 时间戳工具类
 * @author zdq
 * @date 2024/12/26  15:05
 */
public class TimeStampUtils {

    /**
     * 传入本月开始，结束时间戳获取本月每一天开始与结束时间戳
     * @param startTime
     * @param endTime
     */
    public static LinkedHashMap<String, Object> getAllDayByMonth(String startTime, String endTime) {
        long startTimestamp = Long.parseLong(startTime);
        long endTimestamp = Long.parseLong(endTime);

        LinkedHashMap<String, Object> timeStampList = new LinkedHashMap<>();

        LocalDateTime currentDay = LocalDateTime.ofInstant(Instant.ofEpochSecond(startTimestamp), ZoneOffset.UTC);
        LocalDateTime lastDayOfMonth = LocalDateTime.ofInstant(Instant.ofEpochSecond(endTimestamp), ZoneOffset.UTC);

        int day = 1;
        while (!currentDay.isAfter(lastDayOfMonth)) {
            long dayStartTimestamp = currentDay.toEpochSecond(ZoneOffset.UTC);
            LocalDateTime nextDay = currentDay.plusDays(1);
            long dayEndTimestamp = nextDay.toEpochSecond(ZoneOffset.UTC) - 1;

            String dayKey = "Day " + day;
            timeStampList.put(dayKey, new long[]{dayStartTimestamp, dayEndTimestamp});

            currentDay = nextDay;
            day++;
        }

        return timeStampList;
    }

    /**
     * 获取到传入时间戳当天的最后一秒的时间戳 并格式化时间返出
     * @param timestamp
     */
    public static LocalDateTime getLastSecondOfDay(long timestamp) {
        // 获取当天最后一小时最后一分钟最后一秒的时间戳
        LocalDateTime startDay = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.of("Asia/Shanghai"));
        LocalDateTime endOfDay = LocalDateTime.of(startDay.toLocalDate(), LocalTime.of(23, 59, 59));
        return endOfDay;
    }
}
