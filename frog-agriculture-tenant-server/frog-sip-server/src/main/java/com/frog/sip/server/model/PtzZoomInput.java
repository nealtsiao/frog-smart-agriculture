package com.frog.sip.server.model;

import lombok.Data;

@Data
public class PtzZoomInput {
    // 1是out 2是in
    int inOut;
    int zoomSpeed;
}
