package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 萤石系统配置对象 ys_config
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@Data
public class YsConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 配置ID */
    private Long id;

    /** 产品ID */

    private Long productId;

    /** 萤石云AppKey */
    @Excel(name = "萤石云AppKey")
    private String appKey;

    /** 萤石云Secret */
    @Excel(name = "萤石云Secret")
    private String secret;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;
}
