package com.frog.sip.server.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.sip.domain.MediaServer;
import com.frog.sip.server.model.ZlmMediaServer;
import com.frog.sip.service.IMediaServerService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;


@Slf4j
@Component
public class ZlmApiUtil {
    @Autowired
    private IMediaServerService mediaServerService;

    private final int[] udpPortRangeArray = new int[2];
    private int currentPort = 0;

    public JSONObject sendPost(MediaServer media, String api, Map<String, Object> param) {
        JSONObject responseJSON = null;
        if (media != null) {
            OkHttpClient client = new OkHttpClient();
            String url = String.format("http://%s:%s/index/api/%s", media.getIp(), media.getPortHttp(), api);
            log.debug(url);

            FormBody.Builder builder = new FormBody.Builder();
            builder.add("secret", media.getSecret());
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.add(key, param.get(key).toString());
                }
            }

            FormBody body = builder.build();
            Request request = new Request.Builder().post(body).url(url).build();
            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    String responseStr = response.body().string();
                    responseJSON = JSON.parseObject(responseStr);
                }
            } catch (ConnectException e) {
                log.error(String.format("连接ZLM失败: %s, %s", e.getCause().getMessage(), e.getMessage()));
                log.info("请检查media配置并确认ZLM已启动...");
            } catch (IOException e) {
                log.error(String.format("连接ZLM失败: %s,%s",e.getMessage(),"请在启动成功之后重新配置流媒体服务器参数！"));
            }

        }
        return responseJSON;
    }

    public JSONObject getMediaList(MediaServer media,String app, String schema) {
        Map<String, Object> param = new HashMap<>();
        param.put("app", app);
        param.put("schema", schema);
        param.put("vhost", "__defaultVhost__");
        return sendPost(media,"getMediaList", param);
    }

    public JSONObject getMediaInfo(MediaServer media, String app, String schema, String stream) {
        Map<String, Object> param = new HashMap<>();
        param.put("app", app);
        param.put("schema", schema);
        param.put("stream", stream);
        param.put("vhost", "__defaultVhost__");
        return sendPost(media,"getMediaInfo", param);
    }

    public JSONObject getRtpInfo(MediaServer media, String stream_id) {
        Map<String, Object> param = new HashMap<>();
        param.put("stream_id", stream_id);
        return sendPost(media,"getRtpInfo", param);
    }


    public JSONObject setServerConfig(MediaServer media, Map<String, Object> param) {
        return sendPost(media,"setServerConfig", param);
    }
    public JSONObject openRtpServer(MediaServer media, Map<String, Object> param) {
        return sendPost(media,"openRtpServer", param);
    }
    public JSONObject closeRtpServer(MediaServer media, Map<String, Object> param) {
        return sendPost(media,"closeRtpServer", param);
    }

    public ZlmMediaServer getMediaServerConfig(MediaServer media) {
        JSONObject responseJSON = sendPost(media,"getServerConfig", null);
        ZlmMediaServer mediaServer = null;
        if (responseJSON != null) {
            JSONArray data = responseJSON.getJSONArray("data");
            if (data != null && data.size() > 0) {
                log.info("[1-流媒体服务配置获取成功]");
                mediaServer = JSON.parseObject(JSON.toJSONString(data.get(0)), ZlmMediaServer.class);
            }
        } else {
            log.error("获取流媒体服务配置失败");
        }
        return mediaServer;
    }

    /**
     * 创建zml的rtpServer
     * @param media
     * @param streamId
     * @return 创建成功智慧zlm的rtpServer的udp端口
     */
    public int createRTPServer(MediaServer media, String streamId) {
        Map<String, Object> param = new HashMap<>();
        int result = -1;

        if (media != null) {
            int newPort = getPortFromUdpPortRange(media.getRtpPortRange());
            /**
             * /index/api/openRtpServer接口的参数
             * secret api操作密钥
             * port 绑定的端口，0时为随机端口
             * tcp_mode tcp模式，0时为不启用tcp监听，1时为启用tcp监听，2时为tcp主动连接模式
             * stream_id 该端口绑定的流id
             */
            param.put("port", newPort);
            param.put("tcp_mode", 1);
            param.put("stream_id", streamId);
            //调用zlm的/index/api/openRtpServer接口创建rtpserver
            JSONObject jsonObject = openRtpServer(media,param);
            if (jsonObject != null) {
                switch (jsonObject.getInteger("code")) {
                    case 0:
                        result = newPort;
                        break;
                    case -300: // id已经存在
                        result = newPort;
                        break;
                    case -400: // 端口占用
                        result = createRTPServer(media,streamId);
                        break;
                    default:
                        log.error("创建RTP Server 失败: " + jsonObject.getString("msg"));
                        break;
                }
            } else {
                //  检查ZLM状态
                log.error("创建RTP Server 失败: 请检查ZLM服务");
            }
        }
        return result;
    }

    public boolean closeRTPServer(MediaServer media, String streamId) {
        boolean result = false;
        Map<String, Object> param = new HashMap<>();
        param.put("stream_id", streamId);
        JSONObject jsonObject = closeRtpServer(media,param);
        if (jsonObject != null ) {
            if (jsonObject.getInteger("code") == 0) {
                result = jsonObject.getInteger("hit") == 1;
            }else {
                log.error("关闭RTP Server 失败: " + jsonObject.getString("msg"));
            }
        }else {
            //  检查ZLM状态
            log.error("关闭RTP Server 失败: 请检查ZLM服务");
        }
        return result;
    }

    /**
     * 获取udp推流端口号
     * @param udpPortRange mediaServe中的端口号范围
     * @return 返回一个推流的udp端口号
     */
    private int getPortFromUdpPortRange(String udpPortRange) {
        if (currentPort == 0) {
            String[] udpPortRangeStrArray = udpPortRange.split(",");
            udpPortRangeArray[0] = Integer.parseInt(udpPortRangeStrArray[0]);
            udpPortRangeArray[1] = Integer.parseInt(udpPortRangeStrArray[1]);
        }

        if (currentPort == 0 || currentPort++ > udpPortRangeArray[1]) {
            currentPort = udpPortRangeArray[0];
            return udpPortRangeArray[0];
        } else {
            if (currentPort % 2 == 1) {
                currentPort++;
            }
            return currentPort++;
        }
    }
}
