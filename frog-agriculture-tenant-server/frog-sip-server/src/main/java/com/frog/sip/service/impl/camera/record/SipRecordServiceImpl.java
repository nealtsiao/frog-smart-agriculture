package com.frog.sip.service.impl.camera.record;

import com.frog.common.core.redis.RedisCache;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.RecordCacheManager;
import com.frog.sip.service.IRecordService;
import com.frog.sip.service.ISipDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("sipRecord")
public class SipRecordServiceImpl implements IRecordService {

    @Autowired
    private ISipCmd sipCmd;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private RecordCacheManager recordCacheManager;

    @Autowired
    private ISipDeviceService sipDeviceService;

    @Override
    public RecordList listDevRecord(String deviceId, String channelId, String start, String end) {
        SipDevice sipDevice = sipDeviceService.selectSipDeviceBySipId(deviceId);
        if (sipDevice != null) {
            String sn = String.valueOf((int)((Math.random() * 9 + 1) * 100000));
            String recordkey = channelId + ":" + sn;
            recordCacheManager.addlock(recordkey);
            sipCmd.recordInfoQuery(sipDevice,channelId,sn,start,end);
            return (RecordList)sipCmd.getRecord( recordkey, 2000L);//超时时间2秒
        }
        return null;
    }
}
