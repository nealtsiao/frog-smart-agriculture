package com.frog.view.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * goview大屏对象 view_goview_project
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@Data
public class GoViewInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 项目状态[0未发布,1发布] */
    @Excel(name = "项目状态[0未发布,1发布]")
    private Integer state;

    /** 首页图片 */
    @Excel(name = "首页图片")
    private String indexImage;

    /** 存储数据 */
    @Excel(name = "存储数据")
    private String content;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private Long orderNum;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户ID */
    @Excel(name = "租户ID")
    private Long tenantId;

    /** 基地ID */
    @Excel(name = "基地ID")
    private Long baseId;

    /** 部门ID */
    @Excel(name = "部门ID")
    private Long deptId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;
}
