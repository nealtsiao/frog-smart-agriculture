package com.frog.view.service.impl;

import java.util.List;

import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.view.mapper.GoViewPrivateImagesMapper;
import com.frog.view.domain.GoViewPrivateImages;
import com.frog.view.service.IGoViewPrivateImagesService;

/**
 * goview用户私有图片Service业务层处理
 *
 * @author nealtsiao
 * @date 2024-11-25
 */
@Service
public class GoViewPrivateImagesServiceImpl implements IGoViewPrivateImagesService {
    @Autowired
    private GoViewPrivateImagesMapper goViewPrivateImagesMapper;

    /**
     * 查询goview用户私有图片
     *
     * @param id goview用户私有图片主键
     * @return goview用户私有图片
     */
    @Override
    public GoViewPrivateImages selectGoViewPrivateImagesById(Long id) {
        return goViewPrivateImagesMapper.selectGoViewPrivateImagesById(id);
    }

    /**
     * 查询goview用户私有图片列表
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return goview用户私有图片
     */
    @Override
    public List<GoViewPrivateImages> selectGoViewPrivateImagesList(GoViewPrivateImages goViewPrivateImages) {
        goViewPrivateImages.setCreateBy(SecurityUtils.getUserId().toString());
        return goViewPrivateImagesMapper.selectGoViewPrivateImagesList(goViewPrivateImages);
    }

    /**
     * 新增goview用户私有图片
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    @Override
    public int insertGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages) {
        goViewPrivateImages.setCreateBy(SecurityUtils.getUserId().toString());
        goViewPrivateImages.setCreateTime(DateUtils.getNowDate());
        goViewPrivateImagesMapper.insertGoViewPrivateImages(goViewPrivateImages);
        int Id = goViewPrivateImages.getId().intValue();
        return  Id;
    }

    /**
     * 修改goview用户私有图片
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    @Override
    public int updateGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages) {
        goViewPrivateImages.setUpdateBy(SecurityUtils.getUserId().toString());
        goViewPrivateImages.setUpdateTime(DateUtils.getNowDate());
        return goViewPrivateImagesMapper.updateGoViewPrivateImages(goViewPrivateImages);
    }

    /**
     * 批量删除goview用户私有图片
     *
     * @param ids 需要删除的goview用户私有图片主键
     * @return 结果
     */
    @Override
    public int deleteGoViewPrivateImagesByIds(Long[] ids) {
        return goViewPrivateImagesMapper.deleteGoViewPrivateImagesByIds(ids);
    }

    /**
     * 删除goview用户私有图片信息
     *
     * @param id goview用户私有图片主键
     * @return 结果
     */
    @Override
    public int deleteGoViewPrivateImagesById(Long id) {
        return goViewPrivateImagesMapper.deleteGoViewPrivateImagesById(id);
    }
}
