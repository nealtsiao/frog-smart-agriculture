package com.frog.sip.server.util;

import lombok.extern.slf4j.Slf4j;

import javax.sip.address.SipURI;
import javax.sip.header.*;

/**
 * @author neal
 * @date 2024/12/20  14:19
 */
@Slf4j
public class GetTextFromHeaderUtil {
    public static String getUserFromFromHeaderOrToHeader(Header header){
        if(header instanceof ToHeader){
            ToHeader toHeader = (ToHeader) header;
            SipURI toUri = (SipURI) toHeader.getAddress().getURI();
            String user = toUri.getUser();
            return user;
        }else if (header instanceof FromHeader){
            FromHeader fromHeader = (FromHeader) header;
            SipURI fromUri = (SipURI) fromHeader.getAddress().getURI();
            String user = fromUri.getUser();
            return user;
        }else{
            log.error("[暂时支持FromHeader和ToHeader解析，其他Header暂不支持]");
            return null;
        }
    }

    public static String getHostFromFromHeaderOrToHeader(Header header){
        if(header instanceof ToHeader){
            ToHeader toHeader = (ToHeader) header;
            SipURI toUri = (SipURI) toHeader.getAddress().getURI();
            String host = toUri.getHost();
            return host;
        }else if (header instanceof FromHeader){
            FromHeader fromHeader = (FromHeader) header;
            SipURI fromUri = (SipURI) fromHeader.getAddress().getURI();
            String host = fromUri.getHost();
            return host;
        }else{
            log.error("[暂时支持FromHeader和ToHeader解析，其他Header暂不支持]");
            return null;
        }
    }

    public static String getTagFromFromHeaderOrToHeader(Header header){
        if(header instanceof ToHeader){
            ToHeader toHeader = (ToHeader) header;
            return toHeader.getTag();
        }else if (header instanceof FromHeader){
            FromHeader fromHeader = (FromHeader) header;
            return fromHeader.getTag();
        }else{
            log.error("[暂时支持FromHeader和ToHeader解析，其他Header暂不支持]");
            return null;
        }
    }

    public static String getHostFromViaHeader(Header header){
        if(header instanceof ViaHeader){
            ViaHeader viaHeader = (ViaHeader) header;
            return viaHeader.getHost();
        }else{
            log.error("[只支持ViaHeader]");
            return "";
        }
    }
    public static int getPortFromViaHeader(Header header){
        if(header instanceof ViaHeader){
            ViaHeader viaHeader = (ViaHeader) header;
            return viaHeader.getPort();
        }else{
            log.error("[只支持ViaHeader]");
            return 0;
        }
    }
    public static String getBranchFromViaHeader(Header header){
        if(header instanceof ViaHeader){
            ViaHeader viaHeader = (ViaHeader) header;
            return viaHeader.getBranch();
        }else{
            log.error("[只支持ViaHeader]");
            return "";
        }
    }
    public static String getReceivedFromViaHeader(Header header){
        if(header instanceof ViaHeader){
            ViaHeader viaHeader = (ViaHeader) header;
            return viaHeader.getReceived();
        }else{
            log.error("[只支持ViaHeader]");
            return "";
        }
    }
    public static String getCallIdTextFromCallIdHeader(Header header){
        if(header instanceof CallIdHeader){
            CallIdHeader callIdHeader = (CallIdHeader) header;
            return callIdHeader.getCallId();
        }else{
            log.error("[只支持ViaHeader]");
            return "";
        }
    }
}
