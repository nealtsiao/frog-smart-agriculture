package com.frog.sip.service;

import com.frog.sip.server.model.Stream;

public interface IPlayService {

    Stream play(String deviceId, String channelId);

    String playStop(String deviceId, String channelId, String streamId);

    String getPlayStream(String deviceId, String channelId);

    Stream playback(String deviceId, String channelId, String startTime, String endTime);

    String playbackStop(String deviceId, String channelId, String streamId);

    String playbackPause(String deviceId, String channelId, String streamId);

    String playbackReplay(String deviceId, String channelId, String streamId);

    String playbackSeek(String deviceId, String channelId, String streamId, long firstVideoTime,long seekTime);

    String playbackSpeed(String deviceId, String channelId, String streamId, Integer speed);
}
