package com.frog.sip.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.enums.BusinessType;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.service.ISipDeviceService;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.common.core.page.TableDataInfo;

/**
 * 监控设备Controller
 *
 * @author zhuangpeng.li
 * @date 2022-10-07
 */
@RestController
@RequestMapping("/sip/device")
@Api(tags = "sip-监控设备管理")
public class SipDeviceController extends BaseController
{
    @Autowired
    private ISipDeviceService sipDeviceService;

    /**
     * 查询监控设备列表
     */
    @ApiOperation("查询监控设备列表")
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @GetMapping("/list")
    public TableDataInfo list(SipDevice sipDevice)
    {
        startPage();
        List<SipDevice> list = sipDeviceService.selectSipDeviceList(sipDevice);
        return getDataTable(list);
    }

    /**
     * 导出监控设备列表
     */
    @ApiOperation("导出监控设备列表")
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @Log(title = "监控设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SipDevice sipDevice)
    {
        List<SipDevice> list = sipDeviceService.selectSipDeviceList(sipDevice);
        ExcelUtil<SipDevice> util = new ExcelUtil<SipDevice>(SipDevice.class);
        util.exportExcel(response, list, "监控设备数据");
    }

    /**
     * 获取监控设备详细信息
     */
    @ApiOperation("获取监控设备详细信息")
    @PreAuthorize("@ss.hasPermi('iot:video:query')")
    @GetMapping(value = "/{deviceId}")
    public AjaxResult getInfo(@PathVariable("deviceId") String deviceId)
    {
        return AjaxResult.success(sipDeviceService.selectSipDeviceBySipId(deviceId));
    }

    /**
     * 新增监控设备
     */
    @ApiOperation("新增监控设备")
    @PreAuthorize("@ss.hasPermi('iot:video:add')")
    @Log(title = "监控设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SipDevice sipDevice)
    {
        return toAjax(sipDeviceService.insertSipDevice(sipDevice));
    }

    /**
     * 修改监控设备
     */
    @ApiOperation("修改监控设备")
    @PreAuthorize("@ss.hasPermi('iot:video:edit')")
    @Log(title = "监控设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SipDevice sipDevice)
    {
        return toAjax(sipDeviceService.updateSipDevice(sipDevice));
    }

    /**
     * 删除监控设备
     */
    @ApiOperation("删除监控设备")
    @PreAuthorize("@ss.hasPermi('iot:video:remove')")
    @Log(title = "监控设备", businessType = BusinessType.DELETE)
    @DeleteMapping("/{deviceIds}")
    public AjaxResult remove(@PathVariable Long[] deviceIds)
    {
        return toAjax(sipDeviceService.deleteSipDeviceByDeviceIds(deviceIds));
    }

    /**
     * 删除监控设备
     * @param sipId
     * @return
     */
    @ApiOperation("删除监控设备")
    @PreAuthorize("@ss.hasPermi('iot:video:remove')")
    @Log(title = "监控设备", businessType = BusinessType.DELETE)
    @DeleteMapping("/sipid/{sipId}")
    public AjaxResult remove(@PathVariable String sipId)
    {
        return toAjax(sipDeviceService.deleteSipDeviceBySipId(sipId));
    }
}
