package com.frog.sip.service.impl;

import com.frog.common.utils.DateUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.enums.DeviceChannelStatus;
import com.frog.sip.mapper.SipDeviceChannelMapper;
import com.frog.sip.mapper.SipDeviceMapper;
import com.frog.sip.service.ISipDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 监控设备Service业务层处理
 *
 * @author zhuangpeng.li
 * @date 2022-10-07
 */
@Service("SipDeviceService")
@Slf4j
public class SipDeviceServiceImpl implements ISipDeviceService {
    @Autowired
    private DeviceMapper deviceMapper;
    @Autowired
    private SipDeviceMapper sipDeviceMapper;

    @Autowired
    private SipDeviceChannelMapper sipDeviceChannelMapper;

    @Override
    public boolean exists(String sipId) {
        return sipDeviceMapper.selectSipDeviceBySipId(sipId) != null;
    }

    @Override
    public boolean updateDevice(SipDevice device) {
        SipDevice devicetemp = sipDeviceMapper.selectSipDeviceBySipId(device.getDeviceSipId());
        if (devicetemp == null) {
            return sipDeviceMapper.insertSipDevice(device) > 0;
        }else {
            device.setDeviceId(devicetemp.getDeviceId());
            return sipDeviceMapper.updateSipDevice(device) > 0;
        }
    }

    /**
     * 查询监控设备
     *
     * @param deviceId 监控设备主键
     * @return 监控设备
     */
    @Override
    public SipDevice selectSipDeviceByDeviceId(Long deviceId)
    {
        return sipDeviceMapper.selectSipDeviceByDeviceId(deviceId);
    }

    @Override
    public SipDevice selectSipDeviceBySipId(String sipId)
    {
        return sipDeviceMapper.selectSipDeviceBySipId(sipId);
    }
    /**
     * 查询监控设备列表
     *
     * @param sipDevice 监控设备
     * @return 监控设备
     */
    @Override
    public List<SipDevice> selectSipDeviceList(SipDevice sipDevice)
    {
        return sipDeviceMapper.selectSipDeviceList(sipDevice);
    }

    /**
     * 新增监控设备
     *
     * @param sipDevice 监控设备
     * @return 结果
     */
    @Override
    public int insertSipDevice(SipDevice sipDevice)
    {
        sipDevice.setCreateTime(DateUtils.getNowDate());
        return sipDeviceMapper.insertSipDevice(sipDevice);
    }

    /**
     * 修改监控设备
     *
     * @param sipDevice 监控设备
     * @return 结果
     */
    @Override
    public int updateSipDevice(SipDevice sipDevice)
    {
        sipDevice.setUpdateTime(DateUtils.getNowDate());
        return sipDeviceMapper.updateSipDevice(sipDevice);
    }

    @Override
    public int updateSipDeviceStatus(SipDevice sipDevice) {
        sipDevice.setUpdateTime(DateUtils.getNowDate());
        return sipDeviceMapper.updateSipDeviceStatus(sipDevice);
    }

    @Override
    public void updateSipDeviceOnlineStatus(Integer timeout) {
        List<SipDevice> devs = sipDeviceMapper.selectOfflineSipDevice(timeout);
        devs.forEach(item -> {
            //更新iot设备状态
            Device dev = deviceMapper.selectDeviceBySerialNumber(item.getDeviceSipId());
            if (dev != null) {
                dev.setStatus(4);
                deviceMapper.updateDevice(dev);
            }
            //更新通道状态
            List<SipDeviceChannel> channels = sipDeviceChannelMapper.selectSipDeviceChannelByDeviceSipId(item.getDeviceSipId());
            channels.forEach(citem -> {
                citem.setStatus(DeviceChannelStatus.offline.getValue());
                sipDeviceChannelMapper.updateSipDeviceChannel(citem);
            });
        });
    }

    /**
     * 批量删除监控设备
     *
     * @param deviceIds 需要删除的监控设备主键
     * @return 结果
     */
    @Override
    public int deleteSipDeviceByDeviceIds(Long[] deviceIds) {
        return sipDeviceMapper.deleteSipDeviceByDeviceIds(deviceIds);
    }

    /**
     * 删除监控设备信息
     *
     * @param deviceId 监控设备主键
     * @return 结果
     */
    @Override
    public int deleteSipDeviceByDeviceId(String deviceId)
    {
        return sipDeviceMapper.deleteSipDeviceByDeviceId(Long.valueOf(deviceId));
    }

    @Override
    public int deleteSipDeviceBySipId(String SipId)
    {
        sipDeviceMapper.deleteSipDeviceByByDeviceSipId(SipId);
        return sipDeviceChannelMapper.deleteSipDeviceChannelByDeviceId(SipId);
    }
}
