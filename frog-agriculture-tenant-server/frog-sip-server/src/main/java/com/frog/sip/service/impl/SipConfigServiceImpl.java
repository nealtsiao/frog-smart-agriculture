package com.frog.sip.service.impl;

import com.frog.common.core.redis.RedisCache;
import com.frog.common.utils.DateUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.sip.server.conf.SysSipConfig;
import com.frog.sip.domain.SipConfig;
import com.frog.sip.mapper.SipConfigMapper;
import com.frog.sip.service.ISipConfigService;
import com.frog.sip.server.util.SipUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * sip系统配置Service业务层处理
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
@Service
public class SipConfigServiceImpl implements ISipConfigService {
    @Autowired
    private SipConfigMapper sipConfigMapper;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private SysSipConfig sysSipConfig;

    @Autowired
    private RedisCache redisCache;

    @Override
    public int updateDefaultSipConfig(SipConfig sipConfig) {
        redisCache.setCacheObject(SipUtil.DEFAULT_SIP_CONFIG,sipConfig);
        return 1;
    }

    /**
     * 查询产品下第一条sip系统配置
     *
     * @return sip系统配置
     */
    @Override
    public List<SipConfig> selectSipConfigByProductId(Long productId) {
        return sipConfigMapper.selectSipConfigByProductId(productId);
    }

    @Override
    public List<SipConfig> selectSipConfigByUserId(Long userId) {
        return sipConfigMapper.selectSipConfigByUserId(userId);
    }

    @Override
    public SipConfig selectSipConfigBydeviceSipId(String deviceSipId) {
        Device device = deviceMapper.selectDeviceBySerialNumber(deviceSipId);
        if (device != null) {
            return sipConfigMapper.selectSipConfigByproductId(device.getProductId());
        } else {
            throw new RuntimeException("设备找不到对应的产品");
        }
    }

    /**
     * 查询sip系统配置列表
     *
     * @param sipConfig sip系统配置
     * @return sip系统配置
     */
    @Override
    public List<SipConfig> selectSipConfigList(SipConfig sipConfig) {
        return sipConfigMapper.selectSipConfigList(sipConfig);
    }

    /**
     * 新增sip系统配置
     *
     * @param sipConfig sip系统配置
     * @return 结果
     */
    @Override
    public int insertSipConfig(SipConfig sipConfig) {
        sipConfig.setCreateTime(DateUtils.getNowDate());
        sipConfig.setDomain(sysSipConfig.getDomain());
        sipConfig.setServerSipid(sysSipConfig.getId());
        return sipConfigMapper.insertSipConfig(sipConfig);
    }

    /**
     * 修改sip系统配置
     *
     * @param sipConfig sip系统配置
     * @return 结果
     */
    @Override
    public int updateSipConfig(SipConfig sipConfig) {
        if (sipConfig.getIsdefault() == 1) {
            sipConfigMapper.updatedefaultSipConfig(0);
        }
        sipConfig.setUpdateTime(DateUtils.getNowDate());
        sipConfig.setDomain(sysSipConfig.getDomain());
        sipConfig.setServerSipid(sysSipConfig.getId());
        this.updateDefaultSipConfig(sipConfig);
        return sipConfigMapper.updateSipConfig(sipConfig);
    }

    /**
     * 批量删除sip系统配置
     *
     * @param ids 需要删除的sip系统配置主键
     * @return 结果
     */
    @Override
    public int deleteSipConfigByIds(Long[] ids) {
        return sipConfigMapper.deleteSipConfigByIds(ids);
    }

    @Override
    public int deleteSipConfigByProductIds(Long[] productIds) {
        return sipConfigMapper.deleteSipConfigByProductId(productIds);
    }

    /**
     * 删除sip系统配置信息
     *
     * @param id sip系统配置主键
     * @return 结果
     */
    @Override
    public int deleteSipConfigById(Long id) {
        return sipConfigMapper.deleteSipConfigById(id);
    }
}
