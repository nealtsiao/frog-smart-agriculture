package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 产品对象 iot_product
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Data
public class Product extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品ID */
    private Long productId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品分类ID */
    @Excel(name = "产品分类ID")
    private Long categoryId;

    /** 产品分类名称 */
    @Excel(name = "产品分类名称")
    private String categoryName;

    /** 是否系统通用（0-否，1-是） */
    @Excel(name = "是否系统通用", readConverterExp = "0=-否，1-是")
    private Integer isSys;

    /** 是否启用授权码（0-否，1-是） */
    @Excel(name = "是否启用授权码", readConverterExp = "0=-否，1-是")
    private Integer isAuthorize;

    /** mqtt账号 */
    private String mqttAccount;

    /** mqtt密码 */
    private String mqttPassword;

    /** 产品秘钥 */
    private String mqttSecret;

    public String getMqttSecret() {
        return mqttSecret;
    }

    public void setMqttSecret(String mqttSecret) {
        this.mqttSecret = mqttSecret;
    }

    /** 状态（1-未发布，2-已发布，不能修改） */
    @Excel(name = "状态", readConverterExp = "1==未发布，2=已发布，不能修改")
    private Integer status;

    /** 设备类型（1-直连设备、2-网关子设备、3-网关设备） */
    @Excel(name = "设备类型", readConverterExp = "1=直连设备、2=网关设备、3=监控设备")
    private Integer deviceType;

    /** 联网方式（1=-wifi、2-蜂窝(2G/3G/4G/5G)、3-以太网、4-其他） */
    @Excel(name = "联网方式", readConverterExp = "1=-wifi、2=蜂窝(2G/3G/4G/5G)、3=以太网、4=其他")
    private Integer networkMethod;

    /** 认证方式（1-账号密码、2-证书、3-Http） */
    @Excel(name = "认证方式", readConverterExp = "1=账号密码、2=证书、3=Http")
    private Integer vertificateMethod;

    /** 图片地址 */
    private String imgUrl;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 物模型Json **/
    private String thingsModelsJson;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;
}
