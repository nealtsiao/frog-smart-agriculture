package com.frog.sip.server.listener;

import com.frog.sip.server.handler.IReqHandler;
import com.frog.sip.server.handler.IResHandler;

import javax.sip.SipListener;

public interface IGBListener extends SipListener {
    public void addRequestProcessor(String method, IReqHandler processor);
    public void addResponseProcessor(String method, IResHandler processor);
}
