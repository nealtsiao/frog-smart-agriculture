package com.frog.sip.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 流媒体服务器配置对象 media_server
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
@Data
public class MediaServer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 流媒体配置ID */
    private Long id;

    /** 租户ID */
    @Excel(name = "租户ID")
    private Long tenantId;

    /** 租户名称 */
    @Excel(name = "租户名称")
    private String tenantName;

    /** 使能开关 */
    @Excel(name = "使能开关")
    private Integer enabled;

    /** 默认播放协议 */
    @Excel(name = "默认播放协议")
    private String protocol;

    /** 服务器ip */
    @Excel(name = "服务器ip")
    private String ip;

    /** 服务器域名 */
    @Excel(name = "服务器域名")
    private String domain;

    /** 流媒体密钥 */
    @Excel(name = "流媒体密钥")
    private String secret;

    /** http端口 */
    @Excel(name = "http端口")
    private Long portHttp;

    /** https端口 */
    @Excel(name = "https端口")
    private Long portHttps;

    /** ws端口 */
    @Excel(name = "ws端口")
    private Long portWs;

    /** rtmp端口 */
    @Excel(name = "rtmp端口")
    private Long portRtmp;

    /** rtsp端口 */
    @Excel(name = "rtsp端口")
    private Long portRtsp;

    /** rtp端口范围 */
    @Excel(name = "rtp端口范围")
    private String rtpPortRange;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

}