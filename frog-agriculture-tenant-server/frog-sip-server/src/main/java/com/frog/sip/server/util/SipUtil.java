package com.frog.sip.server.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.sip.RequestEvent;
import javax.sip.message.Request;
import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SipUtil {
    private static final String date_format_T = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String date_format = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_SIP_CONFIG = "DEFAULT_SIP_CONFIG";


    public static String timestampToISO8601(String formatTime) {
        Date date = new Date(Long.parseLong(formatTime)*1000);
        SimpleDateFormat newsdf = new SimpleDateFormat(date_format_T, Locale.getDefault());
        return newsdf.format(date);
    }

    public static long ISO8601Totimestamp(String formatTime) {
        SimpleDateFormat oldsdf = new SimpleDateFormat(date_format_T, Locale.getDefault());
        try {
            return oldsdf.parse(formatTime).getTime()/1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long DateStringToTimestamp(String formatTime) {
        SimpleDateFormat format=new SimpleDateFormat(date_format);
        //设置要读取的时间字符串格式
        Date date;
        try {
            date = format.parse(formatTime);
            //转换为Date类
            return date.getTime()/1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 随机生成via头的branch
     * @return
     */
    public static  String getNewViaBranch() {
        return "z9hG4bK" + RandomStringUtils.randomNumeric(10);
    }

    /**
     * 从报文的SDP中获取ssrc
     * @param sdpStr
     * @return
     */
    public static String getSsrcFromSdp(String sdpStr) {

        // jainSip不支持y= f=字段， 移除以解析。
        int ssrcIndex = sdpStr.indexOf("y=");
        if (ssrcIndex == 0) {
            return null;
        }
        String lines[] = sdpStr.split("\\r?\\n");
        for (String line : lines) {
            if (line.trim().startsWith("y=")) {
                return line.substring(2);
            }
        }
        return null;
    }

    /**
     * 获取xml文本
     * @param evt
     * @return
     * @throws DocumentException
     */
    public static Element getRootElement(RequestEvent evt) throws DocumentException {
        Request request = evt.getRequest();
        //log.info("request:{}",request);
        SAXReader reader = new SAXReader();
        reader.setEncoding("gbk");
        Document xml = reader.read(new ByteArrayInputStream(request.getRawContent()));
        return xml.getRootElement();
    }
}
