package com.frog.sip.server.model;

import lombok.Data;

@Data
public class PtzDirectionInput {
    //1right 2left
    int leftRight;
    // 1up 2down
    int upDown;
    int moveSpeed;
}
