package com.frog.system.service.impl;

import com.frog.common.core.domain.entity.SysDept;
import com.frog.common.utils.DateUtils;
import com.frog.system.domain.Base;
import com.frog.system.mapper.BaseMapper;
import com.frog.system.mapper.SysDeptMapper;
import com.frog.system.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 基地信息Service业务层处理
 * 
 * @author nealtsiao
 * @date 2023-05-13
 */
@Service
public class BaseServiceImpl implements IBaseService
{
    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    @Autowired
    private BaseMapper baseMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询基地信息
     * 
     * @param baseId 基地信息主键
     * @return 基地信息
     */
    @Override
    public Base selectBaseinfoByBaseId(Long baseId)
    {
        return baseMapper.selectBaseByBaseId(baseId);
    }

    /**
     * 查询基地信息列表
     * 
     * @param base 基地信息
     * @return 基地信息
     */
    @Override
    public List<Base> selectBaseinfoList(Base base)
    {
        return baseMapper.selectBaseList(base);
    }

    /**
     * 新增基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    @Override
    public int insertBaseinfo(Base base)
    {
        base.setCreateTime(DateUtils.getNowDate());
        return baseMapper.insertBase(base);
    }

    /**
     * 修改基地信息
     * 
     * @param base 基地信息
     * @return 结果
     */
    @Override
    public int updateBaseinfo(Base base)
    {
        base.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateBase(base);
    }

    /**
     * 批量删除基地信息
     * 
     * @param baseIds 需要删除的基地信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseinfoByBaseIds(Long[] baseIds)
    {
        return baseMapper.deleteBaseByBaseIds(baseIds);
    }

    /**
     * 删除基地信息信息
     * 
     * @param baseId 基地信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseinfoByBaseId(Long baseId)
    {
        return baseMapper.deleteBaseByBaseId(baseId);
    }

    /**
     * 同步基地信息
     * @param dept
     * @param type insert
     * @return
     */
    @Override
    public void synchronizeBaseinfo(SysDept dept,String type) {
        List<SysDept> sysDepts = sysDeptMapper.selectTenantById(dept.getDeptId());
        Long tenantId=0L;
        if(sysDepts.size()==1){
            tenantId = sysDepts.get(0).getDeptId();
        }
        if(INSERT.equals(type)){
            if("base".equals(dept.getDeptType())){
                Base base = new Base();
                base.setTenantId(tenantId);
                base.setDeptId(dept.getDeptId());
                base.setBaseShortName(dept.getDeptName());
                base.setBaseName(dept.getDeptName());
                base.setBaseLeader(dept.getLeader());
                base.setLeaderTel(dept.getPhone());
                baseMapper.insertBase(base);
            }
        }
        if(UPDATE.equals(type)){
            Base base = baseMapper.selectBaseByDeptId(dept.getDeptId());
            if(base !=null && !"base".equals(dept.getDeptType())){
                baseMapper.deleteBaseByBaseId(base.getBaseId());
            }
            if("base".equals(dept.getDeptType())){
                if(base ==null){
                    base = new Base();
                }
                base.setTenantId(tenantId);
                base.setDeptId(dept.getDeptId());
                base.setBaseShortName(dept.getDeptName());
                base.setBaseName(dept.getDeptName());
                base.setBaseLeader(dept.getLeader());
                base.setLeaderTel(dept.getPhone());
                if(base.getBaseId()==null){
                    baseMapper.insertBase(base);
                }else{
                    baseMapper.updateBase(base);
                }
            }
        }
        if(DELETE.equals(type)){
            Base base = baseMapper.selectBaseByDeptId(dept.getDeptId());
            if(base !=null){
                baseMapper.deleteBaseByBaseId(base.getBaseId());
            }
        }
    }
}
