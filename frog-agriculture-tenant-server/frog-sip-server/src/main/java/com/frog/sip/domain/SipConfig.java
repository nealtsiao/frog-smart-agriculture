package com.frog.sip.domain;

import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * sip系统配置对象 sip_config
 *
 * @author zhuangpeng.li
 * @date 2023-02-21
 */
@Data
public class SipConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 配置ID */
    private Long id;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long productId;

    /**
     * 产品名称
     */
    @Excel(name = "产品名称")
    private String productName;

    /**
     * 使能开关
     */
    @Excel(name = "使能开关")
    private Integer enabled;

    /**
     * 系统默认配置
     */
    @Excel(name = "系统默认配置")
    private Integer isdefault;

    /**
     * 拓展sdp
     */
    @Excel(name = "拓展sdp")
    private Integer seniorsdp;

    /**
     * 服务器域
     */
    @Excel(name = "服务器域")
    private String domain;

    /**
     * 服务器sipid
     */
    @Excel(name = "服务器sipid")
    private String serverSipid;

    /** sip认证密码 */
    @Excel(name = "sip认证密码")
    private String password;

    /** sip接入IP */
    @Excel(name = "sip接入IP")
    private String ip;

    /** sip接入端口号 */
    @Excel(name = "sip接入端口号")
    private Integer port;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private Long userId;
    private Long tenantId;
    private Long baseId;
    private Long deptId;
}