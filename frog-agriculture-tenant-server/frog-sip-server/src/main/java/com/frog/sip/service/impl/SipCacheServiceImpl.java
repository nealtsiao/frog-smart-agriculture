package com.frog.sip.service.impl;

import com.frog.common.core.redis.RedisCache;
import com.frog.sip.server.model.*;
import com.frog.sip.service.ISipCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
public class SipCacheServiceImpl implements ISipCacheService {

    @Autowired
    private RedisCache redisCache;

    /* 流信息缓存前缀 */
    private String sipStreamKey = "SIP_STREAM:";
    public static final String sipCseqKey = "SIP_CSEQ:";
    private static String sipDialogKey = "SIP_DIALOG:";

    public static final String sipRecordInfoKey = "SIP_RECORD_INFO:";

    public static final String sipUnicodeFront14B  = "SIP_UNICODE_FRONT_14B:";
    public static final String sipSsrcKey = "SIP_SSRC:";

    public static final String ysPlaybackUserStream="YS_PLAYBACK_USER_STREAM:";

    @Override
    public Long getCSEQ(String serverSipId) {
        String key = sipCseqKey + serverSipId;
        long result = redisCache.incr(key, 1L);
        if (result > Integer.MAX_VALUE) {
            redisCache.setCacheObject(key, 1);
            result = 1;
        }
        return result;
    }

    @Override
    public void addStream(Stream stream) {
        redisCache.setCacheObject(sipStreamKey +stream.getStreamId(), stream);
    }

    @Override
    public Stream getStreamByStreamId(String streamId) {
        return (Stream) redisCache.getCacheObject(sipStreamKey +streamId);
    }

    @Override
    public void addMediaInfo(ZlmMediaServer mediaServerConfig) {
        redisCache.setCacheObject("media_server_config", mediaServerConfig);
    }

    @Override
    public ZlmMediaServer getMediaInfo() {
        return (ZlmMediaServer) redisCache.getCacheObject("media_server_config");
    }

    @Override
    public void setRecordList(String key, RecordList recordList) {
        redisCache.setCacheObject(sipRecordInfoKey+key, recordList);
    }

    @Override
    public RecordList getRecordList(String key) {
        RecordList recordList = redisCache.getCacheObject(sipRecordInfoKey+key);
        return recordList;
    }


    @Override
    public void addDialog(DialogInfo sipDialogInfo) {
        redisCache.setCacheObject(sipDialogKey +sipDialogInfo.getStreamId(),sipDialogInfo);
    }

    @Override
    public DialogInfo getDialog(String streamId) {
        DialogInfo dialogInfo = redisCache.getCacheObject(sipDialogKey + streamId);
        return dialogInfo;
    }

    @Override
    public void removeDialog(String streamId) {
        DialogInfo dialogInfo = redisCache.getCacheObject(sipDialogKey + streamId);
        releaseSsrc(dialogInfo.getSsrc());
        redisCache.deleteObject(sipDialogKey +streamId);
    }

    /**
     *
     * @param domain
     * @return
     */
    @Override
    public String getPlaySsrc(String domain) {
        boolean containsKey = redisCache.containsKey(sipSsrcKey);
        if(!containsKey) {
            for (int i = 1; i < 100; i++) {
                if (i < 10) {
                    cacheSSrc(domain,"000",i);
                } else if (i < 100) {
                    cacheSSrc(domain,"00",i);
                }
            }
        }
        Set<String> listKeyByPrefix = redisCache.getListKeyByPrefix(sipSsrcKey);
        for(String ssrcKey:listKeyByPrefix){
            Ssrc ssrc = redisCache.getCacheObject(ssrcKey);
            if(ssrc.getIsUsed()==0){
                ssrc.setIsUsed(1);
                redisCache.setCacheObject(ssrcKey,ssrc);
                return ssrc.getSsrc();
            }
        }
        log.info("[ssrc已经耗尽]");
        return "";
    }

    @Override
    public void releaseSsrc(String ssrc) {
        String ssrcKey = sipSsrcKey + ssrc;
        Ssrc ssrcObj = redisCache.getCacheObject(ssrcKey);
        ssrcObj.setIsUsed(0);
        redisCache.setCacheObject(ssrcKey,ssrcObj);
    }

    @Override
    public int getSipUnicodeFront14B(String deviceId) {
        Integer cacheObject = redisCache.getCacheObject(sipUnicodeFront14B + deviceId);
        if(cacheObject==null){
            return 0;
        }else{
            return cacheObject;
        }

    }

    /**
     * 设置萤石云回播地址ID
     * @param userId
     * @param urlId
     */
    @Override
    public void setPlaybackUserStream(Long userId, String urlId) {
        redisCache.setCacheObject(ysPlaybackUserStream+userId,urlId);
    }

    /**
     * 根据用户ID获取回播地址ID
     * @param userId
     * @return
     */
    @Override
    public String getPlaybackUserStream(Long userId) {
        try {
            String urlId = redisCache.getCacheObject(ysPlaybackUserStream + userId);
            return urlId;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void IncrSipUnicodeFront14B(String deviceId, Long num) {
        redisCache.incr(sipUnicodeFront14B+deviceId,num);
    }


    /**
     * 缓存ssrc
     * @param domain
     * @param perfix
     * @param i
     */
    private void cacheSSrc(String domain,String perfix ,int i){
        String sn = perfix + domain.substring(3, 8) + i;

        Ssrc ssrcObj = new Ssrc();
        String ssrc = "0"+sn;
        ssrcObj.setSsrc(ssrc);
        ssrcObj.setIsUsed(0);
        String ssrcKey = sipSsrcKey + ssrc;
        redisCache.setCacheObject(ssrcKey, ssrcObj);
    }
}
