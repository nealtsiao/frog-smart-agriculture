package com.frog.iot.service.impl;

import java.util.List;

import com.frog.common.annotation.TenantScope;
import com.frog.common.core.domain.entity.SysRole;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.domain.model.LoginUser;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.iot.mapper.ThingsModelTemplateMapper;
import com.frog.iot.domain.ThingsModelTemplate;
import com.frog.iot.service.IThingsModelTemplateService;

import static com.frog.common.utils.SecurityUtils.getLoginUser;

/**
 * 通用物模型Service业务层处理
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Service
public class ThingsModelTemplateServiceImpl implements IThingsModelTemplateService 
{
    @Autowired
    private ThingsModelTemplateMapper thingsModelTemplateMapper;

    /**
     * 查询通用物模型
     * 
     * @param templateId 通用物模型主键
     * @return 通用物模型
     */
    @Override
    public ThingsModelTemplate selectThingsModelTemplateByTemplateId(Long templateId)
    {
        return thingsModelTemplateMapper.selectThingsModelTemplateByTemplateId(templateId);
    }

    /**
     * 查询通用物模型列表
     * 
     * @param thingsModelTemplate 通用物模型
     * @return 通用物模型
     */
    @Override
    @TenantScope
    public List<ThingsModelTemplate> selectThingsModelTemplateList(ThingsModelTemplate thingsModelTemplate)
    {
        return thingsModelTemplateMapper.selectThingsModelTemplateList(thingsModelTemplate);
    }

    /**
     * 新增通用物模型
     * 
     * @param thingsModelTemplate 通用物模型
     * @return 结果
     */
    @Override
    public int insertThingsModelTemplate(ThingsModelTemplate thingsModelTemplate)
    {
        // 判断是否为管理员
        SysUser user = getLoginUser().getUser();
        if(user.isAdmin()){
            thingsModelTemplate.setIsSys(1);
        }else{
            thingsModelTemplate.setIsSys(0);
        }
        thingsModelTemplate.setTenantId(SecurityUtils.getTenantId());
        thingsModelTemplate.setBaseId(SecurityUtils.getBaseId());
        thingsModelTemplate.setDeptId(SecurityUtils.getDeptId());
        thingsModelTemplate.setUserId(SecurityUtils.getUserId());

        thingsModelTemplate.setCreateBy(SecurityUtils.getUserId().toString());
        thingsModelTemplate.setCreateTime(DateUtils.getNowDate());
        return thingsModelTemplateMapper.insertThingsModelTemplate(thingsModelTemplate);
    }

    /**
     * 修改通用物模型
     * 
     * @param thingsModelTemplate 通用物模型
     * @return 结果
     */
    @Override
    public int updateThingsModelTemplate(ThingsModelTemplate thingsModelTemplate)
    {
        thingsModelTemplate.setUpdateBy(SecurityUtils.getUserId().toString());
        thingsModelTemplate.setUpdateTime(DateUtils.getNowDate());
        return thingsModelTemplateMapper.updateThingsModelTemplate(thingsModelTemplate);
    }

    /**
     * 批量删除通用物模型
     * 
     * @param templateIds 需要删除的通用物模型主键
     * @return 结果
     */
    @Override
    public int deleteThingsModelTemplateByTemplateIds(Long[] templateIds)
    {
        return thingsModelTemplateMapper.deleteThingsModelTemplateByTemplateIds(templateIds);
    }

    /**
     * 删除通用物模型信息
     * 
     * @param templateId 通用物模型主键
     * @return 结果
     */
    @Override
    public int deleteThingsModelTemplateByTemplateId(Long templateId)
    {
        return thingsModelTemplateMapper.deleteThingsModelTemplateByTemplateId(templateId);
    }
}
