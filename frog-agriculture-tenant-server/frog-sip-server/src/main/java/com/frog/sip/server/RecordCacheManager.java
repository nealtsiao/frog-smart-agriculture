package com.frog.sip.server;

import com.frog.sip.server.model.RecordList;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class RecordCacheManager {
    private final ConcurrentHashMap<String, RecordList> recordMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, ReentrantLock> lockMap = new ConcurrentHashMap<>();

    public  void put(String key,RecordList transaction){
        recordMap.put(key, transaction);
    }

    public  RecordList getRecordList(String key){
        return recordMap.get(key);
    }

    public void remove(String key) {
        recordMap.remove(key);
        lockMap.remove(key);
    }

    public void addlock(String key){
        lockMap.put(key,new ReentrantLock());
    }

    public ReentrantLock getlock(String key){
        return lockMap.get(key);
    }


}
