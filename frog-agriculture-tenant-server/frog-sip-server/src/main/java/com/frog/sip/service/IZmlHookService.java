package com.frog.sip.service;

import com.alibaba.fastjson.JSONObject;
import com.frog.sip.server.model.Stream;

public interface IZmlHookService {
    String onHttpAccess(JSONObject json);
    String onPlay(JSONObject json);
    String onPublish(JSONObject json);
    String onStreamNoneReader(JSONObject json);
    String onStreamNotFound(JSONObject json);
    String onStreamChanged(JSONObject json);
    Stream updateStream(Stream streamInfo);
}
