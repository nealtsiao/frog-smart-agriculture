package com.frog.sip.service.impl.camera.play;

import com.alibaba.fastjson.JSON;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.model.DialogInfo;
import com.frog.sip.server.model.Stream;
import com.frog.sip.server.cmd.IRtspCmd;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.util.StreamIdUtil;
import com.frog.sip.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sipPlay")
@Slf4j
public class SipPlayServiceImpl implements IPlayService {

    @Autowired
    private ISipCmd sipCmd;

    @Autowired
    private IRtspCmd rtspCmd;

    @Autowired
    private IZmlHookService zmlHookService;

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private ISipDeviceService sipDeviceService;


    @Autowired
    private StreamIdUtil streamIdUtil;

    /**
     * 开始播放
     * @param deviceId sipDevice的device_sip_id
     * @param channelId sip_device_channnel的channel_sip_id
     * @return 当前播放流的信息
     */
    @Override
    public Stream play(String deviceId, String channelId) {
        SipDevice sipDevice = sipDeviceService.selectSipDeviceBySipId(deviceId);
        String streamId = streamIdUtil.getPlayStreamId(sipDevice.getDeviceSipId(),channelId);
        //早播放之前检查是否有存在未结束的视频播放对话
        DialogInfo dialog = sipCacheService.getDialog(streamId);
        if(dialog!=null){
            sipCmd.streamByeCmd(streamId);
        }
        sipCmd.playStreamCmd(sipDevice, channelId, streamId);
        Stream  streamInfo = new Stream(sipDevice.getDeviceSipId(), channelId, streamId);

        // 拼接播放地址返回stream对象
        return zmlHookService.updateStream(streamInfo);
    }

    /**
     * 停止播放
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @Override
    public String playStop(String deviceId, String channelId, String streamId) {
        sipCmd.streamByeCmd(streamId);
        return null;
    }

    /**
     * 获取流信息
     * @param deviceId
     * @param channelId
     * @return
     */
    @Override
    public String getPlayStream(String deviceId, String channelId) {
        String streamId = streamIdUtil.getPlayStreamId(deviceId, channelId);
        Stream streamInfo = sipCacheService.getStreamByStreamId(streamId);
        if (streamInfo != null) {
            return JSON.toJSONString(zmlHookService.updateStream(streamInfo));
        }
        return "";
    }

    /**
     * 开始播放设备录像
     * @param deviceId
     * @param channelId
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public Stream playback(String deviceId, String channelId, String startTime, String endTime) {
        SipDevice sipDevice = sipDeviceService.selectSipDeviceBySipId(deviceId);
        String streamId = streamIdUtil.getPlayBackStreamId(sipDevice.getDeviceSipId(),channelId);
        //先要停掉当前的流
        sipCmd.streamByeCmd(streamId);
        sipCmd.playbackStreamCmd(sipDevice, channelId, streamId, startTime, endTime);
        Stream streaminfo = new Stream(sipDevice.getDeviceSipId(), channelId, streamId);
        streaminfo.setStreamId(streamIdUtil.getPlayBackStreamId(deviceId,channelId));
        return zmlHookService.updateStream(streaminfo);
    }

    /**
     * 停止播放设备录像
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @Override
    public String playbackStop(String deviceId, String channelId, String streamId) {
        sipCmd.streamByeCmd(streamId);
        return null;
    }

    /**
     * 暂停播放录像
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @Override
    public String playbackPause(String deviceId, String channelId, String streamId) {
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        Stream streamInfo = sipCacheService.getStreamByStreamId(streamId);
        if (null == streamInfo) {
            return "streamId不存在";
        }
        rtspCmd.setCseq(streamInfo.getSsrc());
        rtspCmd.playPause(dev, channelId, streamInfo.getStreamId());
        return null;
    }

    /**
     * 重新播放录像
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @Override
    public String playbackReplay(String deviceId, String channelId, String streamId) {
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        Stream streamInfo = sipCacheService.getStreamByStreamId(streamId);
        if (null == streamInfo) {
            return "streamId不存在";
        }
        rtspCmd.setCseq(streamInfo.getSsrc());
        rtspCmd.playReplay(dev, channelId, streamInfo.getStreamId());
        return null;
    }

    /**
     * 选时播放录像
     * @param deviceId
     * @param channelId
     * @param streamId
     * @param seektime
     * @return
     */
    @Override
    public String playbackSeek(String deviceId, String channelId, String streamId, long firstVideoTime,long seekTime) {
        Long range = seekTime-firstVideoTime;
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        Stream streamInfo = sipCacheService.getStreamByStreamId(streamId);
        if (null == streamInfo) {
            return "streamId不存在";
        }
        rtspCmd.setCseq(streamId);
        rtspCmd.playBackSeek(dev, channelId, streamId, range);
        return null;
    }

    /**
     * 播放录像速度
     * @param deviceId
     * @param channelId
     * @param streamId
     * @param speed
     * @return
     */
    @Override
    public String playbackSpeed(String deviceId, String channelId, String streamId, Integer speed) {
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        Stream streamInfo = sipCacheService.getStreamByStreamId(streamId);
        if (null == streamInfo) {
            return "streamId不存在";
        }
        rtspCmd.setCseq(streamInfo.getSsrc());
        rtspCmd.playBackSpeed(dev, channelId, streamInfo.getStreamId(), speed);
        return null;
    }
}
