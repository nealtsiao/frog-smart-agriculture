package com.frog.sip.server.cmd.impl;

import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.cmd.IRtspCmd;
import com.frog.sip.server.model.DialogInfo;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.server.transcation.SipSender;
import com.frog.sip.service.ISipCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sip.ClientTransaction;
import javax.sip.InvalidArgumentException;
import javax.sip.SipException;
import javax.sip.SipProvider;
import javax.sip.message.Request;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class RtspCmdImpl implements IRtspCmd {
    public static Map<String, Long> CSEQCACHE = new ConcurrentHashMap<>();

    @Autowired
    private HeaderBuilder headerBuilder;

    @Autowired
    @Qualifier(value = "udpSipServer")
    private SipProvider sipserver;

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private SipSender sipSender;
    public void playPause(SipDevice device, String channelId, String streamId) {
        try {
            String content = "PAUSE RTSP/1.0\r\n" +
                    "CSeq: " + CSEQCACHE.get(streamId) + "\r\n" +
                    "PauseTime: now\r\n";
            DialogInfo dialog = sipCacheService.getDialog(streamId);
            Request request = headerBuilder.createInfoRequest(dialog, content);
            ClientTransaction clientTransaction = sipserver.getNewClientTransaction(request);
            clientTransaction.sendRequest();

        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    public void playReplay(SipDevice device, String channelId, String streamId) {
        try {
            String content = "PLAY RTSP/1.0\r\n" +
                                "CSeq: " + CSEQCACHE.get(streamId) + "\r\n" +
                                "Range: npt=now-\r\n";
            DialogInfo dialog = sipCacheService.getDialog(streamId);
            Request request = headerBuilder.createInfoRequest(dialog, content);
            ClientTransaction clientTransaction = sipserver.getNewClientTransaction(request);
            clientTransaction.sendRequest();
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    public void playBackSeek(SipDevice device, String channelId, String streamId, long seektime) {
        try {
            String content = "PLAY RTSP/1.0\r\n" +
                    "CSeq: " + CSEQCACHE.get(streamId) + "\r\n" +
                    "Range: npt=" + seektime + "-\r\n";
            DialogInfo dialog = sipCacheService.getDialog(streamId);
            Request request = headerBuilder.createInfoRequest(dialog, content);
            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }
    }

    public void playBackSpeed(SipDevice device, String channelId, String streamId, Integer speed) {
        try {
            String content = "PLAY RTSP/1.0\r\n" +
                    "CSeq: " + CSEQCACHE.get(streamId) + "\r\n" +
                    "Scale: " + speed + ".000000\r\n";
            DialogInfo dialog = sipCacheService.getDialog(streamId);
            Request request = headerBuilder.createInfoRequest(dialog, content);
            sipSender.transmit(request);
        } catch (SipException | ParseException | InvalidArgumentException e) {
            e.printStackTrace();
        }

    }

    public void setCseq(String streamId) {
        if (CSEQCACHE.containsKey(streamId)) {
            CSEQCACHE.put(streamId, CSEQCACHE.get(streamId) + 1);
        } else {
            CSEQCACHE.put(streamId, 2l);
        }
    }
}
