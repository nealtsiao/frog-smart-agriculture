package com.frog.view.service.impl;

import java.beans.Transient;
import java.util.List;

import com.frog.common.annotation.TenantScope;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.view.mapper.GoViewInfoMapper;
import com.frog.view.domain.GoViewInfo;
import com.frog.view.service.IGoViewInfoService;

/**
 * goview大屏Service业务层处理
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@Service
public class GoViewInfoServiceImpl implements IGoViewInfoService
{
    @Autowired
    private GoViewInfoMapper GoViewInfoMapper;

    /**
     * 查询goview大屏
     * 
     * @param id goview大屏主键
     * @return goview大屏
     */
    @Override
    public GoViewInfo selectGoViewInfoById(String id)
    {
        return GoViewInfoMapper.selectGoViewInfoById(id);
    }

    /**
     * 查询goview大屏列表
     * 
     * @param GoViewInfo goview大屏
     * @return goview大屏
     */
    @Override
    @TenantScope
    public List<GoViewInfo> selectGoViewInfoList(GoViewInfo GoViewInfo)
    {
        return GoViewInfoMapper.selectGoViewInfoList(GoViewInfo);
    }

    /**
     * 新增goview大屏
     * 
     * @param GoViewInfo goview大屏
     * @return 结果
     */
    @Override
    public int insertGoViewInfo(GoViewInfo GoViewInfo)
    {
        GoViewInfo.setCreateBy(SecurityUtils.getUserId().toString());
        GoViewInfo.setCreateTime(DateUtils.getNowDate());
        GoViewInfo.setTenantId(SecurityUtils.getTenantId());
        GoViewInfo.setBaseId(SecurityUtils.getBaseId());
        GoViewInfo.setDeptId(SecurityUtils.getDeptId());
        GoViewInfo.setUserId(SecurityUtils.getUserId());
        return GoViewInfoMapper.insertGoViewInfo(GoViewInfo);
    }

    /**
     * 修改goview大屏
     * 
     * @param GoViewInfo goview大屏
     * @return 结果
     */
    @Override
    public int updateGoViewInfo(GoViewInfo GoViewInfo)
    {
        GoViewInfo.setUpdateBy(SecurityUtils.getUserId().toString());
        GoViewInfo.setUpdateTime(DateUtils.getNowDate());
        return GoViewInfoMapper.updateGoViewInfo(GoViewInfo);
    }

    /**
     * 批量删除goview大屏
     * 
     * @param ids 需要删除的goview大屏主键
     * @return 结果
     */
    @Override
    public int deleteGoViewInfoByIds(String[] ids)
    {
        return GoViewInfoMapper.deleteGoViewInfoByIds(ids);
    }

    /**
     * 删除goview大屏信息
     * 
     * @param id goview大屏主键
     * @return 结果
     */
    @Override
    public int deleteGoViewInfoById(String id)
    {
        return GoViewInfoMapper.deleteGoViewInfoById(id);
    }
}
