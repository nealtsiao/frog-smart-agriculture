package com.frog.view.mapper;

import java.util.List;
import com.frog.view.domain.GoViewPrivateImages;

/**
 * goview用户私有图片Mapper接口
 * 
 * @author nealtsiao
 * @date 2024-11-25
 */
public interface GoViewPrivateImagesMapper 
{
    /**
     * 查询goview用户私有图片
     * 
     * @param id goview用户私有图片主键
     * @return goview用户私有图片
     */
    public GoViewPrivateImages selectGoViewPrivateImagesById(Long id);

    /**
     * 查询goview用户私有图片列表
     * 
     * @param goViewPrivateImages goview用户私有图片
     * @return goview用户私有图片集合
     */
    public List<GoViewPrivateImages> selectGoViewPrivateImagesList(GoViewPrivateImages goViewPrivateImages);

    /**
     * 新增goview用户私有图片
     * 
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    public int insertGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages);

    /**
     * 修改goview用户私有图片
     * 
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    public int updateGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages);

    /**
     * 删除goview用户私有图片
     * 
     * @param id goview用户私有图片主键
     * @return 结果
     */
    public int deleteGoViewPrivateImagesById(Long id);

    /**
     * 批量删除goview用户私有图片
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGoViewPrivateImagesByIds(Long[] ids);
}
