package com.frog.sip.service.impl.camera.ptz;

import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.model.PtzDirectionInput;
import com.frog.sip.server.model.PtzZoomInput;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.service.IPtzCmdService;
import com.frog.sip.service.ISipDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service("sipPtz")
public class SipPtzCmdServiceImpl implements IPtzCmdService {

    @Autowired
    private ISipCmd sipCmd;

    @Autowired
    private ISipDeviceService sipDeviceService;

    @Override
    public boolean direction(String deviceId, String channelId, PtzDirectionInput ptzDirectionInput) {
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        if (dev != null) {
            return sipCmd.ptzCmd(dev, channelId, ptzDirectionInput.getLeftRight(), ptzDirectionInput.getUpDown(), 0, ptzDirectionInput.getMoveSpeed(), 0);
        }
        return false;
    }

    @Override
    public boolean zoom(String deviceId, String channelId, PtzZoomInput ptzZoomInput) {
        SipDevice dev = sipDeviceService.selectSipDeviceBySipId(deviceId);
        if (dev != null) {
            return sipCmd.ptzCmd(dev, channelId, 0, 0, ptzZoomInput.getInOut(), 0, ptzZoomInput.getZoomSpeed());
        }
        return false;
    }
}
