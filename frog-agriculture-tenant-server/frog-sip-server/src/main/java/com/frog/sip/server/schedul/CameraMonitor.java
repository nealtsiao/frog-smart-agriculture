package com.frog.sip.server.schedul;

import com.frog.iot.domain.Device;
import com.frog.iot.mqtt.EmqxService;
import com.frog.iot.service.IDeviceService;
import com.frog.sip.server.conf.SysSipConfig;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.enums.DeviceChannelStatus;
import com.frog.sip.server.enums.SipDeviceStatus;
import com.frog.sip.service.IMqttService;
import com.frog.sip.service.ISipDeviceChannelService;
import com.frog.sip.service.ISipDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 监测摄像头在线状态
 * @author neal
 * @date 2024/4/11  18:19
 */
@Component
@Slf4j
public class CameraMonitor {
    @Autowired
    ISipDeviceService sipDeviceService;
    @Autowired
    IDeviceService deviceService;
    @Autowired
    ISipDeviceChannelService sipDeviceChannelService;
    @Autowired
    private EmqxService emqxService;
    @Autowired
    private SysSipConfig sysSipConfig;
    @Autowired
    private IMqttService mqttService;

    @Scheduled(fixedRateString = "${sip.monitorTime}")
    public void monitorCameraStatus() {
        log.info("开始监测监控状态");
        List<SipDevice> sipDevices = sipDeviceService.selectSipDeviceList(new SipDevice());
        Date currentTime = new Date();
        List<SipDevice> collect = sipDevices.stream()
                .filter(device -> {
                    Date lastConnectTime = device.getLastconnecttime();
                    if (lastConnectTime == null) {
                        return false;
                    }
                    long diffInSeconds = (currentTime.getTime() - lastConnectTime.getTime()) / 1000;
                    return diffInSeconds > sysSipConfig.getMonitorTime() && device.getOnline().equals(SipDeviceStatus.online.getStatus());
                }).collect(Collectors.toList());
                for(SipDevice sipDevice:collect) {
                    try {
                        if(sipDevice==null) continue;
                        //设置sipDevice的在线状态为离线4
                        sipDevice.setOnline(SipDeviceStatus.offline.getStatus());
                        sipDeviceService.updateSipDeviceStatus(sipDevice);

                        //发布设备状态到iot设备，跟新设备状态和发布主题到前端
                        mqttService.publishSipDeviceInfoToInfoPost(sipDevice);

                        /** 记录设备离线日志 */
                        Device iotDevice = deviceService.selectShortDeviceBySerialNumber(sipDevice.getDeviceSipId());
                        iotDevice.setStatus(4);
                        deviceService.updateDeviceStatusAndLocation(iotDevice, "");

                        //手动更新通道，因为设备断电离线，无法发送CataLog来获取通道状态，设备不在线设备下面的通道全部设置为离线状态
                        List<SipDeviceChannel> channels = sipDeviceChannelService.selectSipDeviceChannelByDeviceSipId(sipDevice.getDeviceSipId());
                        if (channels != null && !channels.isEmpty()) {
                            for (SipDeviceChannel channel : channels) {
                                channel.setStatus(DeviceChannelStatus.offline.getValue());
                                sipDeviceChannelService.updateChannel(sipDevice.getDeviceSipId(), channel);
                            }
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                };
    }
}
