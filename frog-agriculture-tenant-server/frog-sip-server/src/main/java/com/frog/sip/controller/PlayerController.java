package com.frog.sip.controller;

import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.iot.service.IDeviceService;
import com.frog.iot.service.IProductService;
import com.frog.iot.service.IYsConfigService;
import com.frog.iot.util.YsApiUtils;
import com.frog.sip.service.IPlayService;
import com.frog.sip.service.impl.camera.CameraFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/sip/player")
@Api(tags = "sip-流媒体播放")
public class PlayerController extends BaseController {
    @Autowired
    private IProductService productService;
    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private YsApiUtils ysApiUtils;
    @Autowired
    private IYsConfigService ysConfigService;
    @Autowired
    private CameraFactory cameraFactory;

    /**
     * 开始推流，sip协议需要，其他的平台不需要
     * @param deviceId
     * @param channelId
     * @return
     */
    @GetMapping("/play/{deviceId}/{channelId}")
    @ApiOperation("开始推流")
    public AjaxResult play(@PathVariable String deviceId, @PathVariable String channelId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.play(deviceId, channelId));
    }

    /**
     * 停止推流，sip协议需要其他的协议不需要
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @GetMapping("/playstop/{deviceId}/{channelId}/{streamId}")
    @ApiOperation("停止推流")
    public AjaxResult playStop(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playStop(deviceId, channelId, streamId));
    }

    /**
     * 获取播放地址
     * @param deviceId
     * @param channelId
     * @return
     */
    @ApiOperation("获取播放地址")
    @GetMapping("/playstream/{deviceId}/{channelId}")
    public AjaxResult getPlayStream(@PathVariable String deviceId, @PathVariable String channelId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.getPlayStream(deviceId, channelId));
    }

    /**
     * 视频回播
     * @param deviceId
     * @param channelId
     * @param start
     * @param end
     * @return
     */
    @ApiOperation("视频回播")
    @GetMapping("/playback/{deviceId}/{channelId}")
    public AjaxResult playback(@PathVariable String deviceId,
                               @PathVariable String channelId, String start, String end) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playback(deviceId, channelId, start, end));
    }

    /**
     * 停止回播
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @ApiOperation("停止回播")
    @GetMapping("/playbackStop/{deviceId}/{channelId}/{streamId}")
    public AjaxResult playbackStop(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playbackStop(deviceId, channelId, streamId));
    }

    /**
     * 回播暂停
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @ApiOperation("回播暂停")
    @GetMapping("/playbackPause/{deviceId}/{channelId}/{streamId}")
    public AjaxResult playbackPause(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playbackPause(deviceId, channelId, streamId));
    }

    /**
     * 回播重播
     * @param deviceId
     * @param channelId
     * @param streamId
     * @return
     */
    @ApiOperation("回播重播")
    @GetMapping("/playbackReplay/{deviceId}/{channelId}/{streamId}")
    public AjaxResult playbackReplay(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playbackReplay(deviceId, channelId, streamId));
    }

    /**
     * 寻时回播
     * @param deviceId
     * @param channelId
     * @param streamId
     * @param firstVideoStart 第一段视频的时间
     * @param seekTime 点击的时间
     * @return
     */
    @ApiOperation("回播寻时")
    @GetMapping("/playbackSeek/{deviceId}/{channelId}/{streamId}")
    public AjaxResult playbackSeek(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId, long firstVideoStart,long seekTime) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playbackSeek(deviceId, channelId, streamId, firstVideoStart,seekTime));
    }

    /**
     * 回播调整播放速度
     * @param deviceId
     * @param channelId
     * @param streamId
     * @param speed
     * @return
     */
    @ApiOperation("回播调整播放速度")
    @GetMapping("/playbackSpeed/{deviceId}/{channelId}/{streamId}")
    public AjaxResult playbackSpeed(@PathVariable String deviceId, @PathVariable String channelId, @PathVariable String streamId, Integer speed) {
        IPlayService playService = cameraFactory.createPlay(deviceId);
        return AjaxResult.success("success!", playService.playbackSpeed(deviceId, channelId, streamId, speed));
    }
}
