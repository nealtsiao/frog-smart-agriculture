package com.frog.iot.service.impl;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import com.frog.common.utils.StringUtils;
import com.frog.iot.domain.Product;
import com.frog.iot.mapper.ProductMapper;
import com.frog.iot.service.IProductService;
import com.frog.iot.util.YsApiUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.iot.mapper.YsConfigMapper;
import com.frog.iot.domain.YsConfig;
import com.frog.iot.service.IYsConfigService;

/**
 * 萤石系统配置Service业务层处理
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@Slf4j
@Service
public class YsConfigServiceImpl implements IYsConfigService
{
    @Autowired
    private YsConfigMapper ysConfigMapper;
    @Autowired
    private YsApiUtils ysApiUtils;

    /**
     * 查询萤石系统配置
     * 
     * @param id 萤石系统配置主键
     * @return 萤石系统配置
     */
    @Override
    public YsConfig selectYsConfigByProductId(Long id)
    {
        return ysConfigMapper.selectYsConfigByProductId(id);
    }
    /**
     * 新增萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    @Override
    public int insertYsConfig(YsConfig ysConfig)
    {
        ysConfig.setCreateBy(SecurityUtils.getUserId().toString());
        ysConfig.setCreateTime(DateUtils.getNowDate());
        return ysConfigMapper.insertYsConfig(ysConfig);
    }

    /**
     * 修改萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    @Override
    public int updateYsConfig(YsConfig ysConfig)
    {
        ysConfig.setUpdateBy(SecurityUtils.getUserId().toString());
        ysConfig.setUpdateTime(DateUtils.getNowDate());
        return ysConfigMapper.updateYsConfig(ysConfig);
    }

    /**
     * 获取萤石云设备分页列表
     * @param productId
     * @param pageSize
     * @param pageStart
     * @return
     */
    @Override
    public JSONObject getDeviceList(Long productId,int pageSize,int pageStart) {
        try {
            return ysApiUtils.getDeviceList(productId, pageSize, pageStart);
        } catch (Exception e) {
            log.error(String.format("获取设备列表异常:%s",e));
            // Log the exception and rethrow
            return null;
        }
    }
}
