package com.frog.view.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.enums.BusinessType;
import com.frog.view.domain.GoViewPrivateImages;
import com.frog.view.service.IGoViewPrivateImagesService;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.common.core.page.TableDataInfo;

/**
 * goview用户私有图片Controller
 *
 * @author nealtsiao
 * @date 2024-11-25
 */
@Api(tags = "view-GoView用户私有图片")
@RestController
@RequestMapping("/view/goViewImages")
public class GoViewPrivateImagesController extends BaseController {
    @Autowired
    private IGoViewPrivateImagesService goViewPrivateImagesService;

    /**
     * 查询goview用户私有图片列表
     */
    @ApiOperation("查询GoView私有图片列表")
    @GetMapping("/list")
    public TableDataInfo list(GoViewPrivateImages goViewPrivateImages) {
        startPage();
        List<GoViewPrivateImages> list = goViewPrivateImagesService.selectGoViewPrivateImagesList(goViewPrivateImages);
        return getDataTable(list);
    }

    /**
     * 导出goview用户私有图片列表
     */
    @ApiOperation("导出GoView私有图片列表")
    @Log(title = "goview用户私有图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoViewPrivateImages goViewPrivateImages) {
        List<GoViewPrivateImages> list = goViewPrivateImagesService.selectGoViewPrivateImagesList(goViewPrivateImages);
        ExcelUtil<GoViewPrivateImages> util = new ExcelUtil<GoViewPrivateImages>(GoViewPrivateImages.class);
        util.exportExcel(response, list, "goview用户私有图片数据");
    }

    /**
     * 获取goview用户私有图片详细信息
     */
    @ApiOperation("获取GoView私有图片详细信息")
    @PreAuthorize("@ss.hasPermi('view:goViewImages:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(goViewPrivateImagesService.selectGoViewPrivateImagesById(id));
    }

    /**
     * 新增goview用户私有图片
     */
    @ApiOperation("新增GoView私有图片")
    @Log(title = "goview用户私有图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoViewPrivateImages goViewPrivateImages) {
        return AjaxResult.success(goViewPrivateImagesService.insertGoViewPrivateImages(goViewPrivateImages));
    }

    /**
     * 修改goview用户私有图片
     */
    @ApiOperation("修改GoView私有图片")
    @Log(title = "goview用户私有图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoViewPrivateImages goViewPrivateImages) {
        return toAjax(goViewPrivateImagesService.updateGoViewPrivateImages(goViewPrivateImages));
    }

    /**
     * 删除goview用户私有图片
     */
    @ApiOperation("删除GoView私有图片")
    @Log(title = "goview用户私有图片", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(goViewPrivateImagesService.deleteGoViewPrivateImagesByIds(ids));
    }
}
