package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 设备用户对象 iot_device_user
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Data
public class DeviceUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 固件ID */
    private Long deviceId;

    /** 用户ID */
    private Long userId;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userName;

    /** 是否为设备所有者 */
    @Excel(name = "是否为设备所有者")
    private Integer isOwner;

    /** 手机号码 */
    private String phonenumber;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;
}
