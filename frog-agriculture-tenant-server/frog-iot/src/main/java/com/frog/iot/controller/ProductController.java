package com.frog.iot.controller;

import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.page.TableDataInfo;
import com.frog.common.enums.BusinessType;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.iot.domain.Product;
import com.frog.iot.model.ChangeProductStatusModel;
import com.frog.iot.model.IdAndName;
import com.frog.iot.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 产品Controller
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Api(tags = "iot-产品管理")
@RestController
@RequestMapping("/iot/product")
public class ProductController extends BaseController
{
    @Autowired
    private IProductService productService;

    /**
     * 查询产品列表
     */
    @ApiOperation("查询产品列表")
    @GetMapping("/list")
    public TableDataInfo list(Product product)
    {
        startPage();
        return getDataTable(productService.selectProductList(product));
    }

    /**
     * 查询产品简短列表
     */
    @ApiOperation("查询产品简短列表")
    @PreAuthorize("@ss.hasPermi('iot:product:list')")
    @GetMapping("/shortList")
    public AjaxResult shortList(Product product)
    {

        List<IdAndName>  list = productService.selectProductShortList();
        return AjaxResult.success(list);
    }

    /**
     * 导出产品列表
     */
    @ApiOperation("导出产品列表")
    @PreAuthorize("@ss.hasPermi('iot:product:export')")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Product product)
    {
        List<Product> list = productService.selectProductList(product);
        ExcelUtil<Product> util = new ExcelUtil<Product>(Product.class);
        util.exportExcel(response, list, "产品数据");
    }

    /**
     * 获取产品详细信息
     */
    @ApiOperation("获取产品详细信息")
    @PreAuthorize("@ss.hasPermi('iot:product:query')")
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId)
    {
        return AjaxResult.success(productService.selectProductByProductId(productId));
    }

    /**
     * 新增产品
     */
    @ApiOperation("新增产品")
    @PreAuthorize("@ss.hasPermi('iot:product:add')")
    @Log(title = "添加产品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Product product)
    {
        return AjaxResult.success(productService.insertProduct(product));
    }

    /**
     * 修改产品
     */
    @ApiOperation("修改产品")
    @PreAuthorize("@ss.hasPermi('iot:product:edit')")
    @Log(title = "修改产品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Product product)
    {
        return toAjax(productService.updateProduct(product));
    }

    /**
     * 获取产品下面的设备数量
     */
    @ApiOperation("获取产品下面的设备数量")
    @PreAuthorize("@ss.hasPermi('iot:product:query')")
    @GetMapping("/deviceCount/{productId}")
    public AjaxResult deviceCount(@PathVariable Long productId)
    {
        return AjaxResult.success(productService.selectDeviceCountByProductId(productId));
    }

    /**
     * 更新产品状态
     */
    @ApiOperation("更新产品状态")
    @PreAuthorize("@ss.hasPermi('iot:product:ispublish')")
    @Log(title = "更新产品状态", businessType = BusinessType.UPDATE)
    @PutMapping("/status")
    public AjaxResult changeProductStatus(@RequestBody ChangeProductStatusModel model)
    {
        return productService.changeProductStatus(model);
    }

    /**
     * 删除产品
     */
    @ApiOperation("删除产品")
    @PreAuthorize("@ss.hasPermi('iot:product:remove')")
    @Log(title = "产品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{productIds}")
    public AjaxResult remove(@PathVariable Long[] productIds)
    {
        return productService.deleteProductByProductIds(productIds);
    }
    /**
     * 生成AES产品密钥
     */
    @ApiOperation("生成AES产品密钥")
    @GetMapping(value="generateAesKey/{productId}/{authorizeId}")
    public AjaxResult generateAesKey(@PathVariable("productId") Long productId,@PathVariable("authorizeId") Long authorizeId){
        return AjaxResult.success("操作成功",productService.generateAesKey(productId,authorizeId));
    }

}
