package com.frog.iot.service;

import java.util.List;
import java.util.Map;

import com.frog.iot.domain.ThingsModel;
import com.frog.iot.model.ImportThingsModelInput;
import com.frog.iot.model.ThingsModels.ThingsModelSimpleItem;

/**
 * 物模型Service接口
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
public interface IThingsModelService 
{
    /**
     * 查询物模型
     * 
     * @param modelId 物模型主键
     * @return 物模型
     */
    public ThingsModel selectThingsModelByModelId(Long modelId);

    /**
     * 查询物模型列表
     * 
     * @param thingsModel 物模型
     * @return 物模型集合
     */
    public List<ThingsModel> selectThingsModelList(ThingsModel thingsModel);

    /**
     * 新增物模型
     * 
     * @param thingsModel 物模型
     * @return 结果
     */
    public int insertThingsModel(ThingsModel thingsModel);

    /**
     * 从模板导入物模型
     * @param input
     * @return
     */
    public int importByTemplateIds(ImportThingsModelInput input);

    /**
     * 修改物模型
     * 
     * @param thingsModel 物模型
     * @return 结果
     */
    public int updateThingsModel(ThingsModel thingsModel);

    /**
     * 批量删除物模型
     * 
     * @param modelIds 需要删除的物模型主键集合
     * @return 结果
     */
    public int deleteThingsModelByModelIds(Long[] modelIds);

    /**
     * 删除物模型信息
     * 
     * @param modelId 物模型主键
     * @return 结果
     */
    public int deleteThingsModelByModelId(Long modelId);

    /**
     * 根据产品ID获取JSON物模型
     * @param productId
     * @return
     */
    public String getCacheThingsModelByProductId(Long productId);

    /**
     * 根据产品缓存的物模型，将对象，简单数组和对象数组都拆开成单个物模型
     * @param productId
     * @return
     */
    public String getCacheThingsModelToSingleThingsModelByProductId(Long productId);
    /**
     * 批量查询产品的缓存物模型
     * @param productIds
     * @return
     */
    public Map<String, String> getBatchCacheThingsModelByProductIds(Long[] productIds);
    /**
     * 根据产品id查询产品物模型
     * @param productId
     * @return
     */
    public List<com.frog.iot.model.ThingsModelItem.ThingsModel> getCacheThingsModelsToListByProductId(Long productId);


    /**
     * 根据产品id和物模型id查询当前的物模型
     * @param productId
     * @param id
     * @return ThingsModel
     */
    public com.frog.iot.model.ThingsModelItem.ThingsModel getThingsModelByProductIdAndId(Long productId, String id);

    /**
     * 根据步长和上报的值进行转换
     * @param productId
     * @param thingsModelSimpleItem
     * @return
     */
    public String transByStep(Long productId, ThingsModelSimpleItem thingsModelSimpleItem);
}
