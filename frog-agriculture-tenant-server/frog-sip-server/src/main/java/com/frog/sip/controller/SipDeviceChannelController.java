package com.frog.sip.controller;

import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.page.TableDataInfo;
import com.frog.common.enums.BusinessType;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.service.ISipDeviceChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 监控设备通道信息Controller
 *
 * @author zhuangpeng.li
 * @date 2022-10-07
 */
@RestController
@RequestMapping("/sip/channel")
@Api(tags = "sip-频道管理")
public class SipDeviceChannelController extends BaseController
{
    @Autowired
    private ISipDeviceChannelService sipDeviceChannelService;

    /**
     * 查询监控设备通道信息列表
     */
    @ApiOperation("查询监控设备通道信息列表")
    @PreAuthorize("@ss.hasPermi('sip:channel:list')")
    @GetMapping("/list")
    public TableDataInfo list(SipDeviceChannel sipDeviceChannel)
    {
        startPage();
        List<SipDeviceChannel> list = sipDeviceChannelService.selectSipDeviceChannelList(sipDeviceChannel);
        return getDataTable(list);
    }

    /**
     * 导出监控设备通道信息列表
     */
    @ApiOperation("导出监控设备通道信息列表")
    @PreAuthorize("@ss.hasPermi('sip:channel:export')")
    @Log(title = "监控设备通道信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SipDeviceChannel sipDeviceChannel)
    {
        List<SipDeviceChannel> list = sipDeviceChannelService.selectSipDeviceChannelList(sipDeviceChannel);
        ExcelUtil<SipDeviceChannel> util = new ExcelUtil<SipDeviceChannel>(SipDeviceChannel.class);
        util.exportExcel(response, list, "监控设备通道信息数据");
    }

    /**
     * 获取监控设备通道信息详细信息
     */
    @ApiOperation("获取监控设备通道信息详细信息")
    @PreAuthorize("@ss.hasPermi('sip:channel:query')")
    @GetMapping(value = "/{channelId}")
    public AjaxResult getInfo(@PathVariable("channelId") Long channelId)
    {
        return AjaxResult.success(sipDeviceChannelService.selectSipDeviceChannelByChannelId(channelId));
    }

    /**
     * 新增监控设备通道信息
     */
    @ApiOperation("新增监控设备通道信息")
    @PreAuthorize("@ss.hasPermi('sip:channel:add')")
    @Log(title = "监控设备通道信息", businessType = BusinessType.INSERT)
    @PostMapping(value = "/{createNum}")
    public AjaxResult add(@PathVariable("createNum") Long createNum, @RequestBody SipDeviceChannel sipDeviceChannel) {
        return AjaxResult.success("操作成功", sipDeviceChannelService.insertSipDeviceChannel(createNum, sipDeviceChannel));
    }

    /**
     * 向设备新增监控设备通道信息
     */
    @ApiOperation("向设备新增监控设备通道信息")
    @PreAuthorize("@ss.hasPermi('sip:channel:add')")
    @Log(title = "监控设备通道信息", businessType = BusinessType.INSERT)
    @PostMapping(value = "addToDevice/{createNum}")
    public AjaxResult addToDevice(@PathVariable("createNum") Long createNum, @RequestBody SipDeviceChannel sipDeviceChannel) {
        return AjaxResult.success("操作成功", sipDeviceChannelService.insertSipDeviceChannelToDevice(createNum, sipDeviceChannel));
    }

    /**
     * 修改监控设备通道信息
     */
    @ApiOperation("修改监控设备通道信息")
    @PreAuthorize("@ss.hasPermi('sip:channel:edit')")
    @Log(title = "监控设备通道信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SipDeviceChannel sipDeviceChannel)
    {
        return toAjax(sipDeviceChannelService.updateSipDeviceChannel(sipDeviceChannel));
    }

    /**
     * 删除监控设备通道信息
     */
    @ApiOperation("删除监控设备通道信息")
    @PreAuthorize("@ss.hasPermi('sip:channel:remove')")
    @Log(title = "监控设备通道信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{channelIds}")
    public AjaxResult remove(@PathVariable Long[] channelIds)
    {
        return toAjax(sipDeviceChannelService.deleteSipDeviceChannelByChannelIds(channelIds));
    }
}
