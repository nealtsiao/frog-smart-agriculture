package com.frog.sip.server.model;

import gov.nist.javax.sip.message.SIPResponse;
import lombok.Data;

import javax.sip.PeerUnavailableException;
import javax.sip.header.*;
import javax.sip.message.Response;

import java.text.ParseException;

import static com.frog.sip.server.util.GetTextFromHeaderUtil.*;

/**
 * Dialog对象
 * @author neal
 * @date 2024/12/13  16:31
 */
@Data
public class DialogInfo {
    /**
     * 唯一确认一个Dialog的就是fromHeader、toHeadher、callId
     */
    //from的信息
    private String fromUser;
    private String fromHost;
    private String fromTag;
    //to的信息
    private String toUser;
    private String toHost;
    private String toTag;
    //via头
    private String viaHost;
    private Integer viaPort;
    private String viaReceied;
    //callId头
    private String callIdText;

    private String streamId;
    private String ssrc;
    //给个空构造函数反序列化使用
    public DialogInfo(){};
    public DialogInfo(Response res,String streamId,String ssrc) {
        SIPResponse response = (SIPResponse) res;
        this.fromUser = getUserFromFromHeaderOrToHeader(response.getFromHeader());
        this.fromHost = getHostFromFromHeaderOrToHeader(response.getFromHeader());
        this.fromTag = getTagFromFromHeaderOrToHeader(response.getFromHeader());
        this.toUser = getUserFromFromHeaderOrToHeader(response.getToHeader());
        this.toHost = getHostFromFromHeaderOrToHeader(response.getToHeader());
        this.toTag = getTagFromFromHeaderOrToHeader(response.getToHeader());
        this.viaHost = getHostFromViaHeader(response.getViaHeaders().get(0));
        this.viaPort = getPortFromViaHeader(response.getViaHeaders().get(0));
        this.viaReceied = getReceivedFromViaHeader(response.getViaHeaders().get(0));
        this.callIdText = getCallIdTextFromCallIdHeader(response.getCallIdHeader());
        this.streamId = streamId;
        this.ssrc = ssrc;

    }
}
