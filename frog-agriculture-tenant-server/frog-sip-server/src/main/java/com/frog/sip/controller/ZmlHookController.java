package com.frog.sip.controller;

import com.alibaba.fastjson.JSONObject;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.service.IZmlHookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/zlmhook")
@Api(tags = "sip-zlmediakit事件HOOK")
public class ZmlHookController
{

    @Autowired
    private IZmlHookService zmlHookService;
    @Autowired
    private ISipCmd sipCmd;

    /**
     * on_http_access
     * @param json
     * @return
     */
    @ApiOperation("on_http_access")
    @ResponseBody
    @PostMapping(value = "/on_http_access", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onHttpAccess(@RequestBody JSONObject json){
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("err", "");
        ret.put("path", "");
        ret.put("second", 600);
        return new ResponseEntity<String>(ret.toString(), HttpStatus.OK);
    }

    /**
     * on_play
     * @param json
     * @return
     */
    @ApiOperation("on_play")
    @ResponseBody
    @PostMapping(value = "/on_play", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onPlay(@RequestBody JSONObject json){
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("msg", "success");
        return new ResponseEntity<String>(ret.toString(), HttpStatus.OK);
    }

    /**
     * on_publish
     * @param json
     * @return
     */
    @ApiOperation("on_publish")
    @ResponseBody
    @PostMapping(value = "/on_publish", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onPublish(@RequestBody JSONObject json){
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("msg", "success");
        ret.put("enableHls", true);
        ret.put("enableMP4", false);
        ret.put("enableRtxp", true);
        return new ResponseEntity<String>(ret.toString(),HttpStatus.OK);
    }

    /**
     * on_stream_none_reader
     * @param json
     * @return
     */
    @ApiOperation("on_stream_none_reader")
    @ResponseBody
    @PostMapping(value = "/on_stream_none_reader", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onStreamNoneReader(@RequestBody JSONObject json){
        log.info("[无人观看事件on_stream_none_reader触发]：");
        zmlHookService.onStreamNoneReader(json);
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("close", true);
        return new ResponseEntity<String>(ret.toString(),HttpStatus.OK);
    }

    /**
     * on_stream_not_found
     * @param json
     * @return
     */
    @ApiOperation("on_stream_not_found")
    @ResponseBody
    @PostMapping(value = "/on_stream_not_found", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onStreamNotFound(@RequestBody JSONObject json){
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("msg", "success");
        return new ResponseEntity<String>(ret.toString(),HttpStatus.OK);
    }

    /**
     * on_stream_changed
     * @param json
     * @return
     */
    @ApiOperation("on_stream_changed")
    @ResponseBody
    @PostMapping(value = "/on_stream_changed", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> onStreamChanged(@RequestBody JSONObject json){
        JSONObject ret = new JSONObject();
        ret.put("code", 0);
        ret.put("msg", "success");
        return new ResponseEntity<String>(ret.toString(),HttpStatus.OK);
    }
}
