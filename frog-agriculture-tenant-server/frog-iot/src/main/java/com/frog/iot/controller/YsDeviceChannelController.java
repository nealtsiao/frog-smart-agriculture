package com.frog.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.enums.BusinessType;
import com.frog.iot.domain.YsDeviceChannel;
import com.frog.iot.service.IYsDeviceChannelService;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.common.core.page.TableDataInfo;

/**
 * 萤石云设备通道Controller
 * 
 * @author nealtsiao
 * @date 2024-11-25
 */
@RestController
@RequestMapping("/iot/channel")
@Api(tags = "iot-萤石云通道管理")
public class YsDeviceChannelController extends BaseController
{
    @Autowired
    private IYsDeviceChannelService ysDeviceChannelService;

    /**
     * 查询萤石云设备通道列表
     */
    @ApiOperation("查询萤石云设备通道列表")
    @GetMapping("/list")
    public TableDataInfo list(YsDeviceChannel ysDeviceChannel)
    {
        startPage();
        List<YsDeviceChannel> list = ysDeviceChannelService.selectYsDeviceChannelList(ysDeviceChannel);
        return getDataTable(list);
    }
}
