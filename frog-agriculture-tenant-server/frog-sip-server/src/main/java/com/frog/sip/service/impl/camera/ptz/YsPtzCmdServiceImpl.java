package com.frog.sip.service.impl.camera.ptz;

import com.frog.iot.util.YsApiUtils;
import com.frog.sip.server.model.PtzDirectionInput;
import com.frog.sip.server.model.PtzZoomInput;
import com.frog.sip.service.IPtzCmdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author neal
 * @date 2024/12/2  14:50
 */
@Service("ysPtz")
public class YsPtzCmdServiceImpl implements IPtzCmdService {
    @Autowired
    private YsApiUtils ysApiUtils;
    @Override
    public boolean direction(String deviceId, String channelId, PtzDirectionInput ptzDirectionInput) {
        Integer channelNo = Integer.parseInt(channelId.split("-")[1]);
        if(ptzDirectionInput.getUpDown()==0 && ptzDirectionInput.getLeftRight()==0){
            ysApiUtils.PtzStop(deviceId,channelNo,0);
            ysApiUtils.PtzStop(deviceId,channelNo,1);
            ysApiUtils.PtzStop(deviceId,channelNo,2);
            ysApiUtils.PtzStop(deviceId,channelNo,3);
        }else{
            ysApiUtils.ptzStart(deviceId,channelNo,tansDirectionYsToSip(ptzDirectionInput),1);
        }
        return false;
    }

    @Override
    public boolean zoom(String deviceId, String channelId, PtzZoomInput ptzZoomInput) {
        Integer channelNo = Integer.parseInt(channelId.split("-")[1]);
        if(ptzZoomInput.getInOut()==0){
            ysApiUtils.PtzStop(deviceId,channelNo,8);
            ysApiUtils.PtzStop(deviceId,channelNo,9);
        }else{
            ysApiUtils.ptzStart(deviceId,channelNo,tansZoomYsToSip(ptzZoomInput),1);
        }
        return false;
    }

    private int tansDirectionYsToSip(PtzDirectionInput ptzDirectionInput){
        //right
        if(ptzDirectionInput.getLeftRight()==1){
            return 3;
        }
        //left
        else if(ptzDirectionInput.getLeftRight()==2){
            return 2;
        }
        //up
        else if(ptzDirectionInput.getUpDown()==1){
            return 0;
        }
        //down
        else if(ptzDirectionInput.getUpDown()==2){
            return 1;
        }else{
            return -1;
        }
    }
    private int tansZoomYsToSip(PtzZoomInput ptzZoomInput){
        //in
        if(ptzZoomInput.getInOut()==1){
            return 8;
        }
        //out
        else if(ptzZoomInput.getInOut()==2){
            return 9;
        } else{
            return -1;
        }
    }

}
