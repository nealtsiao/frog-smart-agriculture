package com.frog.iot.controller;

import com.alibaba.fastjson.JSONObject;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.page.TableDataInfo;
import com.frog.common.enums.BusinessType;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.iot.domain.Device;
import com.frog.iot.model.DeviceRelateUserInput;
import com.frog.iot.model.DeviceShortOutput;
import com.frog.iot.service.IDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 设备Controller
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Api(tags = "iot-设备管理")
@RestController
@RequestMapping("/iot/device")
public class DeviceController extends BaseController
{
    @Autowired
    private IDeviceService deviceService;

    /**
     * 查询设备列表
     */
    @PreAuthorize("@ss.hasPermi('iot:device:list')")
    @GetMapping("/list")
    @ApiOperation("查询设备列表")
    public TableDataInfo list(Device device)
    {
        long total = deviceService.selectDeviceList(device).size();
        startPage();
        List<DeviceShortOutput> list = deviceService.selectDeviceList(device);
        TableDataInfo dataTable = getDataTable(list);
        dataTable.setTotal(total);
        return dataTable ;
    }

    /**
     * 导出设备列表
     */
    @PreAuthorize("@ss.hasPermi('iot:device:export')")
    @Log(title = "设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出设备列表")
    public void export(HttpServletResponse response, Device device)
    {
        List<DeviceShortOutput> list = deviceService.selectDeviceList(device);
        ExcelUtil<DeviceShortOutput> util = new ExcelUtil<DeviceShortOutput>(DeviceShortOutput.class);
        util.exportExcel(response, list, "设备数据");
    }

    /**
     * 获取设备详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:device:query')")
    @GetMapping(value = "/{deviceId}")
    @ApiOperation("获取设备详细信息")
    public AjaxResult getInfo(@PathVariable("deviceId") Long deviceId)
    {
        return AjaxResult.success(deviceService.selectDeviceByDeviceId(deviceId));
    }

    /**
     * 根据设备编号详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:device:query')")
    @GetMapping(value = "/getDeviceBySerialNumber/{serialNumber}")
    @ApiOperation("根据设备编号详细信息")
    public AjaxResult getInfoBySerialNumber(@PathVariable("serialNumber") String serialNumber)
    {
        return AjaxResult.success(deviceService.selectDeviceBySerialNumber(serialNumber));
    }

    /**
     * 获取设备详情和运行状态
     */
    @PreAuthorize("@ss.hasPermi('iot:device:query')")
    @GetMapping(value = "/runningStatus/{deviceId}")
    @ApiOperation("获取设备详情和运行状态，嵌套物模型")
    public AjaxResult getRunningStatusInfo(@PathVariable("deviceId") Long deviceId)
    {
        return AjaxResult.success(deviceService.selectDeviceRunningStatusByDeviceId(deviceId));
    }
    /**
       * 获取设备详情和运行状态
     */
    @PreAuthorize("@ss.hasPermi('iot:device:query')")
    @GetMapping(value = "/runningStatusSingle/{deviceId}/{removeNotRecord}")
    @ApiOperation("获取设备详情和运行状态，单层物模型")
    public AjaxResult getRunningStatusInfoSingle(@PathVariable("deviceId") Long deviceId,@PathVariable("removeNotRecord") Long removeNotRecord)
    {
        return AjaxResult.success(deviceService.selectDeviceRunningStatusByDeviceIdFilterSingle(deviceId,removeNotRecord));
    }

    /**
     * 新增设备
     */
    @PreAuthorize("@ss.hasPermi('iot:device:add')")
    @Log(title = "添加设备", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation("新增设备")
    public AjaxResult add(@RequestBody Device device)
    {
        return AjaxResult.success(deviceService.insertDevice(device));
    }

    /**
     * 设备关联用户
     */
    @PreAuthorize("@ss.hasPermi('iot:device:add')")
    @Log(title = "设备关联用户", businessType = BusinessType.UPDATE)
    @PostMapping("/relateUser")
    @ApiOperation("设备关联用户")
    public AjaxResult relateUser(@RequestBody DeviceRelateUserInput deviceRelateUserInput)
    {
        if(deviceRelateUserInput.getUserId()==0 || deviceRelateUserInput.getUserId()==null){
            return AjaxResult.error("用户ID不能为空");
        }
        if(deviceRelateUserInput.getDeviceNumberAndProductIds()==null || deviceRelateUserInput.getDeviceNumberAndProductIds().size()==0){
            return AjaxResult.error("设备编号和产品ID不能为空");
        }
        return deviceService.deviceRelateUser(deviceRelateUserInput);
    }

    /**
     * 修改设备
     */
    @PreAuthorize("@ss.hasPermi('iot:device:edit')")
    @Log(title = "修改设备", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation("修改设备")
    public AjaxResult edit(@RequestBody Device device)
    {
        return deviceService.updateDevice(device);
    }

    /**
     * 重置设备状态
     */
    @PreAuthorize("@ss.hasPermi('iot:device:edit')")
    @Log(title = "重置设备状态", businessType = BusinessType.UPDATE)
    @PutMapping("/reset/{serialNumber}")
    @ApiOperation("重置设备状态")
    public AjaxResult resetDeviceStatus(@PathVariable String serialNumber)
    {
        Device device=new Device();
        device.setSerialNumber(serialNumber);
        return toAjax(deviceService.resetDeviceStatus(device.getSerialNumber()));
    }

    /**
     * 删除设备
     */
    @PreAuthorize("@ss.hasPermi('iot:device:remove')")
    @Log(title = "删除设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{deviceIds}")
    @ApiOperation("批量删除设备")
    public AjaxResult remove(@PathVariable Long[] deviceIds) throws SchedulerException {
        return toAjax(deviceService.deleteDeviceByDeviceId(deviceIds[0]));
    }

    /**
     * 生成设备编号
     */
    @PreAuthorize("@ss.hasPermi('iot:device:edit')")
    @GetMapping("/generator")
    @ApiOperation("生成设备编号")
    public AjaxResult generatorDeviceNum(){
        return AjaxResult.success("操作成功",deviceService.generationDeviceNum());
    }

    /**
     * 导入设备
     */
    @PreAuthorize("@ss.hasPermi('iot:device:import')")
    @Log(title = "导入设备", businessType = BusinessType.INSERT)
    @PostMapping("/import")
    @ApiOperation("导入设备")
    public AjaxResult importDevice(@RequestBody JSONObject jsonObject)
    {
        try {
            long productId= jsonObject.getLong("productId");
            List<String> serialNums = jsonObject.getJSONArray("serialNums").toJavaList(String.class);
            int i = deviceService.importDevice(productId, serialNums);
            return AjaxResult.success(i);
        } catch (Exception e) {
            return AjaxResult.error("参数有误");
        }
    }
}
