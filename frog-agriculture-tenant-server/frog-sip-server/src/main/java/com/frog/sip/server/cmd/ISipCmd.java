package com.frog.sip.server.cmd;

import com.frog.sip.domain.SipDevice;

public interface ISipCmd {
    void playStreamCmd(SipDevice device, String channelId,String streamId);
    void playbackStreamCmd(SipDevice device, String channelId, String streamId, String startTime, String endTime);
    void streamByeCmd(String ssrc);
    boolean deviceInfoQuery(SipDevice device);
    boolean catalogQuery(SipDevice device);
    boolean recordInfoQuery(SipDevice sipDevice, String channelId, String sn, String startTime, String endTime);

    boolean ptzCmd(SipDevice device, String channelId, int leftRight, int upDown, int inOut, int moveSpeed, int scaleSpeed);
    <T> T getRecord(String key, long timeout);
}
