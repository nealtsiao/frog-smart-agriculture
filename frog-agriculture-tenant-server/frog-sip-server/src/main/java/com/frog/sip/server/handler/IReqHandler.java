package com.frog.sip.server.handler;

import javax.sip.RequestEvent;

public interface IReqHandler {
    public void processMsg(RequestEvent evt);
}
