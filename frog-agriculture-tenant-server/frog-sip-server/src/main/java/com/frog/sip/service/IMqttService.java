package com.frog.sip.service;

import com.frog.iot.model.ThingsModels.ThingsModelSimpleItem;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.model.RecordList;

import java.util.List;

public interface IMqttService {
    void publishSipDeviceInfoToInfoPost(SipDevice device);
    void publishProperty(Long productId, String deviceNum, List<ThingsModelSimpleItem> thingsList, int delay);
    void publishChannelsProperty(String DeviceSipId, List<SipDeviceChannel> channels);
    void publishRecordsProperty(String DeviceSipId, RecordList recordList);
}
