package com.frog.sip.server.util;

import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.cmd.impl.SipCmdImpl;
import com.frog.sip.service.ISipDeviceChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.RequestEvent;
import javax.sip.ResponseEvent;
import javax.sip.header.FromHeader;
import javax.sip.header.ToHeader;

import static com.frog.sip.server.util.GetTextFromHeaderUtil.getTagFromFromHeaderOrToHeader;
import static com.frog.sip.server.util.GetTextFromHeaderUtil.getUserFromFromHeaderOrToHeader;

/**
 * @author neal
 * @date 2024/12/18  19:42
 */
@Component
@Slf4j
public class StreamIdUtil {
    @Autowired
    ISipDeviceChannelService sipDeviceChannelService;
    @Autowired
    private StreamIdUtil streamIdUtil;
    public static final String PLAY_TAG = SipCmdImpl.PLAY_TAG;
    public static final String PLAY_BACK_TAG=SipCmdImpl.PLAY_BACK_TAG;

    /**
     * 创建播放的streamId
     * @param deviceSipId
     * @param channelId
     * @return
     */
    public String getPlayStreamId(String deviceSipId,String channelId){
        return String.format(PLAY_TAG+"_%s_%s", deviceSipId, channelId);
    }

    /**
     * 创建播放录像的streamId
     * @param deviceSipId
     * @param channelId
     * @return
     */
    public String getPlayBackStreamId(String deviceSipId,String channelId){
        return String.format(PLAY_BACK_TAG+"_%s_%s", deviceSipId, channelId);
    }

    /**
     * 从Response事件中获取streamId
     * @param evt
     * @return
     */
    public String getStreamIdFromRequestEvent(RequestEvent evt){
        String streamId="";
        String user = getUserFromFromHeaderOrToHeader(evt.getRequest().getHeader(ToHeader.NAME));
        SipDeviceChannel sipDeviceChannel = sipDeviceChannelService.selectSipDeviceChannelByChannelSipId(user);
        if(sipDeviceChannel==null){
            log.error("[系统中不存在此频道信息]");
            return streamId;
        }
        //获取from头中的tag
        String tag = getTagFromFromHeaderOrToHeader(evt.getRequest().getHeader(FromHeader.NAME));
        //获取缓存中的Dialog
        if (tag.equals(PLAY_TAG)){
            streamId = streamIdUtil.getPlayStreamId(sipDeviceChannel.getDeviceSipId(),sipDeviceChannel.getChannelSipId());
        }else if(tag.equals(PLAY_BACK_TAG)){
            streamId = streamIdUtil.getPlayBackStreamId(sipDeviceChannel.getDeviceSipId(),sipDeviceChannel.getChannelSipId());
        }
        return streamId;
    }

    /**
     * 从Request事件中获取streamId
     * @param evt
     * @return
     */
    public String getStreamIdFromResponseEvent(ResponseEvent evt){
        String streamId="";
        String user = getUserFromFromHeaderOrToHeader(evt.getResponse().getHeader(ToHeader.NAME));
        SipDeviceChannel sipDeviceChannel = sipDeviceChannelService.selectSipDeviceChannelByChannelSipId(user);
        if(sipDeviceChannel==null){
            log.error("[系统中不存在此频道信息]");
            return streamId;
        }
        //获取from头中的tag
        String tag = getTagFromFromHeaderOrToHeader(evt.getResponse().getHeader(FromHeader.NAME));
        //获取缓存中的Dialog
        if (tag.equals(PLAY_TAG)){
            streamId = streamIdUtil.getPlayStreamId(sipDeviceChannel.getDeviceSipId(),sipDeviceChannel.getChannelSipId());
        }else if(tag.equals(PLAY_BACK_TAG)){
            streamId = streamIdUtil.getPlayBackStreamId(sipDeviceChannel.getDeviceSipId(),sipDeviceChannel.getChannelSipId());
        }
        return streamId;
    }

}
