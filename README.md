![index](img/giteebanner.jpg)


## 项目简介

开源版本为大家提供一个学习的代码，不能进行商用，商用请联系我们购买授权。未授权进行商用我们会进行追责。长期维护一个产品是需要投入大量人力物力，授权不贵，请大家支持一下。

青蛙智慧农业平台-支持物联网设备；农业设备对接；支持GB28181接入摄像头（IPC、NVR）；支持mqtt、modbus、RS485协议；移动端包含：安卓、苹果、H5；支持农事管理；支持溯源（包含溯源H5）。


> 官网地址：https://zosoftware.yuque.com/org-wiki-zosoftware-ms7q4x/frog
>
> 体验地址:(因为最近访问量变多，需要看演示的可以联系作者)
>

## 联系我们

![](img/qrcode.png)

## 功能更新

### 2.1.0(2025-01-04)
1. 优化zlmedia配置界面（增加https播放端口配置）
2. 手机端增加ai
3. 集成萤石云
4. goview(拖拽大屏)
5. 优化sip模块代码

### 2.0.0（2024-10-26）

1.支持多租户

### 1.3.3（2024-07-30）

1. 物模型支持复杂对象和对象数组以及简单数组
2. 优化设备接入流程
3. 优化大屏数据上报功能
4. 增加种植单元管理
5. 监控sip信息上报和mqtt的整合
6. 简化设备订阅主题

### 1.3.2（2024-05-09）
1. 增加了摄像头设备意外断电设备状态发布
2. 重构溯源功能，增加溯源码可视化编辑、增加溯源页面可视化编辑、增加溯源码图片导出和excel导出，方便印制
3. 重构手机播放，支持APP、H5、微信小程序（微信小程序需要开通live-play权限）
4. 优化农事任务处理流程
5. 增加手机端增加病虫害识别功能（需自己购买识别接口）
6. 优化可视化大屏功能

### 1.2.1（2022-01-20）
  1. 版本发布

## 平台架构

![](img/flow.png)

## 功能清单

![](img/青蛙农业脑图.png)

## 技术栈

- 服务端
  
  相关技术：Spring boot、MyBatis、Spring Security、Jwt、Mysql、Redis、TDengine、EMQX、Netty等
  
  开发工具：IDEA
  
- Web端
  
  相关技术：ES6、Vue、Vuex、Vue-router、Vue-cli、Axios、Element-ui等
  
  开发工具：Visual Studio Code
  
- 移动端（微信小程序 / Android / Ios / H5）
  
  相关技术：uniapp、[uView](https://gitee.com/link?target=https%3A%2F%2Fwww.uviewui.com%2F)、[uChart](https://gitee.com/link?target=https%3A%2F%2Fwww.ucharts.cn%2F)
  
  开发工具：HBuilder

## 设备类型

物联网设备：支持mqtt、TCP设备

摄像头：支持各品牌摄像头（需支持国标GB28181协议）

## 功能介绍
### 工作台

![index](img/index.png)

### 大屏

![index](img/bg1.png)

![index](img/bg2.png)

![index](img/bg3.png)

![index](img/bg4.png)

![index](img/bg5.png)

### 手机端

![index](img/phone.jpg)
![index](img/phone2.jpg)



### 农场

农场包含：地图管理、种植单元、员工管理、农机类别管理、农机信息管理、农资类别管理、农资信息管理、种质系统、地块管理、任务管理、农业资讯。

下面介绍一下特色功能。

#### 种植单元

![index](img/unit.jpg)

#### 地图管理

地图管理可以集中管理设备和地块信息，支持地块和设备检索定位，支持设备新增、编辑，支持地块的修改绘制。

![index](img/mapManager.png)

#### 地块管理

地块管理功能，支持地块绘制，支持边框颜色和透明度，支持覆盖区域颜色和透明度。

![index](img/land.png)

#### 种质系统

种质系统支持维护种质信息，支持种植方法、种质介绍以及种质标准作业流程维护。

![index](img/zz.png)

![index](img/zzJob.png)

![index](img/zzIntro.png)

![index](img/zzMethod.png)

#### 批次管理

新增批次，可按照种质标准任务生成批次任务。

![index](img/batch.png)

![index](img/batchDetail.png)

#### 农事任务管理

支持按照批次查询任务。

![index](img/job.png)

任务支持甘特图预览模式。

![index](img/jobGan.png)

管理人员可以维护任务执行过程中的人员、工时、机械、农资、处理视频和图片等信息。

![index](img/jobDeal.png)

### 设备管理


#### 物模型

物模型功能将设备抽象为模型，主要分三种类型，设备属性、设备方法、设备事件。可实现设备属性上报、控制和事件触发。

![index](img/thingsModel.png)

#### 产品管理

对同一类型的产品进行管理，产品支持物模型配置，认证配置。同时支持物联设备和摄像设备（IPC、NVR）等接入。

![index](img/product.png)

#### 设备管理

可查看设备的在线等基本信息。

![index](img/device.png)

设备通用界面，支持拖拽布局，支持设备属性查看和设备控制，支持实时上报数据。

![index](img/deviceDrag.png)

支持设备定时任务设置。

![index](img/deviceJob.png)

支持设备分享，多用户管理。

![index](img/deviceUser.png)

支持查看设备日志，日志类型有属性上报和设备控制。

![index](img/deviceLog.png)

支持对上报属性进行统计分析，形成报表。

![index](img/deviceTj.png)

#### 其他设备管理界面

虫情灯

![index](img/deviceBug.png)

水肥机

![index](img/deviceSf.png)

#### 场景联动

可以设置触发条件出发动作。

![index](img/screen.png)

#### 监控

根据国标GB28181-2016接入监控设备，实现设备方向、缩放、推流、回播等控制。测试延时500毫秒以内。

单屏

![index](img/camera1.png)

分屏

![index](img/camera2.png)

### 产品溯源
#### 溯源报表
![index](img/traceRF.png)

#### 溯源异常查询
![index](img/traceAlert.png)

#### 溯源产品
支持新建溯源产品，关联溯源模版和溯源码模版
![index](img/tracePro.png)

#### 生成溯源码
![index](img/traceCodeSC.png)

#### 溯源码
可直接导出溯源码图片和溯源码excel
![index](img/traceCode.png)

#### 溯源码自定义
支持拖拽布局
![index](img/traceCodeTemplate.png)

#### 溯源页面自定义
支持系统导入和手动添加各节点数据
![index](img/traceTemplate.png)

## 案例
### 建湖高作稻虾田

本项目为龙虾和水稻种养结合项目，实施面积千余亩，利用物联网技术，有效的提高了龙虾养殖和水稻种植的产量。

![index](img/case1.png)

### 南昌红谷滩元宇宙·VR数字农业示范基地

通过本公司智能物联感知单元——传感器的数据采集、传输及分析，智能水肥一体化系统依据这些数据进行智能管控、精准运行。以实现精准数据的采集传输为关键基础，结合专业的种植指导，促进红谷滩元宇宙·VR数字农业示范基地6号植物工厂培育出高产高质、营养价值高的作物品种——多彩水果番茄。以数据促生产，以数据助研学。

![index](img/case2.jpg)

### 水稻试验基地

该项目是一块水稻种植试验基地里，所使用的农田设备有土壤墒站、智能节水灌溉、农田环境监测站，目前，农田灌溉是当初农田节水灌溉最重要的一个环节，智能节水灌溉设备可以进行无线远程控制，减少人力投入资本，并且可实时查看用水情况，也可以根据现场环境进行自动化控制，自动开闸，自动关闸等，真正做到智能控制。农田气象站数据监测站可以查看空气温湿度、二氧化碳浓度、雨量、风速风向等环境数据，并且配套有户外 LED 屏和 4GDTU，可以本地或者远程查看数据，能够随时随地的关注农田环境数据，可以针对环境数据做到早预防，早处理的作用。

![index](img/case3.jpg)

