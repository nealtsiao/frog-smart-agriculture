package com.frog.iot.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.common.annotation.TenantScope;
import com.frog.common.constant.Constants;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.domain.entity.SysDept;
import com.frog.common.core.domain.entity.SysRole;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.redis.RedisCache;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import com.frog.common.utils.StringUtils;
import com.frog.common.utils.http.HttpUtils;
import com.frog.common.utils.ip.IpUtils;
import com.frog.iot.domain.*;
import com.frog.iot.mapper.YsDeviceChannelMapper;
import com.frog.iot.model.ThingsModelItem.ThingsModel;
import com.frog.iot.mqtt.EmqxService;
import com.frog.iot.service.*;
import com.frog.iot.tdengine.service.ILogService;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.mapper.DeviceUserMapper;
import com.frog.iot.model.*;
import com.frog.iot.model.ThingsModelItem.*;
import com.frog.iot.model.ThingsModels.*;
import com.frog.iot.util.YsApiUtils;
import com.frog.system.domain.Base;
import com.frog.system.mapper.BaseMapper;
import com.frog.system.mapper.SysDeptMapper;
import com.frog.system.service.ISysUserService;
import org.apache.http.annotation.Obsolete;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.frog.common.utils.SecurityUtils.getLoginUser;

/**
 * 设备Service业务层处理
 *
 * @author kerwincui
 * @date 2021-12-16
 */
@Service
public class DeviceServiceImpl implements IDeviceService {
    private static final Logger log = LoggerFactory.getLogger(DeviceServiceImpl.class);

    // 物模型值命名空间：Key：TSLV:{productId}_{deviceNumber}   HKey:{identity#V/identity#S/identity#M/identity#N}
    private String devicePreKey = "TSLV:";

    // 物模型命名空间：Key:TSL:{productId}
    private String tslPreKey = "TSL:";

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    private DeviceUserMapper deviceUserMapper;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private ThingsModelServiceImpl thingsModelService;

    @Autowired
    private DeviceJobServiceImpl deviceJobService;

    @Autowired
    private IToolService toolService;

    @Autowired
    private IProductService productService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ILogService logService;

    @Autowired
    private IAlertLogService alertLogService;

    @Autowired
    @Lazy
    private EmqxService emqxService;

    @Autowired
    private RedisCache redisCache;
    
    @Autowired
    private BaseMapper baseMapper;

    @Autowired
    private IYsConfigService ysConfigService;

    @Autowired
    private YsDeviceChannelMapper ysDeviceChannelMapper;

    @Autowired
    private YsApiUtils ysApiUtils;
    /**
     * 查询设备
     *
     * @param deviceId 设备主键
     * @return 设备
     */
    @Override
    public Device selectDeviceByDeviceId(Long deviceId) {
        Device device = deviceMapper.selectDeviceByDeviceId(deviceId);
        //查询设备channel
        device.setChildren(deviceMapper.selectChannelBySerialNumber(device.getSerialNumber()));
        // redis中获取设备状态（物模型值）
        device.setThingsModelValue(JSONObject.toJSONString(getCacheDeviceStatus(device.getProductId(), device.getSerialNumber())));
        return device;
    }

    /**
     * 根据设备编号查询设备
     *
     * @param serialNumber 设备主键
     * @return 设备
     */
    @Override
    public Device selectDeviceBySerialNumber(String serialNumber) {
        Device device = deviceMapper.selectDeviceBySerialNumber(serialNumber);
        // redis中获取设备状态（物模型值）
        device.setThingsModelValue(JSONObject.toJSONString(getCacheDeviceStatus(device.getProductId(), device.getSerialNumber())));
        return device;
    }

    /**
     * 根据设备编号查询简洁设备
     *
     * @param serialNumber 设备主键
     * @return 设备
     */
    @Override
    public Device selectShortDeviceBySerialNumber(String serialNumber) {
        Device device = deviceMapper.selectShortDeviceBySerialNumber(serialNumber);
        // redis中获取设备状态（物模型值）
        device.setThingsModelValue(JSONObject.toJSONString(getCacheDeviceStatus(device.getProductId(), device.getSerialNumber())));
        return device;
    }

    /**
     * 根据设备编号查询设备认证信息
     *
     * @param model 设备编号和产品ID
     * @return 设备
     */
    @Override
    public ProductAuthenticateModel selectProductAuthenticate(AuthenticateInputModel model) {
        return deviceMapper.selectProductAuthenticate(model);
    }

    /**
     * 更新设备的物模型的值
     *
     * @param input    设备ID和物模型值
     * @param type     1=属性 2=功能 3=事件
     * @param isShadow 是否影子值
     * @return 设备
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int reportDeviceThingsModelValue(ThingsModelValuesInput input, int type, boolean isShadow) {
        String key = devicePreKey + input.getProductId() + "_" + input.getDeviceNumber();
        Map<String, String> maps = new HashMap<String, String>();
        //上报的数据数组
        List<ThingsModelSimpleItem> thingsModelSimpleItems = input.getThingsModelValueRemarkItem();
        for (int i = 0; i < thingsModelSimpleItems.size(); i++) {
            String id = thingsModelSimpleItems.get(i).getId();
            String value = thingsModelSimpleItems.get(i).getValue();
            String isMonitor = "";
            // 数组元素的值，通过前缀中的索引更新，数组元素格式：array_01_parentId_humidity
            if (id.startsWith("array_")) {
                int index = Integer.parseInt(id.substring(6, 8));
                String identity = id.substring(9);
                // 查询redis中是否存在对应物模型
                isMonitor = redisCache.getCacheMapValue(key, identity + "#M");
                // 跳过不存在的物模型ID
                if (isMonitor == null || "".equals(isMonitor)) {
                    continue;
                }
                // 设置值，获取数组值，然后替换其中元素
                if (!isShadow) {
                    String[] values = redisCache.getCacheMapValue(key, identity + "#V").toString().split(",");
                    values[index] = value;
                    maps.put(identity + "#V", String.join(",", values));
                }
                // 设置影子值
                String[] shadows = redisCache.getCacheMapValue(key, identity + "#S").toString().split(",");
                shadows[index] = value;
                maps.put(identity + "#S", String.join(",", shadows));
            } else {
                // 查询redis中是否存在对应物模型
                isMonitor = redisCache.getCacheMapValue(key, thingsModelSimpleItems.get(i).getId() + "#M");
                // 跳过不存在的物模型ID
                if (isMonitor == null || "".equals(isMonitor)) {
                    continue;
                }
                // 设置值
                if (!isShadow) {
                    maps.put(thingsModelSimpleItems.get(i).getId() + "#V", value);
                }
                // 设置影子值
                maps.put(thingsModelSimpleItems.get(i).getId() + "#S", value);
            }

            // 添加到设备日志,isRecord==0则不存储日志到数据库
            ThingsModel thingsModel=thingsModelService.getThingsModelByProductIdAndId(input.getProductId(),id);
            if(thingsModel!=null && thingsModel.getIsRecord()==0){
                continue;
            }
            Device device = selectDeviceBySerialNumber(input.getDeviceNumber());
            DeviceLog deviceLog = new DeviceLog();
            if(device !=null){
                deviceLog.setDeviceId(device.getDeviceId());
                deviceLog.setTenantId(device.getTenantId());
                deviceLog.setBaseId(device.getBaseId());
                deviceLog.setDeptId(device.getDeptId());
                deviceLog.setUserId(device.getUserId());
            }
            deviceLog.setSerialNumber(input.getDeviceNumber());
            deviceLog.setLogType(type);
            // 1=影子模式，2=在线模式，3=其他
            deviceLog.setMode(isShadow ? 1 : 2);
            // 设备日志值
            //根据步长转换，不考虑对象和数组
            deviceLog.setLogValue(thingsModelService.transByStep(input.getProductId(), thingsModelSimpleItems.get(i)));
            deviceLog.setRemark(input.getThingsModelValueRemarkItem().get(i).getRemark());
            deviceLog.setIdentity(input.getThingsModelValueRemarkItem().get(i).getId());
            deviceLog.setIsMonitor(Integer.parseInt(isMonitor));
            deviceLog.setCreateTime(DateUtils.getNowDate());
            logService.saveDeviceLog(deviceLog);
        }
        redisCache.hashPutAll(key, maps);
        return 1;
    }

    /**
     * 查询设备列表
     *
     * @param device 设备
     * @return 设备
     */
    @Override
    @TenantScope(tenantAlias = "device",baseAlias = "device",deptAlias ="device",userAlias = "device")
    public List<DeviceShortOutput> selectDeviceList(Device device) {

        return deviceMapper.selectDeviceList(device);
    }

    /**
     * 查询设备
     *
     * @param deviceId 设备主键
     * @return 设备
     */
    @Override
    public DeviceShortOutput selectDeviceRunningStatusByDeviceId(Long deviceId) {
        //查询设备信息
        DeviceShortOutput device = deviceMapper.selectDeviceRunningStatusByDeviceId(deviceId);
        //从缓存中取产品物模型字符串再转成JSONObject
        JSONObject thingsModelObject = JSONObject.parseObject(thingsModelService.getCacheThingsModelByProductId(device.getProductId()));
        JSONArray properties = thingsModelObject.getJSONArray("properties");
        JSONArray functions = thingsModelObject.getJSONArray("functions");
        List<ThingsModelValueItem> thingsModelValueItems = getCacheDeviceStatus(device.getProductId(), device.getSerialNumber());
        // 物模型转换赋值
        List<ThingsModel> thingsPropertyList = convertJsonObjectToThingsModels(properties, thingsModelValueItems, 1);
        List<ThingsModel> thingsFunctionList = convertJsonObjectToThingsModels(functions, thingsModelValueItems, 2);
        thingsPropertyList.addAll(thingsFunctionList);
        // 排序
        thingsPropertyList = thingsPropertyList.stream().sorted(Comparator.comparing(ThingsModel::getOrder).reversed()).collect(Collectors.toList());
        device.setThingsModels(thingsPropertyList);
        device.setThingsModelValue("");
        return device;
    }

    /**
     * 将redis中的值组合到物模型中
     * @param jsonArray redis中的物模型
     * @param thingsModelValues redis中的设备的值
     * @param type 1 属性 2功能 3 事件
     * @return
     */
    private List<ThingsModel> convertJsonObjectToThingsModels(JSONArray jsonArray, List<ThingsModelValueItem> thingsModelValues,Integer type) {
        List<ThingsModel> thingsModels = jsonArray.toJavaList(ThingsModel.class).stream().filter(thingsModel -> thingsModel.getIsTop()==1).collect(Collectors.toList());
        Map<String, ThingsModel> idToModelMap = thingsModels.stream()
                .map(thingsModel -> {
                    thingsModel.setType(type);
                    return thingsModel;
                })
                .collect(Collectors.toMap(ThingsModel::getId, Function.identity()));

        for (ThingsModelValueItem valueItem : thingsModelValues) {
            ThingsModel model = idToModelMap.get(valueItem.getId().split("_")[0]);
            if (model != null ) {
                updateThingsModel(model, valueItem,type);
            }
        }

        return thingsModels;
    }

    /**
     * 更新物模型
     * @param model
     * @param valueItem
     * @param type type 1 属性 2功能
     */
    private void updateThingsModel(ThingsModel model, ThingsModelValueItem valueItem,Integer type) {
        DataType dataType = model.getDataType();
        String id = valueItem.getId();
        String value = valueItem.getValue();
        String shadow = valueItem.getShadow();
        //对象数组
        if ("array".equals(dataType.getType()) && "object".equals(dataType.getArrayType())) {
            updateArrayParams(model, dataType,value, shadow,type);
        }
        //简单数组
        if ("array".equals(dataType.getType()) && !"object".equals(dataType.getArrayType())) {
            updateSimpleArrayParams(model, dataType,value, shadow,type);
        }
        //对象
        else if ("object".equals(dataType.getType())) {
            updateObjectParams(model, dataType, id, value, shadow,type);
        }
        //其他：int decimal enum string simpleArray
        else {
            model.setValue(value);
            model.setShadow(shadow);
        }
    }

    /**
     * 更新对象数组值
     * @param model
     * @param dataType
     * @param value
     * @param shadow
     * @param type
     */
    private void updateArrayParams(ThingsModel model, DataType dataType,String value, String shadow,Integer type) {
        String[] values = value.split(",");
        String[] shadows = shadow.split(",");
        List<ThingsModel>[] lists = new List[dataType.getArrayCount()];
        for (int i = 0; i < dataType.getArrayCount(); i++) {
            List<ThingsModel> thingsModelList = new ArrayList<>();
            for (int j = 0; j < dataType.getParams().size(); j++) {
                ThingsModel newThingsModel = new ThingsModel();
                BeanUtils.copyProperties(dataType.getParams().get(j), newThingsModel);
                newThingsModel.setId("array"+"_"+String.format("%02d", i)+"_"+newThingsModel.getId());
                newThingsModel.setName("["+ Integer.toString(i+1) + "]" + newThingsModel.getName());
                newThingsModel.setValue(values[i].equals(" ")?"":values[i]);
                newThingsModel.setShadow(shadows[i].equals(" ")?"":shadows[i]);
                newThingsModel.setType(type);
                thingsModelList.add(newThingsModel);
            }
            lists[i] = thingsModelList;
        }
        dataType.setArrayParams(lists);
    }

    /**
     * 更新简单数组值
     * @param model
     * @param dataType
     * @param value
     * @param shadow
     * @param type
     */
    private void updateSimpleArrayParams(ThingsModel model, DataType dataType,String value, String shadow,Integer type) {
        String[] values = value.split(",");
        String[] shadows = shadow.split(",");
        List<ThingsModel> lists = new ArrayList<>();
        for (int i = 0; i < dataType.getArrayCount(); i++) {
            ThingsModel newThingsModel = new ThingsModel();
            newThingsModel.setId("array"+"_"+String.format("%02d", i)+"_"+model.getId());
            newThingsModel.setName("["+ Integer.toString(i+1) + "]"+model.getName());
            newThingsModel.setValue(values[i].equals(" ")?"":values[i]);
            newThingsModel.setShadow(shadows[i].equals(" ")?"":shadows[i]);
            newThingsModel.setType(type);

            //下面这些属性跟随父
            newThingsModel.setIsMonitor(model.getIsMonitor());
            newThingsModel.setIsReadonly(model.getIsReadonly());
            newThingsModel.setIsTop(model.getIsTop());
            newThingsModel.setIsRecord(model.getIsRecord());
            newThingsModel.setModelIcon(model.getModelIcon());

            DataType dt = new DataType();
            dt.setType(model.getDataType().getArrayType());
            newThingsModel.setDataType(dt);
            lists.add(newThingsModel);
        }
        dataType.setSimpleArrayParams(lists);
    }

    /**
     * 更新对象值
     * @param model
     * @param dataType
     * @param id
     * @param value
     * @param shadow
     * @param type
     */
    private void updateObjectParams(ThingsModel model, DataType dataType, String id, String value, String shadow,Integer type) {
        for (ThingsModel tm : dataType.getParams()) {
            if (tm.getId().equals(id)) {
                tm.setName( tm.getName());
                tm.setValue(value);
                tm.setShadow(shadow);
                tm.setType(type);
                break;
            }
        }
    }

    /**
     * 根据设备运行状态抽出单层的模型
     * @param deviceId
     * @param removeNotRecord 1  排除record=0的数据
     * @return
     */
    @Override
    public List<ThingsModel> selectDeviceRunningStatusByDeviceIdFilterSingle(Long deviceId,Long removeNotRecord){
        DeviceShortOutput deviceShortOutput = selectDeviceRunningStatusByDeviceId(deviceId);
        List<ThingsModel> thingsModels = deviceShortOutput.getThingsModels();
        //定义一个心的接收list
        List<ThingsModel> thingsModelList = new ArrayList<>();
        for(int n =0;n<thingsModels.size();n++){
            DataType dataType = thingsModels.get(n).getDataType();
            if (dataType == null) {
                continue;
            }
            //对象
            if (dataType.getType().equals("object")) {
                List<ThingsModel> params = dataType.getParams();
                if (params == null) {
                    continue;
                }
                params.forEach(p -> {
                    thingsModelList.add(p);
                });
            }
            //数组
            else if (dataType.getType().equals("array") && !dataType.getArrayType().equals("object")) {
                List<ThingsModel> simpleArrayParams = dataType.getSimpleArrayParams();
                if (simpleArrayParams == null) {
                    continue;
                }
                simpleArrayParams.forEach(sap -> {
                    thingsModelList.add(sap);
                });
            }
            //对象数组
            else if (dataType.getType().equals("array") && dataType.getArrayType().equals("object")) {
                List<ThingsModel>[] arrayParams = dataType.getArrayParams();
                if (arrayParams == null) {
                    continue;
                }
                for (int i = 0; i < arrayParams.length; i++) {
                    for (int j = 0; j < arrayParams[i].size(); j++) {
                        thingsModelList.add(arrayParams[i].get(j));
                    }
                }
            }
            //普通
            else {
                thingsModelList.add(thingsModels.get(n));
            }
        }
        List<ThingsModel> result = thingsModelList.stream().filter(thingsModel -> thingsModel.getDataType().getType().equals("decimal") || thingsModel.getDataType().getType().equals("integer")).collect(Collectors.toList());
        if(removeNotRecord==1){
            result = result.stream().filter(t -> t.getIsRecord() == 1).collect(Collectors.toList());
        }
        return result;
    }

    /**
     * 新增设备
     * @param device 设备
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Device insertDevice(Device device) {
        System.out.println(Thread.currentThread().getName());
        // 设备编号唯一检查
        Device existDevice = deviceMapper.selectDeviceBySerialNumber(device.getSerialNumber());
        if (existDevice != null) {
            log.error("设备编号：" + device.getSerialNumber() + "已经存在了，新增设备失败");
            return device;
        }
        SysUser user = getLoginUser().getUser();

        Product product = productService.selectProductByProductId(device.getProductId());
        /**
         * deviceName
         * productId
         * productName
         * isCamera
         * serialNumber
         * firmwareVersion
         * status
         */
        device.setRssi(0);//信号满格
        /**
         * isShadow
         * locationWay
         * thingModelValue  不在这边设置
         * netWorkAddress
         */
        device.setNetworkIp(user.getLoginIp());//IP
        /**
         * longitude
         * latitude
         * activeTime 不在这边设置
         * summary    不在这边设置
         */
        device.setImgUrl(product.getImgUrl());//图片
        /**
         * delFlag不在这边设置
         */
        device.setCreateBy(SecurityUtils.getUserId().toString());//创建人
        device.setCreateTime(DateUtils.getNowDate());//创建时间
        /**
         * updateBy   不在这边设置
         * updateTime 不在这边设置
         * remark     不在这边设置
         */
        device.setTenantId(SecurityUtils.getTenantId());//租户
        device.setBaseId(SecurityUtils.getBaseId());//基地
        device.setDeptId(SecurityUtils.getDeptId());//部门
        device.setUserId(SecurityUtils.getUserId());//用户

        deviceMapper.insertDevice(device);

        // 添加设备用户
        DeviceUser deviceUser = new DeviceUser();
        deviceUser.setUserId(SecurityUtils.getUserId());//用户
        deviceUser.setUserName(SecurityUtils.getUsername());
        deviceUser.setPhonenumber(user.getPhonenumber());
        deviceUser.setDeviceId(device.getDeviceId());
        deviceUser.setDeviceName(device.getDeviceName());
        deviceUser.setIsOwner(1);
        deviceUser.setCreateTime(DateUtils.getNowDate());
        deviceUserMapper.insertDeviceUser(deviceUser);

        // redis缓存设备默认状态（物模型值）
        cacheDeviceStatus(device.getProductId(), device.getSerialNumber());
        return device;
    }

    /**
     * 设备关联用户
     *
     * @param deviceRelateUserInput
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult deviceRelateUser(DeviceRelateUserInput deviceRelateUserInput) {
        // 查询用户信息
        SysUser sysUser = userService.selectUserById(deviceRelateUserInput.getUserId());
        for (int i = 0; i < deviceRelateUserInput.getDeviceNumberAndProductIds().size(); i++) {
            Device existDevice = deviceMapper.selectDeviceBySerialNumber(deviceRelateUserInput.getDeviceNumberAndProductIds().get(i).getDeviceNumber());
            if (existDevice != null) {
                if (existDevice.getUserId().longValue() == deviceRelateUserInput.getUserId().longValue()) {
                    return AjaxResult.error("用户已经拥有设备：" + existDevice.getDeviceName() + ", 设备编号：" + existDevice.getSerialNumber());
                }
                // 先删除设备的所有用户
                deviceUserMapper.deleteDeviceUserByDeviceId(new UserIdDeviceIdModel(null, existDevice.getDeviceId()));
                // 添加新的设备用户
                DeviceUser deviceUser = new DeviceUser();
                deviceUser.setUserId(sysUser.getUserId());
                deviceUser.setUserName(sysUser.getUserName());
                deviceUser.setPhonenumber(sysUser.getPhonenumber());
                deviceUser.setDeviceId(existDevice.getDeviceId());
                deviceUser.setDeviceName(existDevice.getDeviceName());
                deviceUser.setIsOwner(1);
                deviceUser.setCreateTime(DateUtils.getNowDate());
                deviceUserMapper.insertDeviceUser(deviceUser);
                // 更新设备用户信息
                existDevice.setUserId(deviceRelateUserInput.getUserId());
                deviceMapper.updateDevice(existDevice);
            } else {
                // 自动添加设备
                int result = insertDeviceAuto(
                        deviceRelateUserInput.getDeviceNumberAndProductIds().get(i).getDeviceNumber(),
                        deviceRelateUserInput.getUserId(),
                        deviceRelateUserInput.getDeviceNumberAndProductIds().get(i).getProductId());
                if (result == 0) {
                    return AjaxResult.error("设备不存在，自动添加设备时失败，请检查产品编号是否正确");
                }
            }
        }
        return AjaxResult.success("添加设备成功");
    }

    /**
     * 设备认证后自动添加设备
     *
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertDeviceAuto(String serialNumber, Long userId, Long productId) {
        SysUser user = userService.selectUserById(userId);
        // 设备编号唯一检查
        int count = deviceMapper.selectDeviceCountBySerialNumber(serialNumber);
        if (count != 0) {
            log.error("设备编号：" + serialNumber + "已经存在了，新增设备失败");
            return 0;
        }
        Product product = productService.selectProductByProductId(productId);
        // 创建用户
        if (product == null) {
            log.error("自动添加设备时，根据产品ID查找不到对应产品");
            return 0;
        }
        int random = (int) (Math.random() * (90)) + 10;
        List<SysDept> sysTenantDepts = deptMapper.selectTenantById(user.getDeptId());
        if(sysTenantDepts.size()==1){
            user.setTenantId(sysTenantDepts.get(0).getDeptId());
        }
        List<SysDept> sysBaseDepts = deptMapper.selectBaseById(user.getDeptId());
        if(sysBaseDepts.size()==1){
            user.setBaseId(sysBaseDepts.get(0).getDeptId());
        }

        Device device = new Device();
        device.setDeviceName(product.getProductName() + random);
        device.setProductId(product.getProductId());
        device.setProductName(product.getProductName());
        device.setSerialNumber(serialNumber);
        device.setFirmwareVersion(BigDecimal.valueOf(1));
        device.setStatus(1);// 设备状态（1-未激活，2-禁用，3-在线，4-离线）
        device.setRssi(0);
        device.setIsShadow(0);
        device.setLocationWay(1);// 1-自动定位，2-设备定位，3-自定义位置
        device.setNetworkAddress("中国");
        device.setNetworkIp("127.0.0.1");
        // 随机位置
        Base base = baseMapper.selectBaseByDeptId(user.getBaseId());
        if(base != null) {
            String[] split = base.getBaseCoordinate().split(",");
            double baseLongitude = Double.parseDouble(split[0]);
            double baseLatitude = Double.parseDouble(split[1]);
            double randomLongitude = baseLongitude + (Math.random() - 0.5) * 0.1; // 随机范围+-0.05度
            double randomLatitude = baseLatitude + (Math.random() - 0.5) * 0.1;
            device.setLongitude(BigDecimal.valueOf(randomLongitude));
            device.setLatitude(BigDecimal.valueOf(randomLatitude));
        }
        //激活时间
        device.setActiveTime(DateUtils.getNowDate());
        device.setImgUrl(product.getImgUrl());
        device.setCreateBy(user.getUserId().toString());
        device.setCreateTime(DateUtils.getNowDate());
        
        device.setTenantId(user.getTenantId());
        device.setBaseId(user.getBaseId());
        device.setDeptId(user.getDeptId());
        device.setUserId(userId);

        deviceMapper.insertDevice(device);

        // 缓存设备状态
        cacheDeviceStatus(device.getProductId(), device.getSerialNumber());

        // 添加设备用户
        DeviceUser deviceUser = new DeviceUser();
        deviceUser.setUserId(userId);
        deviceUser.setUserName(user.getUserName());
        deviceUser.setPhonenumber(user.getPhonenumber());
        deviceUser.setDeviceId(device.getDeviceId());
        deviceUser.setDeviceName(device.getDeviceName());
        deviceUser.setIsOwner(1);
        return deviceUserMapper.insertDeviceUser(deviceUser);
    }

    /**
     * 获取用户操作设备的影子值
     *
     * @param device
     * @return
     */
    @Override
    public ThingsModelShadow getDeviceShadowThingsModel(Device device) {
        // 物模型
        String thingsModels = thingsModelService.getCacheThingsModelByProductId(device.getProductId());
        JSONObject thingsModelObject = JSONObject.parseObject(thingsModels);
        JSONArray properties = thingsModelObject.getJSONArray("properties");
        JSONArray functions = thingsModelObject.getJSONArray("functions");

        // 物模型值
        List<ThingsModelValueItem> thingsModelValueItems = getCacheDeviceStatus(device.getProductId(), device.getSerialNumber());

        // 查询出设置的影子值
        List<ThingsModelValueItem> shadowList = new ArrayList<>();
        for (int i = 0; i < thingsModelValueItems.size(); i++) {
            if (!thingsModelValueItems.get(i).getValue().equals(thingsModelValueItems.get(i).getShadow())) {
                shadowList.add(thingsModelValueItems.get(i));
            }
        }
        ThingsModelShadow shadow = new ThingsModelShadow();
        for (int i = 0; i < shadowList.size(); i++) {
            boolean isGetValue = false;
            for (int j = 0; j < properties.size(); j++) {
                if (properties.getJSONObject(j).getInteger("isMonitor") == 0 && properties.getJSONObject(j).getString("id").equals(shadowList.get(i).getId())) {
                    ThingsModelSimpleItem item = new ThingsModelSimpleItem(shadowList.get(i).getId(), shadowList.get(i).getShadow(), "");
                    shadow.getProperties().add(item);
                    isGetValue = true;
                    break;
                }
            }
            if (!isGetValue) {
                for (int k = 0; k < functions.size(); k++) {
                    if (functions.getJSONObject(k).getString("id").equals(shadowList.get(i).getId())) {
                        ThingsModelSimpleItem item = new ThingsModelSimpleItem(shadowList.get(i).getId(), shadowList.get(i).getShadow(), "");
                        shadow.getFunctions().add(item);
                        break;
                    }
                }
            }
        }
        return shadow;
    }

    /**
     * 修改设备
     *
     * @param device 设备
     * @return 结果
     */
    @Override
    public AjaxResult updateDevice(Device device) {
        // 设备编号唯一检查
        Device oldDevice = deviceMapper.selectDeviceByDeviceId(device.getDeviceId());
        if (!oldDevice.getSerialNumber().equals(device.getSerialNumber())) {
            Device existDevice = deviceMapper.selectDeviceBySerialNumber(device.getSerialNumber());
            if (existDevice != null) {
                log.error("设备编号：" + device.getSerialNumber() + " 已经存在，新增设备失败");
                return AjaxResult.success("设备编号：" + device.getSerialNumber() + " 已经存在，修改失败", 0);
            }
        }
        device.setUpdateTime(DateUtils.getNowDate());
        // 未激活状态,可以修改产品以及物模型值
        if (device.getStatus() == 1) {
            // 缓存设备状态（物模型值）
            cacheDeviceStatus(device.getProductId(), device.getSerialNumber());

        } else {
            device.setProductId(null);
            device.setProductName(null);
        }
        deviceMapper.updateDevice(device);
        // 更新前设备状态为禁用，启用后状态默认为离线，满足时下发获取设备最新状态指令
        if (oldDevice.getStatus() == 2 && device.getStatus() == 4) {
            // 发布设备信息，设备收到该消息后上报最新状态
            emqxService.publishInfo(oldDevice.getProductId(), oldDevice.getSerialNumber());
        }
        return AjaxResult.success("修改成功", 1);
    }

    /**
     * 生成设备唯一编号
     *
     * @return 结果
     */
    @Override
    public String generationDeviceNum() {
        // 设备编号：D + userId + 15位随机字母和数字
        SysUser user = getLoginUser().getUser();
        String number = "D" + user.getUserId().toString() + toolService.getStringRandom(10);
        int count = deviceMapper.getDeviceNumCount(number);
        if (count == 0) {
            return number;
        } else {
            generationDeviceNum();
        }
        return "";
    }

    /**
     * 更新设备status，ip，ipaddress，坐标和激活时间和记录日志，用在设备上下线的时候使用
     * @param device
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDeviceStatusAndLocation(Device device, String ipAddress) {
        // 坐标不为null，且是自动定位就更新设备坐标
        if (ipAddress != "") {
            // 定位方式(1=ip自动定位，2=设备定位，3=自定义)
            if (device.getLocationWay() == 1) {
                device.setNetworkIp(ipAddress);
                setLocation(ipAddress, device);
            }
        }
        //更新设备激活时间
        if (device.getActiveTime() == null && device.getStatus()==3) {
            device.setActiveTime(DateUtils.getNowDate());
        }
        int result = deviceMapper.updateDeviceStatus(device);

        // 添加到设备日志
        DeviceLog deviceLog = new DeviceLog();
        deviceLog.setDeviceId(device.getDeviceId());
        deviceLog.setDeviceName(device.getDeviceName());
        deviceLog.setSerialNumber(device.getSerialNumber());
        deviceLog.setTenantId(device.getTenantId());
        deviceLog.setBaseId(device.getBaseId());
        deviceLog.setDeptId(device.getDeptId());
        deviceLog.setUserId(device.getUserId());
        deviceLog.setCreateTime(DateUtils.getNowDate());
        // 日志模式 1=影子模式，2=在线模式，3=其他
        deviceLog.setIsMonitor(0);
        deviceLog.setMode(3);
        if (device.getStatus() == 3) {
            deviceLog.setLogValue("1");
            deviceLog.setRemark("设备上线");
            deviceLog.setIdentity("online");
            deviceLog.setLogType(5);
        } else if (device.getStatus() == 4) {
            deviceLog.setLogValue("0");
            deviceLog.setRemark("设备离线");
            deviceLog.setIdentity("offline");
            deviceLog.setLogType(6);
        }
        logService.saveDeviceLog(deviceLog);
        return result;
    }

    /**
     * 设备状态
     * @param device
     * @return 结果
     */
    @Override
    public int updateDeviceStatus(Device device) {
        return deviceMapper.updateDeviceStatus(device);
    }

    /**
     * 上报设备信息
     *
     * @param device 设备
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int reportDevice(Device device, Device deviceEntity) {
        // 未采用设备定位则清空定位，定位方式(1=ip自动定位，2=设备定位，3=自定义)
        if (deviceEntity.getLocationWay() != 2) {
            device.setLatitude(null);
            device.setLongitude(null);
        }
        int result = 0;
        if (deviceEntity != null) {
            if (deviceEntity.getUserId().longValue() != device.getUserId().longValue()) {
                // 先删除设备的所有用户
                deviceUserMapper.deleteDeviceUserByDeviceId(new UserIdDeviceIdModel(null, deviceEntity.getDeviceId()));
                // 添加新的设备用户
                SysUser sysUser = userService.selectUserById(device.getUserId());
                DeviceUser deviceUser = new DeviceUser();
                deviceUser.setUserId(sysUser.getUserId());
                deviceUser.setUserName(sysUser.getUserName());
                deviceUser.setPhonenumber(sysUser.getPhonenumber());
                deviceUser.setDeviceId(deviceEntity.getDeviceId());
                deviceUser.setDeviceName(deviceEntity.getDeviceName());
                deviceUser.setIsOwner(1);
                deviceUser.setCreateTime(DateUtils.getNowDate());
                deviceUserMapper.insertDeviceUser(deviceUser);
                // 更新设备用户信息
                device.setUserId(device.getUserId());
            }
            device.setUpdateTime(DateUtils.getNowDate());
            if (deviceEntity.getActiveTime() == null || deviceEntity.getActiveTime().equals("")) {
                device.setActiveTime(DateUtils.getNowDate());
            }
            // 不更新物模型
            device.setThingsModelValue(null);
            result = deviceMapper.updateDeviceBySerialNumber(device);
        }
        return result;
    }

    /**
     * 重置设备状态
     *
     * @return 结果
     */
    @Override
    public int resetDeviceStatus(String deviceNum) {
        int result = deviceMapper.resetDeviceStatus(deviceNum);
        return result;
    }

    /**
     * 从萤石云导入设备和通道
     * @param productId
     * @param serialNums
     * @return -1 配置问题 -2 获取accessToken出现问题 -3产品不存在
     */
    @Override
    public int importDevice(Long productId, List<String> serialNums) {
        YsConfig ysConfig = ysConfigService.selectYsConfigByProductId(productId);
        Product product = productService.selectProductByProductId(productId);
        //删除该产品下的全部通道
        ysDeviceChannelMapper.deleteYsDeviceChannelByProductId(productId);
        String accessToken;
        int successTotal=0;
        if(ysConfig==null || ysConfig.getAppKey()==null || ysConfig.getSecret()==null){
            return -1;
        }
        if(product==null){
            return -3;
        }
        //查询数据并导入
        for(String serialNum : serialNums){
                try {
                    //获取设备信息
                    JSONObject ysDevice = null;
                    try {
                        ysDevice = ysApiUtils.getDeviceInfo(productId,serialNum);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    //同步设备
                    Device iotDevice = deviceMapper.selectDeviceBySerialNumber(ysDevice.getString("deviceSerial"));
                    if(iotDevice==null){
                        iotDevice = new Device();
                    }
                    iotDevice.setDeviceName(ysDevice.getString("deviceName"));
                    iotDevice.setProductId(product.getProductId());
                    iotDevice.setProductName(product.getProductName());
                    iotDevice.setIsCamera("1");
                    iotDevice.setFirmwareVersion(BigDecimal.valueOf(1));
                    iotDevice.setSerialNumber(ysDevice.getString("deviceSerial"));
                    Integer status = ysDevice.getInteger("status");
                    if(status==0){
                        iotDevice.setStatus(4);
                    }else if(status==1){
                        iotDevice.setStatus(3);
                    }
                    iotDevice.setLocationWay(1);
                    iotDevice.setNetworkIp(ysDevice.getString("netAddress"));
                    SysUser user = userService.selectUserById(SecurityUtils.getUserId());
                    Base base = baseMapper.selectBaseByDeptId(user.getBaseId());
                    if(base != null) {
                        String[] split = base.getBaseCoordinate().split(",");
                        double baseLongitude = Double.parseDouble(split[0]);
                        double baseLatitude = Double.parseDouble(split[1]);
                        double randomLongitude = baseLongitude + (Math.random() - 0.5) * 0.1; // 随机范围+-0.05度
                        double randomLatitude = baseLatitude + (Math.random() - 0.5) * 0.1;
                        iotDevice.setLongitude(BigDecimal.valueOf(randomLongitude));
                        iotDevice.setLatitude(BigDecimal.valueOf(randomLatitude));
                    }
                    iotDevice.setActiveTime(new Date(ysDevice.getLong("updateTime")));
                    iotDevice.setImgUrl(product.getImgUrl());
                    iotDevice.setTenantId(SecurityUtils.getTenantId());//租户
                    iotDevice.setBaseId(SecurityUtils.getBaseId());//基地
                    iotDevice.setDeptId(SecurityUtils.getDeptId());//部门
                    iotDevice.setUserId(SecurityUtils.getUserId());//用户
                    if(iotDevice.getDeviceId()==null){
                        deviceMapper.insertDevice(iotDevice);
                        // 添加设备用户
                        DeviceUser deviceUser = new DeviceUser();
                        deviceUser.setUserId(SecurityUtils.getUserId());//用户
                        deviceUser.setUserName(SecurityUtils.getUsername());
                        deviceUser.setPhonenumber(user.getPhonenumber());
                        deviceUser.setDeviceId(iotDevice.getDeviceId());
                        deviceUser.setDeviceName(iotDevice.getDeviceName());
                        deviceUser.setIsOwner(1);
                        deviceUser.setCreateTime(DateUtils.getNowDate());
                        deviceUserMapper.insertDeviceUser(deviceUser);
                    }else{
                        deviceMapper.updateDevice(iotDevice);
                    }
                    //设置通道
                    //根据产品id删除所有的通道
                    List<YsDeviceChannel> yschannels = ysApiUtils.getChannelsByDevice(productId,serialNum).toJavaList(YsDeviceChannel.class);
                    for(YsDeviceChannel channel:yschannels){
                        channel.setProductId(productId);
                        String channelStatus = channel.getStatus();
                        if(channelStatus.equals("0")){
                            channel.setStatus("4");
                        }else if(channelStatus.equals("1")){
                            channel.setStatus("3");
                        }
                        channel.setChannelNo(channel.getDeviceSerial()+"-"+channel.getChannelNo());
                        ysDeviceChannelMapper.insertYsDeviceChannel(channel);
                    }
                    successTotal++;
                } catch (Exception e) {

                }
            }

        return successTotal;
    }

    /**
     * 删除设备
     *
     * @param deviceId 需要删除的设备主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteDeviceByDeviceId(Long deviceId) throws SchedulerException {
        SysUser user = getLoginUser().getUser();
        // 是否为普通用户，普通用户如果不是设备所有者，只能删除设备用户和用户自己的设备关联分组信息
        boolean isGeneralUser = false;
        List<SysRole> roles = user.getRoles();
        for (int i = 0; i < roles.size(); i++) {
            if (roles.get(i).getRoleKey().equals("general")) {
                isGeneralUser = true;
                break;
            }
        }
        Device device = deviceMapper.selectDeviceByDeviceId(deviceId);
        if (isGeneralUser && device.getUserId().longValue() != user.getUserId()) {
            // 删除用户分组中的设备 普通用户，且不是设备所有者。
            deviceMapper.deleteDeviceGroupByDeviceId(new UserIdDeviceIdModel(user.getUserId(), deviceId));
            // 删除用户的设备用户信息。
            deviceUserMapper.deleteDeviceUserByDeviceId(new UserIdDeviceIdModel(user.getUserId(), deviceId));
        } else {
            // 删除设备分组。  租户、管理员和设备所有者
            deviceMapper.deleteDeviceGroupByDeviceId(new UserIdDeviceIdModel(null, deviceId));
            // 删除设备用户。
            deviceUserMapper.deleteDeviceUserByDeviceId(new UserIdDeviceIdModel(null, deviceId));
            // 删除定时任务
            deviceJobService.deleteJobByDeviceIds(new Long[]{deviceId});
            // 批量删除设备日志
            logService.deleteDeviceLogByDeviceNumber(device.getSerialNumber());
            // 批量删除设备告警记录
            alertLogService.deleteAlertLogBySerialNumber(device.getSerialNumber());
            // redis中删除设备物模型（状态）
            String key = devicePreKey + device.getProductId() + "_" + device.getSerialNumber();
            redisCache.deleteObject(key);
            // 删除设备
            deviceMapper.deleteDeviceByDeviceIds(new Long[]{deviceId});
        }
        return 1;
    }

    /**
     * 根据IP获取地址
     *
     * @param ip
     * @return
     */
    private void setLocation(String ip, Device device) {
        String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp";
        String address = "未知地址";
        // 内网不查询
        if (IpUtils.internalIp(ip)) {
            device.setNetworkAddress("内网IP");
        }
        try {
            String rspStr = HttpUtils.sendGet(IP_URL, "ip=" + ip + "&json=true", Constants.GBK);
            if (!StringUtils.isEmpty(rspStr)) {
                JSONObject obj = JSONObject.parseObject(rspStr);
                device.setNetworkAddress(obj.getString("addr"));
                System.out.println(device.getSerialNumber() + "- 设置地址：" + obj.getString("addr"));
                // 查询经纬度
                setLatitudeAndLongitude(obj.getString("city"), device);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * 设置经纬度
     *
     * @param city
     */
    private void setLatitudeAndLongitude(String city, Device device) {
        String BAIDU_URL = "https://api.map.baidu.com/geocoder";
        String baiduResponse = HttpUtils.sendGet(BAIDU_URL, "address=" + city + "&output=json", Constants.GBK);
        if (!StringUtils.isEmpty(baiduResponse)) {
            JSONObject baiduObject = JSONObject.parseObject(baiduResponse);
            JSONObject location = baiduObject.getJSONObject("result").getJSONObject("location");
            device.setLongitude(location.getBigDecimal("lng"));
            device.setLatitude(location.getBigDecimal("lat"));
            System.out.println(device.getSerialNumber() + "- 设置经度：" + location.getBigDecimal("lng") + "，设置纬度：" + location.getBigDecimal("lat"));
        }
    }

    /**
     * 缓存设备状态到redis
     *
     * @return
     */
    public List<ThingsModelValueItem> cacheDeviceStatus(Long productId, String serialNumber) {
        // 获取物模型,设置默认值
        String thingsModels = thingsModelService.getCacheThingsModelByProductId(productId);
        JSONObject thingsModelObject = JSONObject.parseObject(thingsModels);
        JSONArray properties = thingsModelObject.getJSONArray("properties");
        JSONArray functions = thingsModelObject.getJSONArray("functions");
        List<ThingsModelValueItem> valueList = properties.toJavaList(ThingsModelValueItem.class);
        valueList.addAll(functions.toJavaList(ThingsModelValueItem.class));
        // redis存储设备默认状态 键：产品ID_设备编号
        String key = devicePreKey + productId + "_" + serialNumber;
        Map<String, String> maps = new HashMap<>();
        for (int i = 0; i < valueList.size(); i++) {
            valueList.get(i).setValue("");
            valueList.get(i).setShadow("");
            // TODO：待测试
            if (valueList.get(i).getDataType().getType().equals("array")) {
                // 数组元素赋值（英文逗号分割的字符串,包含简单类型数组和对象类型数组，数组元素ID格式：array_01_humidity）
                String defaultValue = " ";
                if (valueList.get(i).getDataType().getArrayType().equals("object")) {
                    // 对象数组赋默认值
                    for (int k = 1; k < valueList.get(i).getDataType().getArrayCount(); k++) {
                        // 默认值需要保留为空格,便于解析字符串为数组
                        defaultValue = defaultValue + ", ";
                    }
                    for (int j = 0; j < valueList.get(i).getDataType().getParams().size(); j++) {
                        maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#V", defaultValue);
                        maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#S", defaultValue);
                        maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#N", valueList.get(i).getDataType().getParams().get(j).getName());
                        maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#M", String.valueOf(valueList.get(i).getDataType().getParams().get(j).getIsMonitor()));
                    }
                } else {
                    // 普通数组赋默认值
                    for (int k = 1; k < valueList.get(i).getDataType().getArrayCount(); k++) {
                        defaultValue = defaultValue + ", ";
                    }
                    maps.put(valueList.get(i).getId() + "#V", defaultValue);
                    maps.put(valueList.get(i).getId() + "#S", defaultValue);
                    maps.put(valueList.get(i).getId() + "#N", valueList.get(i).getName());
                    maps.put(valueList.get(i).getId() + "#M", String.valueOf(valueList.get(i).getIsMonitor()));
                }
            } else if (valueList.get(i).getDataType().getType().equals("object")) {
                // 对象类型赋默认值
                for (int j = 0; j < valueList.get(i).getDataType().getParams().size(); j++) {
                    maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#V", "");
                    maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#S", "");
                    maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#N", valueList.get(i).getDataType().getParams().get(j).getName());
                    maps.put(valueList.get(i).getDataType().getParams().get(j).getId() + "#M", String.valueOf(valueList.get(i).getDataType().getParams().get(j).getIsMonitor()));
                }
            } else {
                //其他类型类型赋默认值（integer、decimal、string、enum）
                maps.put(valueList.get(i).getId() + "#V", "");
                maps.put(valueList.get(i).getId() + "#S", "");
                maps.put(valueList.get(i).getId() + "#N", valueList.get(i).getName());
                maps.put(valueList.get(i).getId() + "#M", String.valueOf(valueList.get(i).getIsMonitor()));
            }
        }
        redisCache.hashPutAll(key, maps);
        return valueList;
    }

    /**
     * 获取Redis缓存的设备全部状态（物模型值）
     *
     * @param productId    产品ID
     * @param deviceNumber 设备编号
     * @return
     */
    private List<ThingsModelValueItem> getCacheDeviceStatus(Long productId, String deviceNumber) {
        Map<String, String> map = redisCache.hashEntity(devicePreKey + productId + "_" + deviceNumber);
        List<ThingsModelValueItem> valueList = new ArrayList<>();
        if (map == null || map.size() == 0) {
            // 缓存设备状态（物模型值）到redis
            valueList = cacheDeviceStatus(productId, deviceNumber);
        } else {
            // 获取redis缓存的物模型值
            valueList = mapToValueList(map, "#S");
        }
        return valueList;
    }

    /**
     * 批量查询设备的缓存状态（物模型值）
     *
     * @param deviceList
     * @param condition  条件：#S=影子值，#V=值，#N=名称，#M=是否监测数据，空字符=所有数据
     * @return
     */
    private Map<String, List<ThingsModelValueItem>> getBatchCacheDeviceStatus(List<DeviceShortOutput> deviceList, String condition) {
        // 批量查询hkey和value
        Set<String> set = new HashSet<>();
        for (int i = 0; i < deviceList.size(); i++) {
            set.add(devicePreKey + deviceList.get(i).getProductId() + "_" + deviceList.get(i).getSerialNumber());
        }
        // 只查询影子值
        Map<String, List<ThingsModelValueItem>> mapThingsModelValue = new HashMap<>();
        Map<String, Map> map = redisCache.hashGetAllByKeys(set, condition);
        map.forEach((k, v) -> {
            mapThingsModelValue.put(k, mapToValueList(v, "#S"));
        });
        // 如果redis键和设备不匹配，添加redis缓存
        if (map.size() != deviceList.size()) {
            for (int i = 0; i < deviceList.size(); i++) {
                String key = devicePreKey + deviceList.get(i).getProductId() + "_" + deviceList.get(i).getSerialNumber();
                if (map.get(key) != null) {
                    continue;
                } else {
                    mapThingsModelValue.put(key, getCacheDeviceStatus(deviceList.get(i).getProductId(), deviceList.get(i).getSerialNumber()));
                }
            }
        }
        return mapThingsModelValue;
    }

    /**
     * 设备状态Map转物模型值集合
     *
     * @param map
     * @param primaryIdentity 主要标识符 #S、#V、#M、#N
     * @return
     */
    private List<ThingsModelValueItem> mapToValueList(Map<String, String> map, String primaryIdentity) {
        List<ThingsModelValueItem> valueList = new ArrayList<>();
        for (Object key : map.keySet()) {
            if (key.toString().endsWith(primaryIdentity)) {
                ThingsModelValueItem item = new ThingsModelValueItem();
                // 物模型ID
                item.setId(key.toString().substring(0, key.toString().length() - 2));
                // 物模型名称
                String name = map.get(key.toString().substring(0, key.toString().length() - 1) + "N");
                if (name == null) {
                    name = "";
                }
                item.setName(name.replace("\"", ""));
                // 物模型值
                String value = map.get(key.toString().substring(0, key.toString().length() - 1) + "V");
                if (value == null) {
                    value = "";
                }
                item.setValue(value.replace("\"", ""));
                // 物模型值
                String shadow = map.get(key.toString().substring(0, key.toString().length() - 1) + "S");
                if (shadow == null) {
                    shadow = "";
                }
                item.setShadow(shadow.replace("\"", ""));
                // 物模型值
                String isMonitor = map.get(key.toString().substring(0, key.toString().length() - 1) + "M");
                if (isMonitor == null) {
                    isMonitor = "";
                }
                item.setIsMonitor("".equals(isMonitor.replace("\"", "")) ? 0 : Integer.parseInt(isMonitor.replace("\"", "")));
                valueList.add(item);
            }
        }
        return valueList;
    }

}
