package com.frog.iot.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.security.MessageDigest;
import java.util.Base64;

/**
 *
 */
public class AESUtils {

    /**
     * 加密用的Key 可以用26个字母和数字组成 使用AES-128-CBC加密模式，key需要为16位。iv 偏移量，长度16
     */
    private static final String ivString = "frog-agriculture";
    private static final String ENCRYPT_MODE = "CBC"; // ECB和CBC两种模式

    // 加密
    public static String encrypt(String plainText, String key) {
        // 判断Key是否正确
        if (key == null || key.length() != 16) {
            System.out.println("Key不能为空，长度不是16位");
            return null;
        }
        try {
            byte[] raw = key.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/" + AESUtils.ENCRYPT_MODE + "/PKCS5Padding");

            // CBC模式需要IV
            IvParameterSpec iv = new IvParameterSpec(ivString.substring(0, 16).getBytes("utf-8")); // 确保IV长度为16字节
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(plainText.getBytes("utf-8"));
            String encryptedStr = Base64.getEncoder().encodeToString(encrypted); // 使用java.util.Base64进行编码

            return encryptedStr;
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }

    // 解密
    public static String decrypt(String cipherText, String key) {
        // 判断Key是否正确
        if (key == null || key.length() != 16) {
            System.out.println("Key不能为空，长度不是16位");
            return null;
        }

        // Base64解码
        byte[] decodedBytes = Base64.getDecoder().decode(cipherText);

        try {
            byte[] raw = key.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/" + AESUtils.ENCRYPT_MODE + "/PKCS5Padding");

            // 使用与加密时相同的IV
            IvParameterSpec iv = new IvParameterSpec(ivString.substring(0, 16).getBytes("utf-8"));
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(decodedBytes);

            return new String(original, "utf-8");
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }

    /**
     * 进行MD5加密
     *
     * @param s 要进行MD5转换的字符串
     * @return 该字符串的MD5值的8-24位
     */
    public static String getMD5(String s) {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str).substring(8, 24);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
