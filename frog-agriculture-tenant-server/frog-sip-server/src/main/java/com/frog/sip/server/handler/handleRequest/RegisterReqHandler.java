package com.frog.sip.server.handler.handleRequest;

import com.frog.common.utils.DateUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.service.IDeviceService;
import com.frog.sip.server.conf.SysSipConfig;
import com.frog.sip.domain.SipConfig;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.domain.SipDeviceChannel;
import com.frog.sip.server.enums.SipDeviceStatus;
import com.frog.sip.server.handler.IReqHandler;
import com.frog.sip.server.model.SipDate;
import com.frog.sip.server.listener.IGBListener;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.service.IMqttService;
import com.frog.sip.service.ISipConfigService;
import com.frog.sip.service.ISipDeviceChannelService;
import com.frog.sip.service.ISipDeviceService;
import com.frog.sip.server.transcation.SipSender;
import com.frog.sip.server.util.DigestAuthUtil;
import com.frog.sip.server.enums.DeviceChannelStatus;
import gov.nist.javax.sip.address.AddressImpl;
import gov.nist.javax.sip.address.SipUri;
import gov.nist.javax.sip.header.Expires;
import gov.nist.javax.sip.header.SIPDateHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.sip.RequestEvent;
import javax.sip.SipException;
import javax.sip.SipFactory;
import javax.sip.header.*;
import javax.sip.message.Request;
import javax.sip.message.Response;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@Slf4j
@Component
public class RegisterReqHandler  implements InitializingBean, IReqHandler {

    @Autowired
    private ISipDeviceService sipDeviceService;
    @Autowired
    private ISipDeviceChannelService sipDeviceChannelService;
    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ISipConfigService sipConfigService;

    @Autowired
    private IGBListener sipListener;

    @Autowired
    private ISipCmd sipCmd;

    @Autowired
    private SysSipConfig sysSipConfig;
    @Autowired
    private IMqttService mqttService;
    @Autowired
    SipSender sipSender;
    @Autowired
    HeaderBuilder headerBuilder;

    @Override
    public void processMsg(RequestEvent evt) {
        log.info(evt.getRequest().toString());
        try {
            Request request = evt.getRequest();
            Response response;
            // 注册标志  0：未携带授权头或者密码错误  1：注册成功   2：注销成功
            int registerFlag;
            FromHeader fromHeader = (FromHeader) request.getHeader(FromHeader.NAME);
            AddressImpl address = (AddressImpl) fromHeader.getAddress();
            SipUri uri = (SipUri) address.getURI();
            String sipId = uri.getUser();
            //取默认Sip配置
            SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(sipId);
            if (sipConfig != null) {
                AuthorizationHeader authorhead = (AuthorizationHeader) request.getHeader(AuthorizationHeader.NAME);
                // 校验密码是否正确
                if (authorhead == null && !ObjectUtils.isEmpty(sipConfig.getPassword())) {
                    log.info("未携带授权头 回复401");
                    response = headerBuilder.createRegisterUnauthorize(request);
                    new DigestAuthUtil().generateChallenge(SipFactory.getInstance().createHeaderFactory(), response, sipConfig.getDomain());
                    sipSender.transmit(response);
                    return;
                }
                //校验sip配置密码
                boolean pcheck = new DigestAuthUtil().doAuthenticatePlainTextPassword(request,
                        sipConfig.getPassword());
                //校验产品sip配置密码
                boolean syscheck = new DigestAuthUtil().doAuthenticatePlainTextPassword(request,
                        sysSipConfig.getPassword());
                //有一个符合就注册成功
                if (!pcheck && !syscheck) {
                    // 注册失败
                    response = headerBuilder.createRegisterForbidden(request);
                    response.setReasonPhrase("wrong password");
                    log.info("[注册请求] 密码/SIP服务器ID错误, 回复403");
                    sipSender.transmit(response);
                    return;
                }
                response = headerBuilder.createRegisterOk(request);
                ExpiresHeader expiresHeader = (ExpiresHeader) request.getHeader(Expires.NAME);
                ViaHeader viaHeader = (ViaHeader) request.getHeader(ViaHeader.NAME);
                String received = viaHeader.getReceived();
                int rPort = viaHeader.getRPort();
                if (StringUtils.isEmpty(received) || rPort == -1) {
                    received = viaHeader.getHost();
                    rPort = viaHeader.getPort();
                }
                SipDevice sipDevice = new SipDevice();
                sipDevice.setStreammode("UDP");
                sipDevice.setDeviceSipId(sipId);
                sipDevice.setIp(received);
                sipDevice.setPort(rPort);
                sipDevice.setHostaddress(received.concat(":").concat(String.valueOf(rPort)));
                /**
                 * 监控设备更换SIP注册IP地址或者更换设备ID之后，设备会发送一个注销的报文，区分的关键点就是Expires为0。
                 * 如果不更换ID，但是更换通道ID设备发注销报文，也不会重新注册
                 */
                //注销
                if (expiresHeader != null && expiresHeader.getExpires() == 0) {
                    registerFlag = 2;
                }
                //注册
                else {
                    registerFlag = 1;
                    // 判断TCP还是UDP
                    ViaHeader reqViaHeader = (ViaHeader) request.getHeader(ViaHeader.NAME);
                    String transport = reqViaHeader.getTransport();
                    sipDevice.setTransport(transport.equals("TCP") ? "TCP" : "UDP");
                }
                //回复register
                sipSender.transmit(response);
                // 注册成功业务处理
                if (registerFlag == 1) {
                    log.info("注册成功! sipId:" + sipDevice.getDeviceSipId());
                    //设置sipDevice注册事件
                    sipDevice.setRegistertime(DateUtils.getNowDate());
                    //设置sipDevice为在线状态
                    sipDevice.setOnline(SipDeviceStatus.online.getStatus());
                    //设置设备的productId
                    Device device = deviceService.selectDeviceBySerialNumber(sipDevice.getDeviceSipId());
                    if(device != null){
                        sipDevice.setProductId(device.getProductId());
                    }
                    sipDeviceService.updateDevice(sipDevice);

                    /** 更新设备的状态和记录设备上线日志 */
                    Device iotDevice = deviceService.selectShortDeviceBySerialNumber(sipId);
                    iotDevice.setStatus(3);
                    //获取设备IP
                    ViaHeader header = (ViaHeader) evt.getRequest().getHeader(ViaHeader.NAME);
                    deviceService.updateDeviceStatusAndLocation(iotDevice, header.getHost());

                    List<SipDeviceChannel> channels = sipDeviceChannelService.selectSipDeviceChannelByDeviceSipId(sipDevice.getDeviceSipId());
                    if (channels.size() > 0) {
                        // 重新注册更新设备和通道，以免设备替换或更新后信息无法更新
                        //这边传入的deviceonline状态就是3，然后通过DeviceInfo发布到了iotdevice
                        onRegister(sipDevice);
                    }
                }
                //注销成功业务处理
                else if (registerFlag == 2) {
                    //设置sipDevice的在线状态为离线4
                    sipDevice.setOnline(SipDeviceStatus.offline.getStatus());
                    sipDeviceService.updateSipDeviceStatus(sipDevice);
                    //发布设备状态到iot设备，跟新设备状态和发布主题到前端
                    mqttService.publishSipDeviceInfoToInfoPost(sipDevice);

                    /** 更新设备状态和记录设备下线日志 */
                    Device iotDevice = deviceService.selectShortDeviceBySerialNumber(sipId);
                    iotDevice.setStatus(4);
                    //获取设备IP
                    ViaHeader header = (ViaHeader) evt.getRequest().getHeader(ViaHeader.NAME);
                    deviceService.updateDeviceStatusAndLocation(iotDevice, header.getHost());

                    //手动更新通道，因为设备注销，无法发送CataLog来获取通道状态，设备不在线设备下面的通道全部设置为离线状态
                    List<SipDeviceChannel> channels = sipDeviceChannelService.selectSipDeviceChannelByDeviceSipId(sipDevice.getDeviceSipId());
                    if (channels != null && !channels.isEmpty()) {
                        for (SipDeviceChannel channel : channels) {
                            channel.setStatus(DeviceChannelStatus.offline.getValue());
                            sipDeviceChannelService.updateChannel(sipDevice.getDeviceSipId(), channel);
                        }
                    }
                }

            }
        } catch (SipException | NoSuchAlgorithmException | ParseException e) {
            e.printStackTrace();
        }
    }

    public void onRegister(SipDevice device) {
        // TODO 查询设备信息和下挂设备信息 自动拉流
        sipCmd.deviceInfoQuery(device);
        sipCmd.catalogQuery(device);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = "REGISTER";
        sipListener.addRequestProcessor(method, this);
    }
}
