package com.frog.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.frog.iot.mapper.YsDeviceChannelMapper;
import com.frog.iot.domain.YsDeviceChannel;
import com.frog.iot.service.IYsDeviceChannelService;

/**
 * 萤石云设备通道Service业务层处理
 * 
 * @author nealtsiao
 * @date 2024-11-25
 */
@Service
public class YsDeviceChannelServiceImpl implements IYsDeviceChannelService 
{
    @Autowired
    private YsDeviceChannelMapper ysDeviceChannelMapper;

    /**
     * 查询萤石云设备通道
     * 
     * @param channelId 萤石云设备通道主键
     * @return 萤石云设备通道
     */
    @Override
    public YsDeviceChannel selectYsDeviceChannelByChannelId(Long channelId)
    {
        return ysDeviceChannelMapper.selectYsDeviceChannelByChannelId(channelId);
    }

    /**
     * 查询萤石云设备通道列表
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 萤石云设备通道
     */
    @Override
    public List<YsDeviceChannel> selectYsDeviceChannelList(YsDeviceChannel ysDeviceChannel)
    {
        return ysDeviceChannelMapper.selectYsDeviceChannelList(ysDeviceChannel);
    }

    /**
     * 新增萤石云设备通道
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 结果
     */
    @Override
    public int insertYsDeviceChannel(YsDeviceChannel ysDeviceChannel)
    {
        return ysDeviceChannelMapper.insertYsDeviceChannel(ysDeviceChannel);
    }

    /**
     * 修改萤石云设备通道
     * 
     * @param ysDeviceChannel 萤石云设备通道
     * @return 结果
     */
    @Override
    public int updateYsDeviceChannel(YsDeviceChannel ysDeviceChannel)
    {
        return ysDeviceChannelMapper.updateYsDeviceChannel(ysDeviceChannel);
    }

    /**
     * 批量删除萤石云设备通道
     * 
     * @param channelIds 需要删除的萤石云设备通道主键
     * @return 结果
     */
    @Override
    public int deleteYsDeviceChannelByChannelIds(Long[] channelIds)
    {
        return ysDeviceChannelMapper.deleteYsDeviceChannelByChannelIds(channelIds);
    }

    /**
     * 删除萤石云设备通道信息
     * 
     * @param channelId 萤石云设备通道主键
     * @return 结果
     */
    @Override
    public int deleteYsDeviceChannelByChannelId(Long channelId)
    {
        return ysDeviceChannelMapper.deleteYsDeviceChannelByChannelId(channelId);
    }
}
