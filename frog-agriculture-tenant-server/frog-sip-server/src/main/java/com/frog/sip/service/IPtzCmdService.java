package com.frog.sip.service;

import com.frog.sip.server.model.PtzDirectionInput;
import com.frog.sip.server.model.PtzZoomInput;

public interface IPtzCmdService {
    boolean direction(String deviceId, String channelId, PtzDirectionInput ptzDirectionInput);
    boolean zoom(String deviceId, String channelId, PtzZoomInput ptzZoomInput);
}
