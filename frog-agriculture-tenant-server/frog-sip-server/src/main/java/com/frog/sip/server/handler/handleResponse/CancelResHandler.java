package com.frog.sip.server.handler.handleResponse;

import com.frog.sip.server.handler.IResHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.sip.ResponseEvent;
import java.text.ParseException;
@Slf4j
@Component
public class CancelResHandler implements InitializingBean, IResHandler {
    @Override
    public void processMsg(ResponseEvent evt) throws ParseException {

    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
