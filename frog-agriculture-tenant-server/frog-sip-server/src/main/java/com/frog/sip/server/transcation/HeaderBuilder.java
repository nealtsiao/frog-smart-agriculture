package com.frog.sip.server.transcation;

import com.frog.sip.domain.SipConfig;
import com.frog.sip.domain.SipDevice;
import com.frog.sip.server.model.DialogInfo;
import com.frog.sip.server.model.SipDate;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.ISipConfigService;
import com.frog.sip.server.util.SipUtil;
import gov.nist.javax.sip.header.SIPDateHeader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.sip.*;
import javax.sip.address.Address;
import javax.sip.address.SipURI;
import javax.sip.header.*;
import javax.sip.message.Request;
import javax.sip.message.Response;
import java.text.ParseException;
import java.util.*;

@Component
@Slf4j
public class HeaderBuilder {
    @Autowired
    private SipFactory sipFactory;

    @Autowired
    @Qualifier(value = "udpSipServer")
    private SipProvider sipserver;

    @Autowired
    private ISipConfigService sipConfigService;

    @Autowired
    private ISipCacheService sipCacheService;

    /**
     * 创建Message请求头
     * @param device
     * @param content
     * @param viaTag
     * @param fromTag
     * @param toTag
     * @return
     * @throws ParseException
     * @throws InvalidArgumentException
     * @throws PeerUnavailableException
     */
    public Request createMessageRequest(SipDevice device, String content, String viaTag, String fromTag, String toTag)
            throws ParseException, InvalidArgumentException, PeerUnavailableException {
        Request request = null;
        // sipuri
        SipURI requestURI = sipFactory.createAddressFactory().createSipURI(device.getDeviceSipId(), device.getHostaddress());

        SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(device.getDeviceSipId());
        if (sipConfig != null) {
            // via
            ArrayList<ViaHeader> viaHeaders = new ArrayList<ViaHeader>();
            ViaHeader viaHeader = sipFactory.createHeaderFactory().createViaHeader(sipConfig.getIp(), sipConfig.getPort(), device.getTransport(), viaTag);
            viaHeader.setRPort();
            viaHeaders.add(viaHeader);
            // from
            SipURI fromSipURI = sipFactory.createAddressFactory().createSipURI(sipConfig.getServerSipid(), sipConfig.getIp() + ":" + sipConfig.getPort());
            Address fromAddress = sipFactory.createAddressFactory().createAddress(fromSipURI);
            FromHeader fromHeader = sipFactory.createHeaderFactory().createFromHeader(fromAddress, fromTag);
            // to
            SipURI toSipURI = sipFactory.createAddressFactory().createSipURI(device.getDeviceSipId(), sipConfig.getDomain());
            Address toAddress = sipFactory.createAddressFactory().createAddress(toSipURI);
            ToHeader toHeader = sipFactory.createHeaderFactory().createToHeader(toAddress, toTag);
            // callid
            CallIdHeader callIdHeader = sipserver.getNewCallId();
            // Forwards
            MaxForwardsHeader maxForwards = sipFactory.createHeaderFactory().createMaxForwardsHeader(70);
            // ceq
            CSeqHeader cSeqHeader = sipFactory.createHeaderFactory().createCSeqHeader(1L, Request.MESSAGE);

            request = sipFactory.createMessageFactory().createRequest(requestURI, Request.MESSAGE, callIdHeader, cSeqHeader,
                    fromHeader, toHeader, viaHeaders, maxForwards);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            ContentTypeHeader contentTypeHeader = sipFactory.createHeaderFactory().createContentTypeHeader("APPLICATION",
                    "MANSCDP+xml");
            request.addHeader(userAgentHeader);
            request.setContent(content, contentTypeHeader);
        }
        return request;
    }

    /**
     * 创建Inviate请求头
     * @param device
     * @param channelId
     * @param content
     * @param viaTag
     * @param fromTag
     * @param toTag
     * @param ssrc
     * @return
     * @throws ParseException
     * @throws InvalidArgumentException
     * @throws PeerUnavailableException
     */
    public Request createInviteRequest(SipDevice device, String channelId, String content, String viaTag, String fromTag,
                                       String toTag, String ssrc) throws ParseException, InvalidArgumentException, PeerUnavailableException {
        Request request = null;
        // 请求行
        SipURI requestLine = sipFactory.createAddressFactory().createSipURI(channelId, device.getHostaddress());
        // via
        ArrayList<ViaHeader> viaHeaders = new ArrayList<ViaHeader>();
        ViaHeader viaHeader = sipFactory.createHeaderFactory().createViaHeader(device.getIp(), device.getPort(),
                device.getTransport(), viaTag);
        viaHeader.setRPort();
        viaHeaders.add(viaHeader);

        SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(device.getDeviceSipId());
        if (sipConfig != null) {
            // from
            SipURI fromSipURI = sipFactory.createAddressFactory().createSipURI(sipConfig.getServerSipid(),
                    sipConfig.getDomain());
            Address fromAddress = sipFactory.createAddressFactory().createAddress(fromSipURI);
            FromHeader fromHeader = sipFactory.createHeaderFactory().createFromHeader(fromAddress, fromTag); // 必须要有标记，否则无法创建会话，无法回应ack
            // to
            SipURI toSipURI = sipFactory.createAddressFactory().createSipURI(channelId, sipConfig.getDomain());
            Address toAddress = sipFactory.createAddressFactory().createAddress(toSipURI);
            //邀请播放的to头中不需要携带tag，携带了tag摄像头就回去找相关的对话，此时对话还没建立
            ToHeader toHeader = sipFactory.createHeaderFactory().createToHeader(toAddress, null);

            // callid
            CallIdHeader callIdHeader = sipserver.getNewCallId();

            // Forwards
            MaxForwardsHeader maxForwards = sipFactory.createHeaderFactory().createMaxForwardsHeader(70);

            // ceq
            CSeqHeader cSeqHeader = sipFactory.createHeaderFactory().createCSeqHeader(1L, Request.INVITE);
            request = sipFactory.createMessageFactory().createRequest(requestLine, Request.INVITE, callIdHeader, cSeqHeader,
                    fromHeader, toHeader, viaHeaders, maxForwards);

            Address concatAddress = sipFactory.createAddressFactory().createAddress(sipFactory.createAddressFactory()
                    .createSipURI(sipConfig.getServerSipid(), sipConfig.getIp() + ":" + sipConfig.getPort()));

            request.addHeader(sipFactory.createHeaderFactory().createContactHeader(concatAddress));
            // Subject
            SubjectHeader subjectHeader = sipFactory.createHeaderFactory()
                    .createSubjectHeader(String.format("%s:%s,%s:%s", channelId, ssrc, sipConfig.getIp(), 0));
            request.addHeader(subjectHeader);
            ContentTypeHeader contentTypeHeader = sipFactory.createHeaderFactory().createContentTypeHeader("APPLICATION",
                    "SDP");
            request.setContent(content, contentTypeHeader);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            request.addHeader(userAgentHeader);

        }
        return request;
    }

    /**
     * 创建回放Invite请求头
     * @param device
     * @param channelId
     * @param content
     * @param viaTag
     * @param fromTag
     * @param toTag
     * @return
     * @throws ParseException
     * @throws InvalidArgumentException
     * @throws PeerUnavailableException
     */
    public Request createPlaybackInviteRequest(SipDevice device, String channelId, String content, String viaTag,
                                               String fromTag, String toTag) throws ParseException, InvalidArgumentException, PeerUnavailableException {
        Request request = null;
        // 请求行
        SipURI requestLine = sipFactory.createAddressFactory().createSipURI(device.getDeviceSipId(),
                device.getHostaddress());
        // via
        ArrayList<ViaHeader> viaHeaders = new ArrayList<ViaHeader>();
        ViaHeader viaHeader = sipFactory.createHeaderFactory().createViaHeader(device.getIp(), device.getPort(),
                device.getTransport(), "z9hG4bK" + UUID.randomUUID().toString().replace("-", "").substring(0, 9));
        viaHeader.setRPort();
        viaHeaders.add(viaHeader);
        SipConfig sipConfig = sipConfigService.selectSipConfigBydeviceSipId(device.getDeviceSipId());
        if (sipConfig != null) {
            // from
            SipURI fromSipURI = sipFactory.createAddressFactory().createSipURI(sipConfig.getServerSipid(),
                    sipConfig.getDomain());
            Address fromAddress = sipFactory.createAddressFactory().createAddress(fromSipURI);
            FromHeader fromHeader = sipFactory.createHeaderFactory().createFromHeader(fromAddress, fromTag); // 必须要有标记，否则无法创建会话，无法回应ack
            // to
            SipURI toSipURI = sipFactory.createAddressFactory().createSipURI(channelId, sipConfig.getDomain());
            Address toAddress = sipFactory.createAddressFactory().createAddress(toSipURI);
            ToHeader toHeader = sipFactory.createHeaderFactory().createToHeader(toAddress, null);

            // callid
            CallIdHeader callIdHeader = sipserver.getNewCallId();

            // Forwards
            MaxForwardsHeader maxForwards = sipFactory.createHeaderFactory().createMaxForwardsHeader(70);

            // ceq
            CSeqHeader cSeqHeader = sipFactory.createHeaderFactory().createCSeqHeader(1L, Request.INVITE);
            request = sipFactory.createMessageFactory().createRequest(requestLine, Request.INVITE, callIdHeader, cSeqHeader,
                    fromHeader, toHeader, viaHeaders, maxForwards);

            Address concatAddress = sipFactory.createAddressFactory().createAddress(sipFactory.createAddressFactory()
                    .createSipURI(sipConfig.getServerSipid(), sipConfig.getIp() + ":" + sipConfig.getPort()));

            request.addHeader(sipFactory.createHeaderFactory().createContactHeader(concatAddress));

            ContentTypeHeader contentTypeHeader = sipFactory.createHeaderFactory().createContentTypeHeader("APPLICATION",
                    "SDP");
            request.setContent(content, contentTypeHeader);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            request.addHeader(userAgentHeader);
        }
        return request;
    }

    /**
     * 创建Info方法请求头，用于控制回播流，基于播放会话
     * @param dialogInfo
     * @param content
     * @return
     * @throws PeerUnavailableException
     * @throws ParseException
     * @throws InvalidArgumentException
     */
    public Request createInfoRequest(DialogInfo dialogInfo, String content)
            throws PeerUnavailableException, ParseException, InvalidArgumentException {
        //请求行的URI，sip:15030300001320000001@192.168.3.79:5060
        SipURI requestLine = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(),dialogInfo.getViaHost()+":"+dialogInfo.getViaPort());

        //Via头,via描述消息传递的通道，这个只能从200OK的via中获取receied，没有端口号，在GB中也不校验端口号
        ArrayList<ViaHeader> viaHeaders = new ArrayList();
        ViaHeader viaHeader = SipFactory.getInstance().createHeaderFactory().createViaHeader(dialogInfo.getViaReceied(),1,"UDP",SipUtil.getNewViaBranch());
        viaHeaders.add(viaHeader);

        //From: <sip:34020000002000000001@3402000000>;tag=live
        SipURI fromUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getFromUser(), dialogInfo.getFromHost());
        Address fromAddress = SipFactory.getInstance().createAddressFactory().createAddress(fromUri);
        FromHeader fromHeader = SipFactory.getInstance().createHeaderFactory().createFromHeader(fromAddress,dialogInfo.getFromTag());

        //To: <sip:15030300001320000001@3402000000>;tag=2123864151
        SipURI toUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(), dialogInfo.getToHost());
        Address toAddress = SipFactory.getInstance().createAddressFactory().createAddress(toUri);
        ToHeader toHeader = SipFactory.getInstance().createHeaderFactory().createToHeader(toAddress, dialogInfo.getToTag());

        //Call-ID: 003de3460eecbc1924d04fc47cd5c1e7@192.168.3.85
        CallIdHeader callIdHeader = SipFactory.getInstance().createHeaderFactory().createCallIdHeader(dialogInfo.getCallIdText());

        // Forwards
        MaxForwardsHeader maxForwards = sipFactory.createHeaderFactory().createMaxForwardsHeader(70);

        //CSeq: 2 INFO
        CSeqHeader cSeqHeader = SipFactory.getInstance().createHeaderFactory().createCSeqHeader(sipCacheService.getCSEQ(dialogInfo.getFromUser()), Request.INFO);

        Request request = sipFactory.createMessageFactory().createRequest(requestLine, Request.INFO, callIdHeader, cSeqHeader,
                fromHeader, toHeader, viaHeaders, maxForwards);

//        Address concatAddress = sipFactory.createAddressFactory().createAddress(sipFactory.createAddressFactory()
//                .createSipURI(sipConfig.getServerSipid(), sipConfig.getIp() + ":" + sipConfig.getPort()));
//        request.addHeader(sipFactory.createHeaderFactory().createContactHeader(concatAddress));

        ContentTypeHeader contentTypeHeader = sipFactory.createHeaderFactory().createContentTypeHeader("Application",
                "MANSRTSP");
        request.setContent(content, contentTypeHeader);

        List<String> userAgents = new ArrayList<>();
        userAgents.add("Frog");
        UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
        request.addHeader(userAgentHeader);

        return request;
    }

    /**
     * 创建Bye请求头，用于结束播放，基于播放会话
     * @param dialogInfo
     * @return
     */
    public Request createByeRequest( DialogInfo dialogInfo) {
        try {
            //请求行的URI，sip:15030300001320000001@192.168.3.79:5060
            SipURI requestLine = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(),dialogInfo.getViaHost()+":"+dialogInfo.getViaPort());

            //Via头,via描述消息传递的通道，这个只能从200OK的via中获取receied，没有端口号，在GB中也不校验端口号
            ArrayList<ViaHeader> viaHeaders = new ArrayList();
            ViaHeader viaHeader = SipFactory.getInstance().createHeaderFactory().createViaHeader(dialogInfo.getViaReceied(),1,"UDP",SipUtil.getNewViaBranch());
            viaHeaders.add(viaHeader);

            //From: <sip:34020000002000000001@3402000000>;tag=live
            SipURI fromUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getFromUser(), dialogInfo.getFromHost());
            Address fromAddress = SipFactory.getInstance().createAddressFactory().createAddress(fromUri);
            FromHeader fromHeader = SipFactory.getInstance().createHeaderFactory().createFromHeader(fromAddress,dialogInfo.getFromTag());

            //To: <sip:15030300001320000001@3402000000>;tag=2123864151
            SipURI toUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(), dialogInfo.getToHost());
            Address toAddress = SipFactory.getInstance().createAddressFactory().createAddress(toUri);
            ToHeader toHeader = SipFactory.getInstance().createHeaderFactory().createToHeader(toAddress, dialogInfo.getToTag());

            //Max-Forwards: 70
            MaxForwardsHeader maxForwards = SipFactory.getInstance().createHeaderFactory().createMaxForwardsHeader(70);

            //CSeq: 2 BYE
            CSeqHeader cSeqHeader = SipFactory.getInstance().createHeaderFactory().createCSeqHeader(sipCacheService.getCSEQ(dialogInfo.getFromUser()), Request.BYE);

            //Call-ID: 003de3460eecbc1924d04fc47cd5c1e7@192.168.3.85
            CallIdHeader callIdHeader = SipFactory.getInstance().createHeaderFactory().createCallIdHeader(dialogInfo.getCallIdText());
            //构建request
            Request request = SipFactory.getInstance().createMessageFactory().createRequest(requestLine, Request.BYE, callIdHeader, cSeqHeader, fromHeader, toHeader, viaHeaders, maxForwards);

            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            request.addHeader(userAgentHeader);

            return request;
        } catch (ParseException | InvalidArgumentException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建ACK确认头，基于播放会话
     * @param dialogInfo
     * @return
     */
    public Request createAkcRequest(DialogInfo dialogInfo){
        try {
            //请求行的URI，sip:15030300001320000001@192.168.3.79:5060
            SipURI requestLine = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(),dialogInfo.getViaHost()+":"+dialogInfo.getViaPort());

            //Via头,via描述消息传递的通道，这个只能从200OK的via中获取receied，没有端口号，在GB中也不校验端口号
            ArrayList<ViaHeader> viaHeaders = new ArrayList();
            ViaHeader viaHeader = SipFactory.getInstance().createHeaderFactory().createViaHeader(dialogInfo.getViaReceied(),1,"UDP",SipUtil.getNewViaBranch());
            viaHeaders.add(viaHeader);

            //From: <sip:34020000002000000001@3402000000>;tag=live
            SipURI fromUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getFromUser(), dialogInfo.getFromHost());
            Address fromAddress = SipFactory.getInstance().createAddressFactory().createAddress(fromUri);
            FromHeader fromHeader = SipFactory.getInstance().createHeaderFactory().createFromHeader(fromAddress,dialogInfo.getFromTag());

            //To: <sip:15030300001320000001@3402000000>;tag=2123864151
            SipURI toUri = SipFactory.getInstance().createAddressFactory().createSipURI(dialogInfo.getToUser(), dialogInfo.getToHost());
            Address toAddress = SipFactory.getInstance().createAddressFactory().createAddress(toUri);
            ToHeader toHeader = SipFactory.getInstance().createHeaderFactory().createToHeader(toAddress, dialogInfo.getToTag());

            //Call-ID: 003de3460eecbc1924d04fc47cd5c1e7@192.168.3.85
            CallIdHeader callIdHeader = SipFactory.getInstance().createHeaderFactory().createCallIdHeader(dialogInfo.getCallIdText());

            //CSeq: 2 ACK
            //CSeq: 2 BYE
            CSeqHeader cSeqHeader = SipFactory.getInstance().createHeaderFactory().createCSeqHeader(sipCacheService.getCSEQ(dialogInfo.getFromUser()), Request.ACK);

            //Max-Forwards: 70
            MaxForwardsHeader maxForwards = SipFactory.getInstance().createHeaderFactory().createMaxForwardsHeader(70);

            Request request = SipFactory.getInstance().createMessageFactory().createRequest(requestLine, Request.ACK, callIdHeader, cSeqHeader, fromHeader, toHeader, viaHeaders, maxForwards);

            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            request.addHeader(userAgentHeader);

            return request;
        } catch (ParseException | InvalidArgumentException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建200OK回复头
     * @param req
     * @return
     */
    public Response createOkResponse(Request req)  {
        try {
            Response response = SipFactory.getInstance().createMessageFactory().createResponse(Response.OK, req);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            response.addHeader(userAgentHeader);
            return response;
        } catch (ParseException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建UNAUTHORIZED回复头
     * @param req
     * @return
     */
    public Response createRegisterUnauthorize(Request req)  {
        try {
            Response response = SipFactory.getInstance().createMessageFactory().createResponse(Response.UNAUTHORIZED, req);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            response.addHeader(userAgentHeader);
            return response;
        } catch (ParseException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建FORBIDDEN回复头
     * @param req
     * @return
     */
    public Response createRegisterForbidden(Request req)  {
        try {
            Response response = SipFactory.getInstance().createMessageFactory().createResponse(Response.FORBIDDEN, req);
            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            response.addHeader(userAgentHeader);
            return response;
        } catch (ParseException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建RegisterOK回复头
     * @param req
     * @return
     */
    public Response createRegisterOk(Request req)  {
        try {
            Response response = SipFactory.getInstance().createMessageFactory().createResponse(Response.OK, req);

            // 添加date头
            SIPDateHeader dateHeader = new SIPDateHeader();
            // 使用自己修改的
            SipDate sipDate = new SipDate(Calendar.getInstance(Locale.ENGLISH).getTimeInMillis());
            dateHeader.setDate(sipDate);
            response.addHeader(dateHeader);

            // 添加Contact头
            response.addHeader(req.getHeader(ContactHeader.NAME));
            // 添加Expires头
            response.addHeader(req.getExpires());

            List<String> userAgents = new ArrayList<>();
            userAgents.add("Frog");
            UserAgentHeader userAgentHeader = SipFactory.getInstance().createHeaderFactory().createUserAgentHeader(userAgents);
            response.addHeader(userAgentHeader);
            return response;
        } catch (ParseException | PeerUnavailableException e) {
            throw new RuntimeException(e);
        }
    }

}
