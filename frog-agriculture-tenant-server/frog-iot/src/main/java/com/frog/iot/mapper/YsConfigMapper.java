package com.frog.iot.mapper;

import java.util.List;
import com.frog.iot.domain.YsConfig;

/**
 * 萤石系统配置Mapper接口
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
public interface YsConfigMapper 
{
    /**
     * 查询萤石系统配置
     * 
     * @param id 萤石系统配置主键
     * @return 萤石系统配置
     */
    public YsConfig selectYsConfigByProductId(Long id);

    /**
     * 新增萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    public int insertYsConfig(YsConfig ysConfig);

    /**
     * 修改萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    public int updateYsConfig(YsConfig ysConfig);
}
