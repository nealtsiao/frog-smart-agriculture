package com.frog.sip.server.handler.handleResponse;

import com.frog.sip.server.handler.IResHandler;
import com.frog.sip.server.listener.IGBListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.ResponseEvent;
import java.text.ParseException;

import static com.frog.sip.server.enums.SipResponseCode.TRYING;

/**
 * @author neal
 * @date 2024/12/18  18:05
 */
@Component
@Slf4j
public class TryingInviteHandler implements InitializingBean, IResHandler {
    @Autowired
    IGBListener sipListener;
    @Override
    public void processMsg(ResponseEvent evt) throws ParseException {
        log.info(evt.getResponse().toString());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = TRYING.getName()+"INVITE";
        sipListener.addResponseProcessor(method,this);
    }
}
