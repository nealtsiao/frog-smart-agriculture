package com.frog.sip.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.frog.sip.domain.MediaServer;
import com.frog.sip.server.cmd.ISipCmd;
import com.frog.sip.server.model.Stream;
import com.frog.sip.service.IMediaServerService;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.IZmlHookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
public class ZmlHookServiceImpl implements IZmlHookService {

    @Autowired
    private ISipCacheService sipCacheService;

    @Autowired
    private IMediaServerService mediaServerService;

    @Autowired
    private ISipCmd sipCmd;
    @Override
    public String onHttpAccess(JSONObject json) {
        return null;
    }

    @Override
    public String onPlay(JSONObject json) {
        return null;
    }

    @Override
    public String onPublish(JSONObject json) {

        return null;
    }

    @Override
    public String onStreamNoneReader(JSONObject json) {
        if(json!=null) {
            String streamId = json.getString("stream");
            String[] s = streamId.split("_");
            if(s.length==4){
                sipCmd.streamByeCmd(streamId);
            }
        }
        return null;
    }

    @Override
    public String onStreamNotFound(JSONObject json) {
        return null;
    }

    @Override
    public String onStreamChanged(JSONObject json) {
        return null;
    }

    /**
     * 更新流
     * @param streamInfo redis缓存中的流的实例
     * @return 当前播放流的信息
     */
    public Stream updateStream(Stream streamInfo) {
        String streamId = streamInfo.getStreamId();
        MediaServer mediaServer = mediaServerService.getMediaInfo();
        streamInfo.setWs_flv(String.format("ws://%s:%s/rtp/%s.live.flv", mediaServer.getDomain(), mediaServer.getPortHttp(), streamId));
        streamInfo.setWs_ts(String.format("ws://%s:%s/rtp/%s.live.ts", mediaServer.getDomain(), mediaServer.getPortHttp(), streamId));
        streamInfo.setRtmp(String.format("rtmp://%s:%s/rtp/%s", mediaServer.getDomain(), mediaServer.getPortRtmp(), streamId));
        streamInfo.setRtsp(String.format("rtsp://%s:%s/rtp/%s", mediaServer.getDomain(), mediaServer.getPortRtsp(), streamId));

        if (Objects.equals(mediaServer.getProtocol(), "http")) {
            streamInfo.setFlv(String.format("http://%s:%s/rtp/%s.live.flv", mediaServer.getDomain(),
                    mediaServer.getPortHttp(), streamId));
            streamInfo.setTs(String.format("http://%s:%s/rtp/%s.live.ts", mediaServer.getDomain(),
                    mediaServer.getPortHttp(), streamId));
            streamInfo.setPlayurl(streamInfo.getFlv());
        } else if (Objects.equals(mediaServer.getProtocol(), "https")) {
            streamInfo.setFlv(String.format("https://%s:%s/rtp/%s.live.flv", mediaServer.getDomain(),mediaServer.getPortHttps(), streamId));
            streamInfo.setTs(String.format("https://%s:%s/rtp/%s.live.ts", mediaServer.getDomain(),mediaServer.getPortHttps(), streamId));
            streamInfo.setPlayurl(streamInfo.getFlv());
        } else if (Objects.equals(mediaServer.getProtocol(), "ws")) {
            streamInfo.setPlayurl(streamInfo.getWs_flv());
        } else if (Objects.equals(mediaServer.getProtocol(), "rtmp")) {
            streamInfo.setPlayurl(streamInfo.getRtmp());
        } else if (Objects.equals(mediaServer.getProtocol(), "rtsp")) {
            streamInfo.setPlayurl(streamInfo.getRtsp());
        }
        //缓存流信息
        sipCacheService.addStream(streamInfo);
        return streamInfo;
    }
}
