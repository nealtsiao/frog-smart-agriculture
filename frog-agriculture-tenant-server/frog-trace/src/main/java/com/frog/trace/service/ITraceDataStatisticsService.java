package com.frog.trace.service;

import com.frog.trace.domain.TraceRecord;

import java.util.HashMap;
import java.util.List;

public interface ITraceDataStatisticsService {
    public List<HashMap> selectRecordGroupByMonth(Long BaseId);
    public List<HashMap> selectRecordGroupByCity(Long baseId);
    public List<HashMap> selectRecordStatistics(Long baseId);
    public List<HashMap> selectRecordGroupBySellpro(Long baseId);
    public List<HashMap> selectRecord(Long baseId);
    //溯源报表功能上面的六个数据
    public List<HashMap> selectTraceInfo(Long baseId);

}
