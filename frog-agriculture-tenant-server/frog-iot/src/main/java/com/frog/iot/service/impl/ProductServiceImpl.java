package com.frog.iot.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.frog.common.annotation.TenantScope;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.domain.entity.SysRole;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.redis.RedisCache;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.SecurityUtils;
import com.frog.iot.domain.Device;
import com.frog.iot.domain.Product;
import com.frog.iot.domain.ProductAuthorize;
import com.frog.iot.mapper.DeviceMapper;
import com.frog.iot.mapper.ProductAuthorizeMapper;
import com.frog.iot.mapper.ProductMapper;
import com.frog.iot.model.ChangeProductStatusModel;
import com.frog.iot.model.DeviceShortOutput;
import com.frog.iot.model.IdAndName;
import com.frog.iot.model.ThingsModelItem.DataType;
import com.frog.iot.model.ThingsModelItem.ThingsModel;
import com.frog.iot.model.ThingsModels.ThingsModelSimpleItem;
import com.frog.iot.service.IAlertService;
import com.frog.iot.service.IFirmwareService;
import com.frog.iot.service.IProductService;
import com.frog.iot.service.IThingsModelService;
import com.frog.iot.util.AESUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import static com.frog.common.utils.SecurityUtils.getLoginUser;

/**
 * 产品Service业务层处理
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Service
public class ProductServiceImpl implements IProductService
{
    private String tslPreKey ="TSL:";

    // 物模型值命名空间：Key：TSLV:{productId}_{deviceNumber}   HKey:{identity#V/identity#S/identity#M/identity#N}
    private String devicePreKey = "TSLV:";

    @Autowired
    private ThingsModelServiceImpl thingsModelService;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductAuthorizeMapper productAuthorizeMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ToolServiceImpl toolService;

    @Autowired
    private DeviceMapper deviceMapper;

    @Autowired
    @Lazy
    private DeviceServiceImpl deviceService;
    @Autowired
    private IFirmwareService firmwareService;
    @Autowired
    private IAlertService alertService;

    /**
     * 查询产品
     * 
     * @param productId 产品主键
     * @return 产品
     */
    @Override
    public Product selectProductByProductId(Long productId)
    {
        return productMapper.selectProductByProductId(productId);
    }

    /**
     * 查询产品列表
     * 
     * @param product 产品
     * @return 产品
     */
    @Override
    @TenantScope
    public List<Product> selectProductList(Product product)
    {
        return productMapper.selectProductList(product);
    }

    /**
     * 查询产品简短列表
     *
     * @return 产品
     */
    @Override
    @TenantScope(isContainAdmin = true)
    public List<IdAndName> selectProductShortList()
    {
        Product product =new Product();
        return productMapper.selectProductShortList(product);
    }

    /**
     * 新增产品
     * 
     * @param product 产品
     * @return 结果
     */
    @Override
    public Product insertProduct(Product product)
    {
        SysUser user = getLoginUser().getUser();
        if(user.isAdmin()){
            product.setIsSys(1);
        }else{
            product.setIsSys(0);
        }
        product.setTenantId(SecurityUtils.getTenantId());
        product.setBaseId(SecurityUtils.getBaseId());
        product.setDeptId(SecurityUtils.getDeptId());
        product.setUserId(SecurityUtils.getUserId());
        // mqtt账号密码
        if(product.getMqttAccount()==null || product.getMqttAccount().equals("")){
            product.setMqttAccount("Frog");
        }
        if(product.getMqttPassword()==null || product.getMqttPassword().equals("")){
            product.setMqttPassword("P"+toolService.getStringRandom(15));
        }
        if(product.getMqttSecret()==null || product.getMqttSecret().equals("")){
            product.setMqttSecret("K"+toolService.getStringRandom(15));
        }
        product.setStatus(product.getStatus()==null?1:product.getStatus());
        product.setCreateBy(user.getUserId().toString());
        product.setCreateTime(DateUtils.getNowDate());
        productMapper.insertProduct(product);
        return product;
    }

    /**
     * 修改产品
     * 
     * @param product 产品
     * @return 结果
     */
    @Override
    @Transactional
    public int updateProduct(Product product)
    {
        //同步修一下设备那边的图片
        Device device = new Device();
        device.setProductId(product.getProductId());
        List<DeviceShortOutput> devicetList = deviceMapper.selectDeviceList(device);
        if(devicetList!=null && !devicetList.isEmpty()){
            for(DeviceShortOutput d : devicetList){
                if(!d.getImgUrl().equals(product.getImgUrl())){
                    //只修改imgUrl
                    Device updateDevice = new Device();
                    updateDevice.setDeviceId(d.getDeviceId());
                    updateDevice.setImgUrl(product.getImgUrl());
                    deviceMapper.updateDevice(updateDevice);
                }
            }
        }
        //修改产品
        product.setUpdateTime(DateUtils.getNowDate());
        return productMapper.updateProduct(product);
    }

    /**
     * 获取产品下面的设备数量
     *
     * @param productId 产品ID
     * @return 结果
     */
    @Override
    public int selectDeviceCountByProductId(Long productId)
    {
        return deviceMapper.selectDeviceCountByProductId(productId);
    }

    /**
     * 更新产品状态,1-未发布，2-已发布
     *
     * @param model
     * @return 结果
     */
    @Override
    @Transactional
    public AjaxResult changeProductStatus(ChangeProductStatusModel model)
    {
        //getDeviceType 1直连设置 2网管设备 3国标摄像头 4萤石云摄像头
        // status 1 未发布 2已发布
        if(model.getStatus()!=1 && model.getStatus()!=2){
            return AjaxResult.error("状态更新失败,状态值有误");
        }
        if(model.getStatus()==2 && (model.getDeviceType()==1 || model.getDeviceType()==2)){
            // 产品下必须包含物模型(不包含监控产品,deviceType==3)
            int thingsCount=productMapper.thingsCountInProduct(model.getProductId());
            if(thingsCount==0){
                return AjaxResult.error("发布失败，请先添加产品的物模型");
            }
            // 产品下物模型的标识符必须唯一
            int repeatCount=productMapper.thingsRepeatCountInProduct(model.getProductId());
            if(repeatCount>1){
                return AjaxResult.error("发布失败，产品物模型的标识符必须唯一");
            }
            // 批量更新产品下所有设备的物模型值
            updateDeviceStatusByProductIdAsync(model.getProductId());
        }
        if(productMapper.changeProductStatus(model)>0){
            return AjaxResult.success("操作成功");
        }
        return AjaxResult.error("状态更新失败");
    }

    /***
     * 更新产品下所有设备的物模型值
     * @param productId
     */
    public void updateDeviceStatusByProductIdAsync(Long productId){
        List<String> deviceNumbers=deviceMapper.selectSerialNumberByProductId(productId);
        deviceNumbers.forEach(x->{
            // 缓存新的物模型值
            deviceService.cacheDeviceStatus(productId,x);
        });
    }

    /**
     * 批量删除产品
     * 
     * @param productIds 需要删除的产品主键
     * @return 结果
     */
    @Override
    @Transactional
    public AjaxResult deleteProductByProductIds(Long[] productIds)
    {
        // 删除物模型JSON缓存
        for(int i=0;i<productIds.length;i++){
            redisCache.deleteObject(tslPreKey+productIds[i]);
        }
        // 产品下不能有设备
        int deviceCount=productMapper.deviceCountInProducts(productIds);
        if(deviceCount>0){
            return AjaxResult.error("删除失败，请先删除对应产品下的设备");
        }
        //删除产品固件
        firmwareService.deleteFirmwareByProductIds(productIds);
        // 删除产品物模型
        productMapper.deleteProductThingsModelByProductIds(productIds);
        // 删除产品的授权码
        productAuthorizeMapper.deleteProductAuthorizeByProductIds(productIds);
        // 删除告警
        alertService.deleteAlertByProductIds(productIds);
        // 删除产品
        if(productMapper.deleteProductByProductIds(productIds)>0){
            return AjaxResult.success("删除成功");
        }

        return AjaxResult.error("删除失败");
    }


    /**
     * 删除产品信息
     * 
     * @param productId 产品主键
     * @return 结果
     */
    @Override
    public int deleteProductByProductId(Long productId)
    {
        // 删除物模型JSON缓存
        redisCache.deleteObject(tslPreKey+productId);
        return productMapper.deleteProductByProductId(productId);
    }

    @Override
    public String generateAesKey(Long productId,Long authorizeId){
        Product product = productMapper.selectProductByProductId(productId);
        //获取明文密码
        String password = product.getMqttPassword();
        //获取加密密钥
        String secret = product.getMqttSecret();
        //获取三个小时之后的时间戳（毫秒）
        String timeStamp =String.valueOf(System.currentTimeMillis()+3*60*60*1000) ;
        String plainText="";
        if(authorizeId==0){
            plainText=password + "&" + timeStamp;
        }
        if(authorizeId!=0){
            ProductAuthorize productAuthorize = productAuthorizeMapper.selectProductAuthorizeByAuthorizeId(authorizeId);
            plainText=password + "&" + timeStamp+"&"+ productAuthorize.getAuthorizeCode();
        }
        String encrypt = AESUtils.encrypt(plainText, secret);
        return  encrypt;
    }
}
