package com.frog.sip.server.util;

import com.frog.sip.server.enums.PTZCmd;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PtzCmdUtil {
    public static String cmdString(int leftRight, int upDown, int inOut, int moveSpeed, int scaleSpeed) {
        int speed1 = 0;
        int speed2 = 0;
        int cmdCode = 0;
        if (leftRight == 1) {
            cmdCode = PTZCmd.right.getValue();		// 右移
            speed1 = moveSpeed;
        } else if(leftRight == 2) {
            cmdCode = PTZCmd.left.getValue();		// 左移
            speed1 = moveSpeed;
        }
        if (upDown == 1) {
            cmdCode = PTZCmd.up.getValue();		// 上移
            speed2 = moveSpeed;
        } else if(upDown == 2) {
            cmdCode = PTZCmd.down.getValue();		// 下移
            speed2 = moveSpeed;
        }
        if (inOut == 1) {
            cmdCode = PTZCmd.in.getValue();	// 放大
        } else if(inOut == 2) {
            cmdCode = PTZCmd.out.getValue();	// 缩小
        }
        StringBuilder builder = new StringBuilder("A50F01");
        String strTmp;
        strTmp = String.format("%02X", cmdCode);
        builder.append(strTmp, 0, 2);

        //字节5
        strTmp = String.format("%02X", speed1);
        builder.append(strTmp, 0, 2);
        //字节6
        strTmp = String.format("%02X", speed2);
        builder.append(strTmp, 0, 2);
        //字节7高4位
        strTmp = String.format("%X", scaleSpeed);
        builder.append(strTmp, 0, 1).append("0");

        //计算校验码
        int checkCode = (0XA5 + 0X0F + 0X01 + cmdCode + speed1 + speed2 + (scaleSpeed /*<< 4*/ & 0XF0)) % 0X100;
        strTmp = String.format("%02X", checkCode);
        builder.append(strTmp, 0, 2);
        return builder.toString();
    }

}
