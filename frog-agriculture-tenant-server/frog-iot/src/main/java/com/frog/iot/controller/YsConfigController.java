package com.frog.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.enums.BusinessType;
import com.frog.iot.domain.YsConfig;
import com.frog.iot.service.IYsConfigService;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.common.core.page.TableDataInfo;

/**
 * 萤石系统配置Controller
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
@RestController
@RequestMapping("/iot/ys")
@Api(tags = "iot-萤石云配置")
public class YsConfigController extends BaseController
{
    @Autowired
    private IYsConfigService ysConfigService;

    /**
     * 获取萤石系统配置详细信息
     */
    @ApiOperation("获取萤石系统配置详细信息")
    @PreAuthorize("@ss.hasPermi('iot:ys:query')")
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId)
    {
        return AjaxResult.success(ysConfigService.selectYsConfigByProductId(productId));
    }

    /**
     * 新增萤石系统配置
     */
    @ApiOperation("新增萤石系统配置")
    @PreAuthorize("@ss.hasPermi('iot:ys:add')")
    @Log(title = "萤石系统配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YsConfig ysConfig)
    {
        return toAjax(ysConfigService.insertYsConfig(ysConfig));
    }

    /**
     * 修改萤石系统配置
     */
    @ApiOperation("修改萤石系统配置")
    @PreAuthorize("@ss.hasPermi('iot:ys:edit')")
    @Log(title = "萤石系统配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YsConfig ysConfig)
    {
        return toAjax(ysConfigService.updateYsConfig(ysConfig));
    }

    /**
     * 修改萤石系统配置
     */
    @ApiOperation("获取萤石云设备分页列表")
    @PreAuthorize("@ss.hasPermi('iot:ys:list')")
    @PostMapping(value="/getDeviceList",produces = "application/json;charset=UTF-8")
    public AjaxResult getDeviceList(@RequestBody JSONObject jsonObject) {
        Long productId = jsonObject.getLong("productId");
        Integer pageSize = jsonObject.getInteger("pageSize");
        Integer pageStart = jsonObject.getInteger("pageStart");
        JSONObject data = ysConfigService.getDeviceList(productId, pageSize, pageStart);
        return AjaxResult.success(data);
    }
}
