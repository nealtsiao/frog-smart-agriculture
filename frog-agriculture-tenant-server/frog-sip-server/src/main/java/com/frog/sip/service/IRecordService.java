package com.frog.sip.service;

import com.frog.sip.server.model.RecordItem;
import com.frog.sip.server.model.RecordList;

import java.util.List;

public interface IRecordService {
    RecordList listDevRecord(String deviceId, String channelId, String startTime, String endTime);
}
