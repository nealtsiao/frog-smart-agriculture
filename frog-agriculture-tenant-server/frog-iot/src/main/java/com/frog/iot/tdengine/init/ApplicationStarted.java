package com.frog.iot.tdengine.init;


import com.alibaba.druid.pool.DruidDataSource;
import com.frog.iot.tdengine.config.TDengineConfig;
import com.frog.iot.tdengine.dao.TDDeviceLogDAO;
import com.taosdata.jdbc.TSDBDriver;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * 类名: ApplicationStarted
 * 时间: 2022/5/18,0018 1:41
 * 开发人: wxy
 */
@Component
@Slf4j
@Order(2)
public class ApplicationStarted implements ApplicationRunner {

    @Autowired
    private ApplicationContext applicationContext;

    private TDengineConfig dengineConfig;

    private TDDeviceLogDAO deviceLogMapper;

    @Override
    public void run(ApplicationArguments args) {

        //先获取TDengine的配置，检测TDengine是否已经配置
        if (containBean(TDengineConfig.class)) {
            this.dengineConfig = applicationContext.getBean(TDengineConfig.class);
            this.deviceLogMapper= applicationContext.getBean(TDDeviceLogDAO.class);
            log.info("[3-启用TDENGINE数据库，正在初始化数据库和超级表]");
            initTDengine(this.dengineConfig.getDbName());
        }
    }

    /**
     * @return
     * @Method
     * @Description 开始初始化加载系统参数, 创建数据库和超级表
     * @Param null
     * @date 2022/5/22,0022 14:27
     * @author wxy
     */
    public void initTDengine(String dbName) {
        try {
            //创建数据库
            deviceLogMapper.createDB(dbName);
            log.info("[4-TDENGINE数据库创建成功]");
            //创建数据库表
            deviceLogMapper.createSTable(dbName);
            log.info("[5-TDENGINE超级表创建成功]");
        } catch (Exception e) {
            log.error("错误",e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * @return
     * @Method containBean
     * @Description 根据类判断是否有对应bean
     * @Param 类
     * @date 2022/5/22,0022 14:12
     * @author wxy
     */
    private boolean containBean(@Nullable Class<?> T) {
        String[] beans = applicationContext.getBeanNamesForType(T);
        if (beans == null || beans.length == 0) {
            return false;
        } else {
            return true;
        }
    }


}
