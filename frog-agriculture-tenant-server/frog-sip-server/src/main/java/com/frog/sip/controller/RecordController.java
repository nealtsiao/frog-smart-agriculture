package com.frog.sip.controller;

import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.sip.server.model.RecordList;
import com.frog.sip.service.IRecordService;
import com.frog.sip.server.util.WebAsyncUtil;
import com.frog.sip.server.util.result.BaseResult;
import com.frog.sip.server.util.result.CodeEnum;
import com.frog.sip.server.util.result.DataResult;
import com.frog.sip.service.impl.camera.CameraFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;

@Slf4j
@RestController
@RequestMapping("/sip/record")
@Api(tags = "sip-录像记录")
public class RecordController extends BaseController {

    @Autowired
    private CameraFactory cameraFactory;

    @Qualifier("sipTask")
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    /**
     * 查找录像记录
     * @param deviceId
     * @param channelId
     * @param start
     * @param end
     * @return
     */
    @ApiOperation("查找录像记录")
    @PreAuthorize("@ss.hasPermi('sip:record:list')")
    @GetMapping("/devquery/{deviceId}/{channelId}")
    public WebAsyncTask<Object> devquery(@PathVariable String deviceId,
                                     @PathVariable String channelId, String start, String end)
    {
        return WebAsyncUtil.init(taskExecutor, () -> {
            try {
                RecordList result = cameraFactory.createRecord(deviceId).listDevRecord(deviceId,channelId,start,end);
                return DataResult.out(CodeEnum.SUCCESS, result);
            } catch (Exception e) {
                log.error("", e);
                return BaseResult.out(CodeEnum.FAIL, e.getMessage());
            }
        });
    }

}
