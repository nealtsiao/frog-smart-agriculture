package com.frog.view.service;

import java.util.List;

import com.frog.view.domain.GoViewPrivateImages;

/**
 * goview用户私有图片Service接口
 *
 * @author nealtsiao
 * @date 2024-11-25
 */
public interface IGoViewPrivateImagesService {
    /**
     * 查询goview用户私有图片
     *
     * @param id goview用户私有图片主键
     * @return goview用户私有图片
     */
    public GoViewPrivateImages selectGoViewPrivateImagesById(Long id);

    /**
     * 查询goview用户私有图片列表
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return goview用户私有图片集合
     */
    public List<GoViewPrivateImages> selectGoViewPrivateImagesList(GoViewPrivateImages goViewPrivateImages);

    /**
     * 新增goview用户私有图片
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    public int insertGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages);

    /**
     * 修改goview用户私有图片
     *
     * @param goViewPrivateImages goview用户私有图片
     * @return 结果
     */
    public int updateGoViewPrivateImages(GoViewPrivateImages goViewPrivateImages);

    /**
     * 批量删除goview用户私有图片
     *
     * @param ids 需要删除的goview用户私有图片主键集合
     * @return 结果
     */
    public int deleteGoViewPrivateImagesByIds(Long[] ids);

    /**
     * 删除goview用户私有图片信息
     *
     * @param id goview用户私有图片主键
     * @return 结果
     */
    public int deleteGoViewPrivateImagesById(Long id);
}
