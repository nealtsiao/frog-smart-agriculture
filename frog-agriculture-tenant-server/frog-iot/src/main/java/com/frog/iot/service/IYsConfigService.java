package com.frog.iot.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.frog.iot.domain.YsConfig;

/**
 * 萤石系统配置Service接口
 * 
 * @author nealtsiao
 * @date 2024-11-21
 */
public interface IYsConfigService 
{
    /**
     * 查询萤石系统配置
     * 
     * @param id 萤石系统配置主键
     * @return 萤石系统配置
     */
    public YsConfig selectYsConfigByProductId(Long id);

    /**
     * 新增萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    public int insertYsConfig(YsConfig ysConfig);

    /**
     * 修改萤石系统配置
     * 
     * @param ysConfig 萤石系统配置
     * @return 结果
     */
    public int updateYsConfig(YsConfig ysConfig);


    /**
     * 获取萤石云设备分页列表
     * @param ProductId
     * @return
     */
    public JSONObject getDeviceList(Long ProductId,int pageSize,int pageStart);
}
