package com.frog.sip.server.handler.handleResponse;

import com.frog.sip.server.handler.IResHandler;
import com.frog.sip.server.listener.IGBListener;
import com.frog.sip.server.model.DialogInfo;
import com.frog.sip.server.transcation.HeaderBuilder;
import com.frog.sip.server.transcation.SipSender;
import com.frog.sip.server.util.StreamIdUtil;
import com.frog.sip.service.ISipCacheService;
import com.frog.sip.service.ISipDeviceChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.ResponseEvent;
import javax.sip.SipException;
import javax.sip.message.Request;
import java.text.ParseException;

import static com.frog.sip.server.enums.SipResponseCode.OK;
import static com.frog.sip.server.util.SipUtil.getSsrcFromSdp;

@Slf4j
@Component
public class OKInviteResHandler implements InitializingBean, IResHandler {

    @Autowired
    private IGBListener sipListener;
    @Autowired
    private ISipDeviceChannelService sipDeviceChannelService;
    @Autowired
    private ISipCacheService sipCacheService;
    @Autowired
    HeaderBuilder headerBuilder;
    @Autowired
    SipSender sipSender;
    @Autowired
    private StreamIdUtil streamIdUtil;

    @Override
    public void processMsg(ResponseEvent evt) throws ParseException {
        try {
            log.info(evt.getResponse().toString());
            //获取sipChannelId
            String streamId = streamIdUtil.getStreamIdFromResponseEvent(evt);
            //获取ssrc
            String s = new String(evt.getResponse().getRawContent());
            String ssrc = getSsrcFromSdp(s);
            //存储dialog
            DialogInfo dialogInfo = new DialogInfo(evt.getResponse(), streamId, ssrc);
            sipCacheService.addDialog(dialogInfo);
            //回复
            Request akcRequest = headerBuilder.createAkcRequest(dialogInfo);
            sipSender.transmit(akcRequest);
        } catch (SipException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String method = OK.getName()+"INVITE";
        sipListener.addResponseProcessor(method,this);
    }
}
