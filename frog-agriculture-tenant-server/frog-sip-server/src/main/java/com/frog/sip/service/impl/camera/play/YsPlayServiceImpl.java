package com.frog.sip.service.impl.camera.play;

import com.alibaba.fastjson.JSON;
import com.frog.common.utils.SecurityUtils;
import com.frog.iot.util.TimeStampUtils;
import com.frog.iot.util.YsApiUtils;
import com.frog.sip.server.model.Stream;
import com.frog.sip.service.IPlayService;
import com.frog.sip.service.ISipCacheService;
import com.sun.jna.platform.win32.Sspi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * @author neal
 * @date 2024/11/30  16:03
 */
@Service("ysPlay")
public class YsPlayServiceImpl implements IPlayService {

    @Autowired
    private YsApiUtils ysApiUtils;
    @Autowired
    private ISipCacheService sipCacheService;

    /**
     * 开始播放，也是返回播放地址
     *
     * @param deviceId
     * @param channelId
     * @return
     */
    @Override
    public Stream play(String deviceId, String channelId) {
        String channelNo = channelId.split("-")[1];
        String playUrl = ysApiUtils.getPlayUrl(deviceId, channelNo, "4");
        Stream stream = new Stream();
        stream.setPlayurl(playUrl);
        stream.setStreamId(channelId);
        return stream;
    }

    @Override
    public String playStop(String deviceId, String channelId, String streamId) {
        return null;
    }

    /**
     * 获取萤石云播放地址
     *
     * @param deviceId
     * @param channelId
     * @return
     */
    @Override
    public String getPlayStream(String deviceId, String channelId) {
        String channelNo = channelId.split("-")[1];
        String playUrl = ysApiUtils.getPlayUrl(deviceId, channelNo, "4");
        Stream stream = new Stream();
        stream.setPlayurl(playUrl);
        stream.setStreamId(channelId);
        return JSON.toJSONString(stream);
    }

    @Override
    public Stream playback(String deviceId, String channelId, String startTime, String endTime) {
        String channelNo = channelId.split("-")[1];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(startTime)), ZoneId.of("Asia/Shanghai"));
        LocalDateTime end = LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(endTime)), ZoneId.of("Asia/Shanghai"));
        String startDate = start.format(formatter);
        String endDate = end.format(formatter);
        HashMap<String, String> map = ysApiUtils.getPlayUrlBack(deviceId, channelNo, "4", startDate, endDate);
        String playUrl = map.get("url");
        Stream stream = new Stream();
        stream.setPlayurl(playUrl);
        stream.setStreamId(channelId);
        return stream;
    }

    @Override
    public String playbackStop(String deviceId, String channelId, String streamId) {
        return null;
    }

    @Override
    public String playbackPause(String deviceId, String channelId, String streamId) {
        return null;
    }

    @Override
    public String playbackReplay(String deviceId, String channelId, String streamId) {
        return null;
    }

    @Override
    public String playbackSeek(String deviceId, String channelId, String streamId, long firstVideoTime, long seekTime) {
        String channelNo = channelId.split("-")[1];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.ofInstant(Instant.ofEpochSecond(seekTime), ZoneId.of("Asia/Shanghai"));
        LocalDateTime end = TimeStampUtils.getLastSecondOfDay(seekTime);
        String startDate = start.format(formatter);
        String endDate = end.format(formatter);
        HashMap<String, String> map = ysApiUtils.getPlayUrlBack(deviceId, channelNo, "4", startDate, endDate);
        String playUrl = map.get("url");
        String urlId = map.get("urlId");
        String OldUrlId = sipCacheService.getPlaybackUserStream(SecurityUtils.getUserId());
        if (OldUrlId != null) {
            ysApiUtils.getPlayUrlDisable(deviceId, channelNo, OldUrlId);
        }
        sipCacheService.setPlaybackUserStream(SecurityUtils.getUserId(), urlId);
        return playUrl;
    }

    @Override
    public String playbackSpeed(String deviceId, String channelId, String streamId, Integer speed) {
        return null;
    }
}
