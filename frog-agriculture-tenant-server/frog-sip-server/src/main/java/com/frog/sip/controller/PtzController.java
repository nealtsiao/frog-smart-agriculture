package com.frog.sip.controller;

import com.frog.common.core.domain.AjaxResult;
import com.frog.sip.server.model.PtzDirectionInput;
import com.frog.sip.server.model.PtzZoomInput;
import com.frog.sip.service.impl.camera.CameraFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/sip/ptz")
@Api(tags = "sip-摄像头控制")
public class PtzController {
    @Autowired
    private CameraFactory cameraFactory;

    /**
     * 方向控制
     * @param deviceId
     * @param channelId
     * @param ptzDirectionInput
     * @return
     */
    @ApiOperation("方向控制")
    @PostMapping ("/direction/{deviceId}/{channelId}")
    public AjaxResult direction(@PathVariable String deviceId, @PathVariable String channelId, @RequestBody PtzDirectionInput ptzDirectionInput) {
        return AjaxResult.success("success!", cameraFactory.createPtz(deviceId).direction(deviceId,channelId,ptzDirectionInput));
    }

    /**
     * 放大缩小控制
     * @param deviceId
     * @param channelId
     * @param ptzZoomInput
     * @return
     */
    @ApiOperation("放大缩小控制")
    @PostMapping("/scale/{deviceId}/{channelId}")
    public AjaxResult scale(@PathVariable String deviceId, @PathVariable String channelId, @RequestBody PtzZoomInput ptzZoomInput) {
        return AjaxResult.success("success!", cameraFactory.createPtz(deviceId).zoom(deviceId,channelId, ptzZoomInput));
    }

}
