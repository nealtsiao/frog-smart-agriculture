package com.frog.trace.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 溯源人员对象 agriculture_trace_staff
 * 
 * @author nealtsiao
 * @date 2023-08-02
 */
@Data
public class TraceStaff extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工ID */
    private Long staffId;

    /** 员工职位 */
    @Excel(name = "员工职位")
    private String staffPosition;

    /** 员工名称 */
    @Excel(name = "员工名称")
    private String staffName;

    /** 员工联系方式 */
    @Excel(name = "员工联系方式")
    private String staffTel;

    /** 员工描述 */
    @Excel(name = "员工描述")
    private String staffDes;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 排序 */
    private Long orderNum;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;
}
