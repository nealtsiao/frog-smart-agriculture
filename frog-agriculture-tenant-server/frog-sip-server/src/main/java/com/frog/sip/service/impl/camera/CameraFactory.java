package com.frog.sip.service.impl.camera;

import com.frog.iot.domain.Device;
import com.frog.iot.domain.Product;
import com.frog.iot.service.IDeviceService;
import com.frog.iot.service.IProductService;
import com.frog.sip.server.enums.CameraType;
import com.frog.sip.service.IPlayService;
import com.frog.sip.service.IPtzCmdService;
import com.frog.sip.service.IRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author neal
 * @date 2024/12/2  09:01
 */
@Service
public class CameraFactory {
    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private IProductService productService;
    @Autowired
    private Map<String,IPlayService> playService;
    @Autowired
    private Map<String,IPtzCmdService> ptzCmdService;
    @Autowired
    private Map<String, IRecordService> recordService;

    /**
     * 选择播放实现类
     * @param serialNum
     * @return
     */
    public IPlayService createPlay(String serialNum){
        Device device = deviceService.selectDeviceBySerialNumber(serialNum);
        Product product = productService.selectProductByProductId(device.getProductId());
        if(CameraType.SIP.getType()==product.getDeviceType()) {
            return playService.get(CameraType.SIP.getPlayBeanName());
        }else if(CameraType.YS.getType()==product.getDeviceType()){
            return playService.get(CameraType.YS.getPlayBeanName());
        }else{
            throw new IllegalArgumentException("Unknown service type");
        }
    }

    /**
     * 选择PTZ控制实现类
     * @param serialNum
     * @return
     */
    public IPtzCmdService createPtz(String serialNum){
        Device device = deviceService.selectDeviceBySerialNumber(serialNum);
        Product product = productService.selectProductByProductId(device.getProductId());
        if(CameraType.SIP.getType()==product.getDeviceType()) {
            return ptzCmdService.get(CameraType.SIP.getPtzBeanName());
        }else if(CameraType.YS.getType()==product.getDeviceType()){
            return ptzCmdService.get(CameraType.YS.getPtzBeanName());
        }else{
            throw new IllegalArgumentException("Unknown service type");
        }
    }
    /**
     * 选择录像回放实现类
     * @param serialNum
     * @return
     */
    public IRecordService createRecord(String serialNum){
        Device device = deviceService.selectDeviceBySerialNumber(serialNum);
        Product product = productService.selectProductByProductId(device.getProductId());
        if(CameraType.SIP.getType()==product.getDeviceType()) {
            return recordService.get(CameraType.SIP.getRecordBeanName());
        }else if(CameraType.YS.getType()==product.getDeviceType()){
            return recordService.get(CameraType.YS.getRecordBeanName());
        }else{
            throw new IllegalArgumentException("Unknown service type");
        }
    }
}
