package com.frog.trace.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 溯源模版对象 agriculture_trace_template
 *
 * @author nealtsiao
 * @date 2024-04-14
 */
@Data
public class TraceTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 模版ID */
    private Long templateId;

    /** 模版名称 */
    @Excel(name = "模版名称")
    private String templateName;

    /** 模版JSON */
    @Excel(name = "模版JSON")
    private String templateJson;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 排序 */
    private Long orderNum;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 基地坐标 */
    private String baseCoordinate;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;
}