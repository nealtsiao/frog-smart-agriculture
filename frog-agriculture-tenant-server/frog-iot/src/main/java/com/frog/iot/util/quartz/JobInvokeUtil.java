package com.frog.iot.util.quartz;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.frog.common.core.domain.entity.SysUser;
import com.frog.common.core.redis.RedisCache;
import com.frog.common.utils.DateUtils;
import com.frog.common.utils.spring.SpringUtils;
import com.frog.iot.domain.*;
import com.frog.iot.mapper.AlertLogMapper;
import com.frog.iot.mapper.AlertMapper;
import com.frog.iot.model.Action;
import com.frog.iot.model.ThingsModels.ThingsModelSimpleItem;
import com.frog.iot.mqtt.EmqxService;
import com.frog.iot.service.IDeviceService;
import com.frog.system.service.ISysUserService;
import org.dromara.sms4j.core.factory.SmsFactory;

import java.util.*;

/**
 * 任务执行工具
 *
 * @author kerwincui
 */
public class JobInvokeUtil {
    /**
     * 执行方法
     *
     * @param deviceJob 系统任务
     */
    public static void invokeMethod(DeviceJob deviceJob) throws Exception {
        // jobType 1设备定时 2产品告警 3场景联动
        if (deviceJob.getJobType() == 1) {
            System.out.println("------------------------执行定时任务-----------------------------");
            List<Action> actions = JSON.parseArray(deviceJob.getActions(), Action.class);
            List<ThingsModelSimpleItem> propertys = new ArrayList<>();
            List<ThingsModelSimpleItem> functions = new ArrayList<>();
            for (int i = 0; i < actions.size(); i++) {
                ThingsModelSimpleItem model = new ThingsModelSimpleItem();
                model.setId(actions.get(i).getId());
                model.setValue(actions.get(i).getValue());
                model.setRemark("设备定时");
                if (actions.get(i).getType() == 1) {
                    propertys.add(model);
                } else if (actions.get(i).getType() == 2) {
                    functions.add(model);
                }
            }
            EmqxService emqxService = SpringUtils.getBean(EmqxService.class);
            // 发布属性
            if (propertys.size() > 0) {
                emqxService.publishProperty(deviceJob.getProductId(), deviceJob.getSerialNumber(), propertys, 0);
            }
            // 发布功能
            if (functions.size() > 0) {
                emqxService.publishFunction(deviceJob.getProductId(), deviceJob.getSerialNumber(), functions, 0);
            }

        } else if (deviceJob.getJobType() == 2) {
            System.out.println("------------------------定时执行告警-----------------------------");
            /**
             * 实现逻辑：
             * 1.查询出所有符合产品告警规则的设备
             * 2.然后循环设备进行actions的处理，日志的记录，短信的发送
             */

            // 从定时任务记录中查询触发器alert_trigger字段
            // {"id": "fs", "name": "风速", "type": 1, "jobId": 2, "value": "10", "params": {}, "source": 2, "status": 1, "alertId": 1, "operator": ">", "isAdvance": 0, "productId": 106, "productName": "唐僧气象站产品11", "alertTriggerId": 10, "cronExpression": "0 40 13 ? * 1,2,3,4,5,6,7"}
            AlertTrigger alertTrigger = JSON.parseObject(deviceJob.getAlertTrigger(), AlertTrigger.class);
            if(alertTrigger==null){
                return;
            }
            //获取redis操作类行bean
            RedisCache redisCache = SpringUtils.getBean(RedisCache.class);

            // 查询该产的所有设备的key，例如190_239393,190_383839
            Set<String> keys = redisCache.getListKeyByPrefix("TSLV:" + alertTrigger.getProductId());

            /**
             * 根据key找到设备下面所有map对，然后根据id找到value，把符合规则的value和设备的key组合成map，因为定时任务每次只监测一个值
             */
            Map<String, String> map = redisCache.hashGetAllMatchByKeys(keys, alertTrigger.getOperator(), alertTrigger.getId(), alertTrigger.getValue());
            if (map == null || map.size() == 0) {
                return;
            }
            // 查询出告警的actions，可能是多个，分别放入propertys和functions
            List<Action> actions = JSON.parseArray(deviceJob.getActions(), Action.class);
            List<ThingsModelSimpleItem> propertys = new ArrayList<>();
            List<ThingsModelSimpleItem> functions = new ArrayList<>();
            for (int i = 0; i < actions.size(); i++) {
                ThingsModelSimpleItem model = new ThingsModelSimpleItem();
                model.setId(actions.get(i).getId());
                model.setValue(actions.get(i).getValue());
                model.setRemark("告警定时触发");
                if (actions.get(i).getType() == 1) {
                    propertys.add(model);
                } else if (actions.get(i).getType() == 2) {
                    functions.add(model);
                }
            }
            EmqxService emqxService = SpringUtils.getBean(EmqxService.class);
            AlertMapper alertMapper = SpringUtils.getBean(AlertMapper.class);
            AlertLogMapper alertLogMapper = SpringUtils.getBean(AlertLogMapper.class);
            IDeviceService deviceService = SpringUtils.getBean(IDeviceService.class);
            ISysUserService sysUserService = SpringUtils.getBean(ISysUserService.class);

            Alert alert = alertMapper.selectAlertByAlertId(alertTrigger.getAlertId());
            List<AlertLog> alertLogList = new ArrayList<>();
            //循环符合要起的设备map进行循环处理
            map.forEach((key, value) -> {
                // Key：TSLV:{productId}_{deviceNumber}
                String deviceNumber = key.substring(key.indexOf("_") + 1);
                // 发布属性
                if (propertys.size() > 0) {
                    emqxService.publishProperty(actions.get(0).getProductId(), deviceNumber, propertys, 0);
                }
                // 发布功能
                if (functions.size() > 0) {
                    emqxService.publishFunction(actions.get(0).getProductId(), deviceNumber, functions, 0);
                }

                // 生成告警消息通知
                AlertLog alertLog = new AlertLog();
                alertLog.setAlertName(alert.getAlertName());
                alertLog.setAlertLevel(alert.getAlertLevel());
                if (alert.getAlertLevel() == 1) {
                    // 1=不需要处理,2=未处理,3=已处理
                    alertLog.setStatus(1);
                } else {
                    alertLog.setStatus(2);
                }
                alertLog.setProductId(alertTrigger.getProductId());
                alertLog.setSerialNumber(deviceNumber);
                ThingsModelSimpleItem item = new ThingsModelSimpleItem();
                item.setId(alertTrigger.getId());
                item.setValue(value);
                item.setRemark("定时触发");
                alertLog.setDetail(JSONObject.toJSONString(item));
                alertLog.setCreateTime(DateUtils.getNowDate());
                alertLogList.add(alertLog);

                //发送短信
                Device device = deviceService.selectDeviceBySerialNumber(deviceNumber);
                if(alert.getMessageType().contains("2")){
                    if(device!=null){
                        SysUser sysUser = sysUserService.selectUserById(device.getUserId());
                        String phonenumber = sysUser.getPhonenumber();
                        String deviceName = device.getDeviceName();
                        String alertName = alert.getAlertName();
                        LinkedHashMap<String, String> message = new LinkedHashMap<>();
                        message.put("deviceName",deviceName);
                        message.put("serialNumber",deviceNumber);
                        message.put("time",DateUtils.getTime());
                        message.put("alertName",alertName);
                        SmsFactory.getSmsBlend("b1").sendMessage(phonenumber,message);
                    }
                }
            });
            // 批量插入告警日志
            alertLogMapper.insertAlertLogBatch(alertLogList);
        } else if (deviceJob.getJobType() == 3) {
            System.out.println("------------------------定时执行场景联动-----------------------------");
            List<Action> actions = JSON.parseArray(deviceJob.getActions(), Action.class);
            EmqxService emqxService = SpringUtils.getBean(EmqxService.class);
            for (int i = 0; i < actions.size(); i++) {
                ThingsModelSimpleItem model = new ThingsModelSimpleItem();
                model.setId(actions.get(i).getId());
                model.setValue(actions.get(i).getValue());
                model.setRemark("场景联动定时触发");
                if (actions.get(i).getType() == 1) {
                    List<ThingsModelSimpleItem> propertys = new ArrayList<>();
                    propertys.add(model);
                    emqxService.publishProperty(actions.get(i).getProductId(), actions.get(i).getSerialNumber(), propertys, 0);
                } else if (actions.get(i).getType() == 2) {
                    List<ThingsModelSimpleItem> functions = new ArrayList<>();
                    functions.add(model);
                    emqxService.publishFunction(actions.get(i).getProductId(), actions.get(i).getSerialNumber(), functions, 0);
                }
            }
        }
    }
}
