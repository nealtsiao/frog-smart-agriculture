package com.frog.iot.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;

/**
 * 通用物模型对象 iot_things_model_template
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Data
public class ThingsModelTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物模型ID */
    private Long templateId;

    /** 物模型名称 */
    @Excel(name = "物模型名称")
    private String templateName;

    /** 标识符，产品下唯一 */
    @Excel(name = "标识符")
    private String identifier;

    /** 模型类别（1-属性，2-功能，3-事件） */
    @Excel(name = "模型类别", dictType = "iot_things_type")
    private Integer type;

    /** 数据类型（integer、decimal、string、bool、array、enum） */
    @Excel(name = "数据类型")
    private String datatype;

    /** 数据定义 */
    @Excel(name = "数据定义")
    private String specs;

    /** 是否系统通用（0-否，1-是） */
    @Excel(name = "是否系统通用", dictType = "iot_yes_no")
    private Integer isSys;

    /** 是否首页显示（0-否，1-是） */
    @Excel(name = "是否首页显示", dictType = "iot_yes_no")
    private Integer isTop;

    /** 是否实时监测（0-否，1-是） */
    @Excel(name = "是否实时监测", dictType = "iot_yes_no")
    private Integer isMonitor;

    /** 是否存储历史数据（0-否，1-是） */
    @Excel(name = "是否存储历史数据", readConverterExp = "0=-否，1-是")
    private Integer isRecord;

    /** 是否只读数据（0-否，1-是） */
    private Integer isReadonly;

    /** 排序，值越大排名越靠前 */
    private Integer modelOrder;

    /** 图标 */
    private String modelIcon;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;
}
