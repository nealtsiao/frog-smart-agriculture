package com.frog.sip.controller;

import com.frog.common.annotation.Log;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.domain.AjaxResult;
import com.frog.common.core.page.TableDataInfo;
import com.frog.common.enums.BusinessType;
import com.frog.common.utils.poi.ExcelUtil;
import com.frog.sip.domain.SipConfig;
import com.frog.sip.service.ISipConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import com.frog.sip.server.conf.SysSipConfig;

/**
 * sip系统配置Controller
 *
 * @author zhuangpeng.li
 * @date 2022-11-30
 */
@RestController
@RequestMapping("/sip/sipconfig")
@Api(tags = "sip-SIP配置")
public class SipConfigController extends BaseController {
    @Autowired
    private ISipConfigService sipConfigService;

    @Autowired
    private SysSipConfig sysSipConfig;

    /**
     * 查询sip系统配置列表
     */
    @ApiOperation("查询sip系统配置列表")
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @GetMapping("/list")
    public TableDataInfo list(SipConfig sipConfig) {
        startPage();
        List<SipConfig> list = sipConfigService.selectSipConfigList(sipConfig);
        return getDataTable(list);
    }

    /**
     * 导出sip系统配置列表
     */
    @ApiOperation("导出sip系统配置列表")
    @PreAuthorize("@ss.hasPermi('iot:video:list')")
    @Log(title = "sip系统配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SipConfig sipConfig) {
        List<SipConfig> list = sipConfigService.selectSipConfigList(sipConfig);
        ExcelUtil<SipConfig> util = new ExcelUtil<SipConfig>(SipConfig.class);
        util.exportExcel(response, list, "sip系统配置数据");
    }

    /**
     * 获取产品下第一条sip系统配置详细信息
     */
    @ApiOperation("获取产品下第一条sip系统配置详细信息")
    @PreAuthorize("@ss.hasPermi('iot:video:query')")
    @GetMapping(value = "/{productId}/{isDefault}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId, @PathVariable("isDefault") Boolean isDefault) {
        SipConfig sipConfig = new SipConfig();
        if (isDefault) {
            // 设置默认值
            sipConfig.setEnabled(sysSipConfig.isEnabled() ? 1 : 0);
            sipConfig.setIp(sysSipConfig.getIp());
            sipConfig.setPort(sysSipConfig.getPort());
            sipConfig.setPassword(sysSipConfig.getPassword());
            sipConfig.setDomain(sysSipConfig.getDomain());
            sipConfig.setServerSipid(sysSipConfig.getId());
        } else {
            List<SipConfig> list = sipConfigService.selectSipConfigByProductId(productId);
            if (list.size() > 0) {
                sipConfig = list.get(0);
            }
        }
        return AjaxResult.success(sipConfig);
    }

    /**
     * 新增sip系统配置
     */
    @ApiOperation("新增sip系统配置")
    @PreAuthorize("@ss.hasPermi('iot:video:add')")
    @Log(title = "sip系统配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SipConfig sipConfig) {
        int result = sipConfigService.insertSipConfig(sipConfig);
        return AjaxResult.success(sipConfig);
    }

    /**
     * 修改sip系统配置
     */
    @ApiOperation("修改sip系统配置")
    @PreAuthorize("@ss.hasPermi('iot:video:edit')")
    @Log(title = "sip系统配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SipConfig sipConfig) {
        return toAjax(sipConfigService.updateSipConfig(sipConfig));
    }

    /**
     * 默认sip配置
     * @param sipConfig
     * @return
     */
    @ApiOperation("默认sip配置")
    @Log(title = "sip系统配置", businessType = BusinessType.UPDATE)
    @PostMapping("/defaultconfig")
    public AjaxResult defaultconfig(@RequestBody SipConfig sipConfig) {
        return toAjax(sipConfigService.updateDefaultSipConfig(sipConfig));
    }

    /**
     * 删除sip系统配置
     */
    @ApiOperation("删除sip系统配置")
    @PreAuthorize("@ss.hasPermi('iot:video:remove')")
    @Log(title = "sip系统配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(sipConfigService.deleteSipConfigByIds(ids));
    }

    /**
     * 删除sip系统配置
     * @param productIds
     * @return
     */
    @ApiOperation("删除sip系统配置")
    @PreAuthorize("@ss.hasPermi('iot:video:remove')")
    @Log(title = "sip系统配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/product/{productIds}")
    public AjaxResult removeByProductId(@PathVariable Long[] productIds) {
        // 设备可能不存在通道，可以返回0
        int result=sipConfigService.deleteSipConfigByProductIds(productIds);
        return AjaxResult.success(result);
    }
}
