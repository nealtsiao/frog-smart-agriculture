package com.frog.iot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.frog.common.annotation.Excel;
import com.frog.common.core.domain.BaseEntity;
import com.frog.iot.model.SipDeviceChannel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 设备对象 iot_device
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
@Data
public class Device extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品分类ID */
    private Long deviceId;

    /** 产品分类名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long productId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 是否监控设备*/
    private String isCamera;

    /** 设备编号 */
    @Excel(name = "设备编号")
    private String serialNumber;

    /** 固件版本 */
    @Excel(name = "固件版本")
    private BigDecimal firmwareVersion;

    /** 设备状态（1-未激活，2-禁用，3-在线，4-离线） */
    @Excel(name = "设备状态")
    private Integer status;

    /** wifi信号强度（信号极好4格[-55— 0]，信号好3格[-70— -55]，信号一般2格[-85— -70]，信号差1格[-100— -85]） */
    @Excel(name = "wifi信号强度")
    private Integer rssi;

    /** 设备影子 */
    private Integer isShadow;

    /** 是否自定义位置 **/
    private Integer locationWay;

    @Excel(name = "物模型")
    private String thingsModelValue;

    /** 设备所在地址 */
    @Excel(name = "设备所在地址")
    private String networkAddress;

    /** 设备入网IP */
    @Excel(name = "设备入网IP")
    private String networkIp;

    /** 设备经度 */
    @Excel(name = "设备经度")
    private BigDecimal longitude;

    /** 设备纬度 */
    @Excel(name = "设备纬度")
    private BigDecimal latitude;

    /** 激活时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "激活时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activeTime;

    /** 设备摘要 **/
    private String summary;

    /** 图片地址 */
    private String imgUrl;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 租户ID */
    private Long tenantId;

    /** 基地ID */
    private Long baseId;

    /** 用户部门ID */
    private Long deptId;

    /** 用户ID */
    private Long userId;

    /** 是否设备所有者，用于查询 **/
    private Integer isOwner;

    /** 设备类型（1-直连设备、2-网关子设备、3-网关设备） */
    private Integer deviceType;

    /** 分组ID，用于分组查询 **/
    private Long groupId;

    /** 通道列表**/
    private List<HashMap> children;

}
