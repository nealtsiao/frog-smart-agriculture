package com.frog.trace.controller;

import com.frog.system.service.IBaseService;
import com.frog.trace.service.ITraceDataStatisticsService;
import com.frog.common.core.controller.BaseController;
import com.frog.common.core.page.TableDataInfo;
import com.frog.trace.domain.TraceRecord;
import com.frog.trace.service.ITraceRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@Api(tags = "trace-统计分析")
@RestController
@RequestMapping("/trace/statistics")
public class TraceDataStatisticsController extends BaseController {

    @Autowired
    private ITraceDataStatisticsService dataStatisticsService;
    @Autowired
    private ITraceRecordService traceRecordService;

    /**
     * 统计溯源记录信息
     */
    @ApiOperation("统计溯源记录信息")
    @GetMapping("/selectRecordGroupByMonth/{baseId}")
    public TableDataInfo selectRecordGroupByMonth(@PathVariable Long baseId){
        List<HashMap> list = dataStatisticsService.selectRecordGroupByMonth(baseId);
        return getDataTable(list);
    }

    /**
     * 按城市分组统计溯源记录
     */
    @ApiOperation("按城市分组统计溯源记录")
    @GetMapping("/selectRecordGroupByCity/{baseId}")
    public TableDataInfo selectRecordGroupByCity(@PathVariable Long baseId){
        List<HashMap> list = dataStatisticsService.selectRecordGroupByCity(baseId);
        return getDataTable(list);
    }

    /**
     * 统计溯源基础信息
     */
    @ApiOperation("统计溯源基础信息")
    @GetMapping("/selectRecordStatistics/{baseId}")
    public TableDataInfo selectRecordStatistics(@PathVariable Long baseId){
        List<HashMap> list = dataStatisticsService.selectRecordStatistics(baseId);
        return getDataTable(list);
    }

    /**
     * 按产品分组统计溯源信息
     */
    @ApiOperation("按产品分组统计溯源信息")
    @GetMapping("/selectRecordGroupBySellpro/{baseId}")
    public TableDataInfo selectRecordGroupBySellpro(@PathVariable Long baseId){
        List<HashMap> list = dataStatisticsService.selectRecordGroupBySellpro(baseId);
        return getDataTable(list);
    }

    /**
     * 按城市分组统计溯源记录（有城市坐标）
     */
    @ApiOperation("按城市分组统计溯源记录（有城市坐标）")
    @GetMapping("/selectRecord/{baseId}")
    public TableDataInfo selectRecord(@PathVariable Long baseId)
    {
        startPage();
        List<HashMap> list = dataStatisticsService.selectRecord(baseId);
        return getDataTable(list);
    }

    /**
     * 统计溯源信息（溯源统计界面使用）
     */
    @ApiOperation("统计溯源信息（溯源统计界面使用）")
    @GetMapping("/selectTraceInfo/{baseId}")
    public TableDataInfo selectTraceInfo(@PathVariable Long baseId){
        List<HashMap> list = dataStatisticsService.selectTraceInfo(baseId);
        return getDataTable(list);
    }
}
