package com.frog.sip.server.conf;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "sip")
/**
 * 读取application.yml配置文件中的SIP配置信息
 */
public class SysSipConfig {
    boolean enabled;
    String ip;
    Integer port;
    String domain;
    String id;
    String password;
    Long   monitorTime;
}
