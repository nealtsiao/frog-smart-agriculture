package com.frog.sip.server.transcation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sip.InvalidArgumentException;
import javax.sip.ServerTransaction;
import javax.sip.SipException;
import javax.sip.SipProvider;
import javax.sip.message.Message;
import javax.sip.message.Request;
import javax.sip.message.Response;

/**
 * @author neal
 * @date 2024/12/13  16:31
 */
@Component
@Slf4j
public class SipSender {
    @Autowired
    SipProvider sipProvider;
    public void transmit( Message message) throws SipException {
        log.info(message.toString());
        if (message instanceof Request) {
            sipProvider.sendRequest((Request)message);
        }else if (message instanceof Response) {
            sipProvider.sendResponse((Response)message);
        }
    }
    public void tansmitByServerTanscation(ServerTransaction serverTransaction,Message message){
        log.info(message.toString());
        try {
            serverTransaction.sendResponse((Response) message);
        } catch (SipException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }
}
